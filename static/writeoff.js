"use strict"
var button = document.getElementById("sendData");
var errPlace = document.getElementById("errPlace");
button.onclick = sendData;

var stock = JSON.parse(document.getElementById("stock").innerHTML);

function sendData (){
    button.disabled = true;
    var currTime = new Date().toISOString().slice(0,10);
    var wroffDoc =
        { qty   : Math.round(100 * parseFloat(document.getElementById("qty").value))
        , day   :  currTime
        , note  : document.getElementById("note").value
        , stockK : stock.stockK
        };


    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == XMLHttpRequest.DONE) {
            if (xhttp.status === 200) {
                window.location.replace("/protected/html/stock/lots");
            } else {
                button.disabled = false;
                errPlace.innerHTML = "Error: " + xhttp.status + ' - ' + xhttp.statusText + ': ' + xhttp.responseText;
            }
        }
    }
    xhttp.open("POST", "/protected/stock/writeoff", true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send(JSON.stringify(wroffDoc));
}

