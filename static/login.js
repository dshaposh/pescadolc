"use strict"
var button = document.getElementById("sendData");
button.onclick = sendData;
var errPlace = document.getElementById("errPlace");
function sendData (){
    button.disabled = true;
    var name = document.getElementById("login").value;
    var pwd = document.getElementById("pwd").value;
    var tzshift = new Date().getTimezoneOffset();

    var credentials = [ name, pwd, -1*tzshift ];
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == XMLHttpRequest.DONE) {
            if (xhttp.status === 200) {
                window.location.replace("/protected/html/stock/summary");
            } else {
                button.disabled = false;
                errPlace.innerHTML = "Error: " + xhttp.status + ' - ' + xhttp.statusText + ': ' + xhttp.responseText;
            }
        }
    }

    xhttp.open("POST", "/protected/login", true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send(JSON.stringify(credentials));
}

