"use strict"
var sendBtn = document.getElementById("sendData");
sendBtn.onclick = sendData;
var errPlace = document.getElementById("errPlace");

var plCont = document.getElementById("picklist");
var plEls = plCont.getElementsByTagName("input");
var l = plEls.length;

var rowK = document.getElementById("rowK").innerHTML;

function sendData (){
    sendBtn.disabled = true;
    var picklist=[];

    var qtyEl;
    var placeK;
    for(var i=0; i<l; i++){
        var plEl = plEls[i];
        var qty = Math.round(parseFloat(plEl.value)*100) || 0;
        if (qty !== 0) {
            placeK = parseInt(plEl.id);
            picklist.push([placeK, qty]);
        }
    }


    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == XMLHttpRequest.DONE) {
            if (xhttp.status === 200) {
                window.location.replace("/protected/html/orders/rows/2prepare");
            } else {
                sendBtn.disabled = false;
                errPlace.innerHTML = "Error: " + xhttp.status + ' - ' + xhttp.statusText + ': ' + xhttp.responseText;
            }
        }
    }
    xhttp.open("POST", "/protected/orders/rows/" + rowK + "/prepare", true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send(JSON.stringify(picklist));
}

