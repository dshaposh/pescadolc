"use strict"
var button = document.getElementById("sendData");
button.onclick = sendData;
var errPlace = document.getElementById("errPlace");
function sendData (){
    button.disabled = true;
    var name = document.getElementById("name").value;
    var taxid = document.getElementById("taxid").value;
    var phone = document.getElementById("phone").value;

    var vendor =
        { "name"      : name
        , "ordnum"    : 1000
        , "taxid"     : taxid
        , "phone"     : phone
        };
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == XMLHttpRequest.DONE) {
            if (xhttp.status === 200) {
                window.location.replace("/protected/html/vendors");
            } else {
                button.disabled = false;
                errPlace.innerHTML = "Error: " + xhttp.status + ' - ' + xhttp.statusText + ': ' + xhttp.responseText;
            }
        }
    }

    xhttp.open("POST", "/protected/vendors", true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send(JSON.stringify(vendor));
}
