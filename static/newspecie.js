"use strict"
var buttonSave = document.getElementById("sendData");
buttonSave.onclick = saveSpecie;
//var buttonDelete = document.getElementById("deleteItem");
//buttonDelete.onclick = deleteItem;
var errPlace = document.getElementById("errPlace");

//var specieK = document.getElementById("itemK").value;

function saveSpecie (){
    buttonSave.disabled = true;
    var nameInp = document.getElementById("name");
    var uomSel = document.getElementById("uom");
    var priceInp = document.getElementById("price");

    var specie =
        { name: nameInp.value
        , ordnum: 100
        , uom: parseInt(uomSel.options[uomSel.selectedIndex].value)
        , price: parseInt(priceInp.value)
        };

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == XMLHttpRequest.DONE) {
            if (xhttp.status === 200) {
                window.location.replace("/protected/html/species");
            } else {
                buttonSave.disabled = false;
                errPlace.innerHTML = "Error: " + xhttp.status + ' - ' + xhttp.statusText + ': ' + xhttp.responseText;
            }
        }
    }
    xhttp.open("POST", "/protected/species/", true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send(JSON.stringify(specie));
}

