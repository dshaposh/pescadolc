"use strict"
var btn = document.getElementById("sendData");
btn.onclick = sendContent;

var errPlace = document.getElementById("errPlace");

var rsrNameSel = document.getElementById("name");

function sendContent (){
    btn.disabled = true;
    var rsrName = rsrNameSel.options[rsrNameSel.selectedIndex].value;
    var contentInp = document.getElementById("content");
    var content = contentInp.files[0];
    var formData = new FormData();
    formData.append(rsrName, content);

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == XMLHttpRequest.DONE) {
            if (xhttp.status === 200) {
                window.location.replace("/protected/html/cms");
            } else {
                btn.disabled = false;
                errPlace.innerHTML = "Error: " + xhttp.status + ' - ' + xhttp.statusText + ': ' + xhttp.responseText;
            }
        }
    }
    xhttp.open("POST", "/protected/cms/", true);
    xhttp.send(formData);
}

