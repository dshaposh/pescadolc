"use strict"
var button = document.getElementById("sendData");
button.onclick = sendData;
var radios = document.getElementsByName("order");
var rowsQty = radios.length;
var errPlace = document.getElementById("errPlace")
function sendData (){
    button.disabled = true;
    var deliveredOrder;

    for(var i=0; i< rowsQty; i++){
        if (radios[i].checked){
            deliveredOrder = radios[i].id
        }
    }

    if (deliveredOrder == null) {
        button.disabled = false;
        errPlace.innerHTML = "Error: Ninguna orden está selecionada";
        return;
    }

    var delivery={};
    delivery.orderK=parseInt(deliveredOrder);

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == XMLHttpRequest.DONE) {
            if (xhttp.status === 200) {
                window.location.replace("/protected/html/orders/2deliver");
            } else {
                button.disabled = false;
                errPlace.innerHTML = "Error: " + xhttp.status + ' - ' + xhttp.statusText + ': ' + xhttp.responseText;
            }
        }
    }

    xhttp.open("POST", "/protected/orders/deliveries", true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send(deliveredOrder);
}

