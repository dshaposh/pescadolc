"use strict"
var buttonImg = document.getElementById("sendImage");
buttonImg.onclick = saveImage;
var buttonPrice = document.getElementById("sendPrice");
buttonPrice.onclick = updPrice;
var buttonDelete = document.getElementById("deleteSpecie");
buttonDelete.onclick = deleteSpecie;
var errPlace = document.getElementById("errPlace");

var specieK = document.getElementById("specieK").value;

var imageInp = document.getElementById("image"),
    descrTxtAr = document.getElementById("description");

function saveImage (){
    buttonImg.disabled = true;
    var image = imageInp.files[0],
        descr = descrTxtAr.value,
        formData = new FormData();
    formData.append("image", image);
    formData.append("description", descr);

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == XMLHttpRequest.DONE) {
            if (xhttp.status === 200) {
                window.location.replace("/protected/html/species");
            } else {
                buttonImg.disabled = false;
                errPlace.innerHTML = "Error: " + xhttp.status + ' - ' + xhttp.statusText + ': ' + xhttp.responseText;
            }
        }
    }
    xhttp.open("POST", "/protected/species/" + specieK + "/image", true);
    xhttp.send(formData);
}

function updPrice (){
    buttonPrice.disabled = true;
    var priceInp = document.getElementById("price");
    var price = parseInt(priceInp.value.replace(/\./g,''));

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == XMLHttpRequest.DONE) {
            if (xhttp.status === 200) {
                window.location.replace("/protected/html/species");
            } else {
                buttonPrice.disabled = false;
                errPlace.innerHTML = "Error: " + xhttp.status + ' - ' + xhttp.statusText + ': ' + xhttp.responseText;
            }
        }
    }
    xhttp.open("PATCH", "/protected/species/" + specieK, true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send(price);
}

function deleteSpecie (){
    buttonDelete.disabled = true;

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == XMLHttpRequest.DONE) {
            if (xhttp.status === 200) {
                window.location.replace("/protected/html/species");
            } else {
                buttonDelete.disabled = false;
                errPlace.innerHTML = "Error: " + xhttp.status + ' - ' + xhttp.statusText + ': ' + xhttp.responseText;
            }
        }
    }
    xhttp.open("DELETE", "/protected/species/" + specieK, true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send();
}

