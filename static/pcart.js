"use strict"
var tmplOrder = _.template(document.getElementById("tmplOrder").innerHTML);
var errPlace = document.getElementById("errPlace");

var totalEl = document.getElementById("total");
var total = parseInt(totalEl.innerHTML.replace(/\./g, ""));
var subTotalEl = document.getElementById("subtotal");
var subTotal = parseInt(subTotalEl.innerHTML.replace(/\./g, ""));

var orderEl = document.getElementById("orderEl");
var client = JSON.parse(document.getElementById("user").innerHTML);

var submitOrder = function(phone){
    console.log(phone);
    errPlace.innerHTML = "";
    orderEl.innerHTML = tmplOrder({phone: phone});
    var saveOrderBtn = document.getElementById("saveOrderBtn");
    var newPhoneBtn = document.getElementById("newPhoneBtn");
    saveOrderBtn.onclick = saveOrder;
    newPhoneBtn.onclick = function(){
        newPhoneBtn.disabled = true;
        var promise = logout();
        promise
            .finally(() => {newPhoneBtn.disabled = false;}) 
            .then
                ( (smsDelay) => {
                    orderEl.innerHTML=""; 
                    loginModule(null, submitOrder);
                    }
                , (err) => { errPlace.innerHTML = err}
                )
    }
    function saveOrder (){
        saveOrderBtn.disabled = true;

        var pcart = [];
        var pcartEl = document.getElementById("pcart");
        var pitemEls = pcartEl.querySelectorAll(".pitem");
        var rowsQty = pitemEls.length;
        var qty;
        for (var i=0; i < rowsQty; i++){
            var merchP = JSON.parse(pitemEls[i].getAttribute("merchkey"));
            var amountStr = pitemEls[i].getElementsByClassName("pamount")[0].innerHTML.replace(/\./g, "");
            var instr = pitemEls[i].getElementsByClassName("instruction")[0].value;
            var prow = [merchP, parseInt(amountStr), instr];
            pcart.push(prow);
            }
        var name = document.getElementById("clname").value;
        var address = document.getElementById("claddress").value;
        var dlvPrice = parseInt(document.getElementById("dlvPrice").innerHTML.replace(/\./g, ""));
        var dlvDay = document.getElementById("dlvDay").innerHTML;
            //, day: new Date().toJSON().slice(0,10)
        var orderHdr =
            { phone: phone
            , dlvDay: dlvDay 
            , name : name
            , address : address
            , dlvCost : dlvPrice
            , total : total
            };
        console.log(orderHdr);
        var order = [ orderHdr, pcart ]; 

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState == XMLHttpRequest.DONE) {
                if (xhttp.status === 200) {
                    window.location.replace("/html/orders");
                } else {
                    saveOrderBtn.disabled = false;
                    errPlace.innerHTML = "Error: " + xhttp.status + ' - ' + xhttp.statusText + ': ' + xhttp.responseText;
                }
            }
        }
        xhttp.open("POST", "/order", true);
        xhttp.setRequestHeader("Content-Type", "application/json");
        xhttp.send(JSON.stringify(order));
    }
}


if (client !== null && client[1] == "Authenticated"){
    submitOrder(client[0]);
} else {
    loginModule(client, submitOrder);
}

//---------------------------------------------------------------------
var addListener = function(btn){

    var container = btn.closest(".pitem");
    var amount = parseInt(container.getElementsByClassName("pamount")[0].innerHTML.replace(/\./g, ""));
    btn.addEventListener("click", removeRow, true);

    function removeRow () {

        var merchK = JSON.parse(container.getAttribute("merchkey"));
        btn.disabled = true;
        document.body.style.cursor = "wait";
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState == XMLHttpRequest.DONE) {
                if (xhttp.status === 200) {
                    document.body.style.cursor = "";
                    container.remove();
                    total -= amount;
                    totalEl.innerHTML=formatNumber(total);
                    subTotal -= amount;
                    subTotalEl.innerHTML=formatNumber(subTotal);
                } else {
                    btn.disabled = false;
                    document.body.style.cursor = "";
                    errPlace.innerHTML = "Error: " + xhttp.status + ' - ' + xhttp.statusText + ': ' + xhttp.responseText;
                }
            }
        }
        xhttp.open("DELETE", "/pcart", true);
        xhttp.setRequestHeader("Content-Type", "application/json");
        xhttp.send(JSON.stringify(merchK));

    }
}



var deleteDivs = document.getElementsByClassName("pdelete");
var itemsQty = deleteDivs.length;
for (var i=0; i<itemsQty; i++){
    var btn = deleteDivs[i].getElementsByTagName("button")[0];
    btn && addListener(btn);
}

