"use strict"
var buttonMarkup = document.getElementById("saveMarkup");
buttonMarkup.onclick = saveMarkup;
var buttonDelete = document.getElementById("deleteItem");
buttonDelete.onclick = deleteItem;
var errPlace = document.getElementById("errPlace");

var itemK = document.getElementById("itemK").value;

var markupInp = document.getElementById("markupInp");
function saveMarkup (){
    buttonMarkup.disabled = true;
    var markupVal = Math.round(parseFloat(markupInp.value)*100);

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == XMLHttpRequest.DONE) {
            if (xhttp.status === 200) {
                window.location.replace("/protected/html/items");
            } else {
                buttonMarkup.disabled = false;
                errPlace.innerHTML = "Error: " + xhttp.status + ' - ' + xhttp.statusText + ': ' + xhttp.responseText;
            }
        }
    }
    xhttp.open("PATCH", "/protected/items/" + itemK, true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send(JSON.stringify(markupVal));
}

function deleteItem (){
    buttonDelete.disabled = true;

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == XMLHttpRequest.DONE) {
            if (xhttp.status === 200) {
                window.location.replace("/protected/html/items");
            } else {
                buttonDelete.disabled = false;
                errPlace.innerHTML = "Error: " + xhttp.status + ' - ' + xhttp.statusText + ': ' + xhttp.responseText;
            }
        }
    }
    xhttp.open("DELETE", "/protected/items/" + itemK, true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send();
}
