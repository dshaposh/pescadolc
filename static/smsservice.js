"use strict"
var button = document.getElementById("sendSms");
var responsePlace = document.getElementById("response");
button.onclick = sendData;
function sendData (){
    button.disabled = true;
    var phone = document.getElementById("phone").value.replace(/^0/, "");

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == XMLHttpRequest.DONE) {
            button.disabled = false;
            responsePlace.innerHTML = xhttp.status + ' - ' + xhttp.statusText + ': ' + xhttp.responseText;
            }
        }

    xhttp.open("POST", "/protected/smsservice", true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send(JSON.stringify(phone));
}

