"use strict"
var btnPrice = document.getElementById("sendPrice");
var errPrice = document.getElementById("errPrice");
btnPrice.onclick = sendPrice;
function sendPrice (){
    btnPrice.disabled = true;
    var price = parseInt(document.getElementById("price").value);

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == XMLHttpRequest.DONE) {
            if (xhttp.status === 200) {
                window.location.replace("/protected/html/delivery");
            } else {
                btnPrice.disabled = false;
                errPrice.innerHTML = "Error: " + xhttp.status + ' - ' + xhttp.statusText + ': ' + xhttp.responseText;
            }
        }
    }
    xhttp.open("POST", "/protected/delivery/price", true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send(JSON.stringify(price));
}

var btnDay = document.getElementById("sendDay");
var errDay = document.getElementById("errDay");
btnDay.onclick = sendDay;
function sendDay (){
    btnDay.disabled = true;
    var day = document.getElementById("day").value;

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == XMLHttpRequest.DONE) {
            if (xhttp.status === 200) {
                window.location.replace("/protected/html/delivery");
            } else {
                btnDay.disabled = false;
                errDay.innerHTML = "Error: " + xhttp.status + ' - ' + xhttp.statusText + ': ' + xhttp.responseText;
            }
        }
    }
    xhttp.open("POST", "/protected/delivery/day", true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send(JSON.stringify(day));
}
