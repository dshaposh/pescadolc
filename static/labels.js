"use strict"
var button = document.getElementById("sendData");
button.onclick = sendData;
var errPlace = document.getElementById("errPlace");
var radios = document.getElementsByName("label");
var rowsQty = radios.length;
function sendData (){
    button.disabled = true;
    var label2Del;

    for(var i=0; i< rowsQty; i++){
        if (radios[i].checked){
            label2Del = radios[i].id
        }
    }

    if (label2Del == null) {
        button.disabled = false;
        errPlace.innerHTML = "Error: Ningun presinto está selecionado";
        return;
    }


    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == XMLHttpRequest.DONE) {
            if (xhttp.status === 200) {
                window.location.replace("/protected/html/labels");
            } else {
                button.disabled = false;
                errPlace.innerHTML = "Error: " + xhttp.status + ' - ' + xhttp.statusText + ': ' + xhttp.responseText;
            }
        }
    }

    xhttp.open("DELETE", "/protected/labels", true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send(JSON.stringify(label2Del));
}

