"use strict"
var button = document.getElementById("sendData");
button.onclick = sendData;
var errPlace = document.getElementById("errPlace");
var szSel = document.getElementById("size");
var szValInp = document.getElementById("sizeval");
var changeSz = function(){
    console.log("here");
    size = szSel.options[szSel.selectedIndex].value;
    if (size == "Fixed") {
        szValInp.style.display = "inline";
        szValInp.value = 0;
    } else {
        szValInp.style.display = "none";
    }
}
changeSz();
szSel.onchange = changeSz

function sendData (){
    button.disabled = true;
    var name = document.getElementById("name").value;
    var speSel = document.getElementById("specie");
    var cutSel = document.getElementById("cutting");
    var uomSel = document.getElementById("uom");
    var markup = document.getElementById("markup").value;

    var item = {};
    item.name = name;
    item.ordnum = 1000;
    item.specie = parseInt(speSel.options[speSel.selectedIndex].value);
    item.cutting = parseInt(cutSel.options[cutSel.selectedIndex].value);
    item.uom = parseInt(uomSel.options[uomSel.selectedIndex].value);
    item.size = {};
    item.size.tag = szSel.options[szSel.selectedIndex].value;
    item.size.contents = parseInt(szValInp.value);
    item.markup = Math.round(parseFloat(markup)*100);

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == XMLHttpRequest.DONE) {
            if (xhttp.status === 200) {
                window.location.replace("/protected/html/items");
            } else {
                button.disabled = false;
                errPlace.innerHTML = "Error: " + xhttp.status + ' - ' + xhttp.statusText + ': ' + xhttp.responseText;
            }
        }
    }
    xhttp.open("POST", "/protected/items", true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send(JSON.stringify(item));
}

