"use strict"
var wroffBtn = document.getElementById("writeoff");
var errPlace = document.getElementById("errPlace");
wroffBtn.onclick = writeOff;

var labelK = document.getElementById("labelK").value;

function writeOff () {
    wroffBtn.disabled = true;
    var currTime = new Date().toISOString().slice(0,10);

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == XMLHttpRequest.DONE) {
            if (xhttp.status === 200) {
                window.location.replace("/protected/html/stock/labeled");
            } else {
                wroffBtn.disabled = false;
                errPlace.innerHTML = "Error: " + xhttp.status + ' - ' + xhttp.statusText + ': ' + xhttp.responseText;
            }
        }
    }
    xhttp.open("POST", "/protected/stock/labeled/" + labelK + "/writeoff", true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send();
}

