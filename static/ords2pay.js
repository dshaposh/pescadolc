"use strict"
var button = document.getElementById("sendData");
button.onclick = sendData;
var radios = document.getElementsByName("order");
var rowsQty = radios.length;
var errPlace = document.getElementById("errPlace")
function sendData (){
    button.disabled = true;
    var paidOrder;

    for(var i=0; i< rowsQty; i++){
        if (radios[i].checked){
            paidOrder = radios[i].id
        }
    }

    if (paidOrder == null) {
        button.disabled = false;
        errPlace.innerHTML = "Error: Ninguna orden está selecionada";
        return;
    }

    var payment={};
    payment.orderK=parseInt(paidOrder);

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == XMLHttpRequest.DONE) {
            if (xhttp.status === 200) {
                window.location.replace("/protected/html/orders/2pay");
            } else {
                button.disabled = false;
                errPlace.innerHTML = "Error: " + xhttp.status + ' - ' + xhttp.statusText + ': ' + xhttp.responseText;
            }
        }
    }

    xhttp.open("POST", "/protected/orders/payments", true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send(JSON.stringify(payment));
}

