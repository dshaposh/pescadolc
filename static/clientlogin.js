"use strict"
function loginModule(client, callback){
    var showDelay = function(num){
        var result = "";
        var hrs = ~~(num / (60 * 60));
        if (hrs > 0) result = hrs+"h:";
        num = num % (60 * 60);
        var mins = ~~(num / 60);
        if (mins > 0) result += mins+"m:";
        result += (num % 60) + "s";
        return result;
    }
    var Counter = function (inival, el){
        var toWait = inival;
        return function (){
            el.innerHTML= showDelay(toWait);
            toWait -= 1
        }
    }

    var submitEl = document.getElementById("submitPhone");
    var confirmEl = document.getElementById("confirmPhone");

    var submitPhone = function(smsDelay){
        submitEl.style.display="block";
        var errPlace = document.getElementById("errPlace");
        errPlace.innerHTML = "";

        var phoneInp = document.getElementById("phoneInp");
        phoneInp.value = "04";
        phoneInp.oninput = function(){
            this.value = this.value.replace(/[^0-9]/g, '').replace(/^0*4?/g, '').replace(/(^)/g, '04$1');
        };
        var phoneBtn = document.getElementById("phoneBtn");
        phoneBtn.onclick = sendPhone;

        var waitingHintEl = document.getElementById("submitWaitingHint"),
            delayCounterEl = document.getElementById("submitDelayCounter");
        waitingHintEl.style.display = "none";

        var counter;
        if (smsDelay >0) {
            var counterFunc = new Counter(smsDelay, delayCounterEl);
            phoneBtn.disabled = true 
            counter = setInterval(counterFunc, 1000);
            setTimeout(waitDelay, smsDelay*1000);
            waitingHintEl.style.display = "block";
        }

        function waitDelay (){
            clearTimeout(counter);
            phoneBtn.disabled = false;
            waitingHintEl.style.display = "none";
        }

        function sendPhone (){
            phoneBtn.disabled = true;
            var phone = phoneInp.value.replace(/^0/, "");
            var credentials =
                { "phone"   : phone
                , "tzshift" : (-1) * (new Date().getTimezoneOffset())
                };
        
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (xhttp.readyState == XMLHttpRequest.DONE) {
                    phoneBtn.disabled = false;
                    if (xhttp.status === 200) {
                        submitEl.style.display="none";
                        var smsDelay = Math.round(parseFloat(xhttp.responseText));
                        confirmPhone(phone, smsDelay);
                    } else {
                        errPlace.innerHTML = "Error: " + xhttp.status + ' - ' + xhttp.statusText + ': ' + xhttp.responseText;
                    }
                }
            }

            xhttp.open("POST", "/login/credentials", true);
            xhttp.setRequestHeader("Content-Type", "application/json");
            xhttp.send(JSON.stringify(credentials));
        }
    }


    var confirmPhone = function(phone, smsDelay){
        confirmEl.style.display="block";
        var errPlace = document.getElementById("errPlace");
        errPlace.innerHTML = "";

        var confirmBtn = document.getElementById("confirmBtn");
        var requestBtn = document.getElementById("resendBtn");
        var newPhoneBtn = document.getElementById("newPhoneBtn");
        var phoneInp = confirmEl.querySelector("input.phone");
        phoneInp.value = phone;
        var codeInp = document.getElementById("codeInp");
        codeInp.value = "";
        confirmBtn.onclick = confirmCode;
        requestBtn.onclick = requestCode;

        newPhoneBtn.onclick = function(){
            newPhoneBtn.disabled = true;
            var promise = logout();
            promise
                .finally(() => {newPhoneBtn.disabled = false;}) 
                .then
                    ( (smsDelay) => {
                        confirmEl.style.display = "none"; 
                        submitPhone(smsDelay)
                        }
                    , (err) => { errPlace.innerHTML = err}
                    )
        }

        var waitingHintEl = document.getElementById("confirmWaitingHint"),
            delayCounterEl = document.getElementById("confirmDelayCounter");
        waitingHintEl.style.display = "none";

        var counter;
        if (smsDelay >0) {
            var counterFunc = new Counter(smsDelay, delayCounterEl);
            resendBtn.style.display = "none"; 
            counter = setInterval(counterFunc, 1000);
            setTimeout(waitDelay, smsDelay*1000);
            waitingHintEl.style.display = "inline-block";
        }

        function waitDelay (){
            clearTimeout(counter);
            resendBtn.style.display = "inline"; 
            waitingHintEl.style.display = "none";
        }


        function requestCode (){
            resendBtn.disabled = true;

            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (xhttp.readyState == XMLHttpRequest.DONE) {
                    resendBtn.disabled = false;
                    if (xhttp.status === 200) {
                        var newSmsDelay = Math.round(parseFloat(xhttp.responseText));
                        confirmPhone(phone, newSmsDelay);
                    } else {
                        errPlace.innerHTML = "Error: " + xhttp.status + ' - ' + xhttp.statusText + ': ' + xhttp.responseText;
                    }
                }
            }
            xhttp.open("POST", "/login/newcode", true);
            xhttp.setRequestHeader("Content-Type", "application/json");
            xhttp.send();
        }

        function confirmCode (){
            confirmBtn.disabled = true;

            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (xhttp.readyState == XMLHttpRequest.DONE) {
                    confirmBtn.disabled = false;
                    if (xhttp.status === 200) {
                        confirmEl.style.display="none";
                        onLogin();
                        callback(phone);
                    } else {
                        errPlace.innerHTML = "Error: " + xhttp.status + ' - ' + xhttp.statusText + ': ' + xhttp.responseText;
                    }
                }
            }

            xhttp.open("POST", "/login/code", true);
            xhttp.setRequestHeader("Content-Type", "application/json");
            xhttp.send(JSON.stringify(codeInp.value));
        }
    }



    var delayStr = document.getElementById("smsDelay").innerHTML,
        delayIni = Math.round(parseFloat(delayStr.slice(0,-1)));



    if (client === null){
        submitPhone(delayIni);
    } else if (client[1] =="CodeSent"){
        confirmPhone(client[0], delayIni);
    }
    else {
        console.log("Error! Already logged in!");
        callback(client[0]);
    }
}
