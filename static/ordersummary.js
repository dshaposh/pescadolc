"use strict"
var button = document.getElementById("sendData");
button.onclick = sendData;
var ordKey = button.value;
var errPlace = document.getElementById("errPlace")
function sendData (){
    button.disabled = true;

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == XMLHttpRequest.DONE) {
            if (xhttp.status === 200) {
                window.location.replace("/protected/html/orders/" + ordKey);
            } else {
                button.disabled = false;
                errPlace.innerHTML = "Error: " + xhttp.status + ' - ' + xhttp.statusText + ': ' + xhttp.responseText;
            }
        }
    }

    xhttp.open("DELETE", "/protected/orders/" + ordKey + "/laststatus", true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send();
}

