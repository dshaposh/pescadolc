"use strict"
var button = document.getElementById("sendData");
button.onclick = sendData;
var errPlace = document.getElementById("errPlace");
function sendData (){
    button.disabled = true;
    var labelSel = document.getElementById("label");
    var qty = document.getElementById("qty").value;
    var note = document.getElementById("note").value;
    var placeSel = document.getElementById("place");
    var itemSel = document.getElementById("item");
    var blceSel = document.getElementById("balance");

    var blcesEl = document.getElementById("baldata");
    var blceData = JSON.parse(blcesEl.innerHTML);

    var blceIndex = blceSel.options[blceSel.selectedIndex].value;
    var from = _.find(blceData, function(el){ return el[1] == blceIndex; })[0][0];
    var doc = {};
    doc.from = [from.wh, from.item, from.token];
    doc.token2 = {};
    doc.token2.code = labelSel.options[labelSel.selectedIndex].value;
    doc.token2.token = 0
    doc.token2.item = parseInt(itemSel.options[itemSel.selectedIndex].value);
    doc.token2.qty = Math.round(parseFloat(qty)*100);
    doc.token2.amount = 0
    doc.token2.wh = {contents: parseInt(placeSel.options[placeSel.selectedIndex].value), tag: "AtPlace"}
    doc.note = note
    console.log(doc);
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == XMLHttpRequest.DONE) {
            if (xhttp.status === 200) {
                window.location.replace("/protected/html/stock/markitem");
            } else {
                button.disabled = false;
                errPlace.innerHTML = "Error: " + xhttp.status + ' - ' + xhttp.statusText + ': ' + xhttp.responseText;
            }
        }
    }

    xhttp.open("POST", "/protected/stock/markitem", true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send(JSON.stringify(doc));
}

