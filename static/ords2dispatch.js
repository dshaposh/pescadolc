"use strict"
var button = document.getElementById("sendData");
button.onclick = sendData;
var chBoxes = document.getElementsByName("order");
var rowsQty = chBoxes.length;
var errPlace = document.getElementById("errPlace")
var courrierSel = document.getElementById("courrier");
var noteInp = document.getElementById("note");
function sendData (){
    button.disabled = true;
    var dspHdr = {};
    dspHdr.courrier = parseInt(courrierSel.options[courrierSel.selectedIndex].value);
    dspHdr.note = noteInp.value;
    var orders = [];

    for(var i=0; i< rowsQty; i++){
        if (chBoxes[i].checked){
            orders.push(parseInt(chBoxes[i].id));
        }
    }
    console.log(orders.length);
    if (orders.length == 0) {
        button.disabled = false;
        errPlace.innerHTML = "Error: Ninguna orden está selecionada";
        return;
    }

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == XMLHttpRequest.DONE) {
            if (xhttp.status === 200) {
                window.location.replace("/protected/html/orders/2dispatch");
            } else {
                button.disabled = false;
                errPlace.innerHTML = "Error " + xhttp.status + ' - ' + xhttp.statusText + ': ' + xhttp.responseText;
            }
        }
    }

    xhttp.open("POST", "/protected/orders/dispatches", true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send(JSON.stringify([dspHdr, orders]));
}

