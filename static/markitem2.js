"use strict"
var button = document.getElementById("sendData");
button.onclick = sendData;
var errPlace = document.getElementById("errPlace");

function sendData (){
    button.disabled = true;
    var fromEl = document.getElementById("from");
    var labelSel = document.getElementById("label");
    var qty = document.getElementById("qty").value;
    var note = document.getElementById("note").value;
    var placeSel = document.getElementById("place");
    var itemSel = document.getElementById("item");
    var stockK = JSON.parse(document.getElementById("stockK").innerHTML);


    var doc = {};
    doc.from =  JSON.parse(fromEl.innerHTML);
    //doc.from = [from.wh, from.item, from.token];
    doc.token2 = {};
    doc.token2.code = labelSel.options[labelSel.selectedIndex].value;
    doc.token2.token = stockK.merchK.tokenK; 
    doc.token2.item = parseInt(itemSel.options[itemSel.selectedIndex].value);
    doc.token2.qty = Math.round(parseFloat(qty)*100);
    doc.token2.amount = 0
    doc.token2.wh = {contents: parseInt(placeSel.options[placeSel.selectedIndex].value), tag: "AtPlace"}
    doc.note = note
    console.log(doc);
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == XMLHttpRequest.DONE) {
            if (xhttp.status === 200) {
                window.location.replace("/protected/html/stock/lots");
            } else {
                button.disabled = false;
                errPlace.innerHTML = "Error: " + xhttp.status + ' - ' + xhttp.statusText + ': ' + xhttp.responseText;
            }
        }
    }

    xhttp.open("POST", "/protected/stock/tolabel", true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send(JSON.stringify(doc));
}


