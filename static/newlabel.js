"use strict"
var button = document.getElementById("sendData");
button.onclick = sendData;
var errPlace = document.getElementById("errPlace");
function sendData (){
    button.disabled = true;
    var name = document.getElementById("label").value;

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == XMLHttpRequest.DONE) {
            if (xhttp.status === 200) {
                window.location.replace("/protected/html/labels");
            } else {
                button.disabled = false;
                errPlace.innerHTML = "Error: " + xhttp.status + ' - ' + xhttp.statusText + ': ' + xhttp.responseText;
            }
        }
    }

    xhttp.open("POST", "/protected/labels", true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send(JSON.stringify(name));
}

