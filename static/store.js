(function(){
"use strict"
var formatNumber = function(num){
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
}

var srchProductDiv = function(el){
    return el.closest(".product");
}

var toggleActive = function(el, isActive){
    //if (isActive && !el.classList.contains("active"))
    if (isActive)
        el.classList.add("active");
    else
        el.classList.remove("active");
}

var InputListener = function(el, updAmount){
    var data = JSON.parse(el.getAttribute("data"));
    var lotK = data[0];
    var qty = data[1];
    var qtyAvlb = data[2];
    var price = data[3];

    var productDiv = srchProductDiv(el);
    var errPlace = productDiv.previousSibling;

    return function() {
        el.disabled = true;
        document.body.style.cursor = "wait";

        var qty2 = Math.round(100*parseFloat(el.value));
        if (qty2 > qtyAvlb) {
            el.value = qty / 100;
            el.disabled=false;
            document.body.style.cursor = "";
            errPlace.innerHTML="La cantidad seleccionada supera la cantidad disponible";
            return;
        }

        var payload = {};
        payload.lotK = lotK;
        var method;
        if (qty2 === 0){
            method = "DELETE";
        } else {
            method = "POST";
            payload.qty = qty2;
        };
            
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState == XMLHttpRequest.DONE) {
                if (xhttp.status === 200) {
                    updAmount(Math.round((qty2-qty)*price/100));
                    qty = qty2;
                    toggleActive(el, qty>0);
                    errPlace.innerHTML="";
                } else {
                    el.value = qty;
                    errPlace.innerHTML = "Error: " + xhttp.status + ' - ' + xhttp.statusText + ': ' + xhttp.responseText;
                }
                el.disabled=false;
                document.body.style.cursor = "";
            }
        }
        xhttp.open(method, "/pcart", true);
        xhttp.setRequestHeader("Content-Type", "application/json");
        xhttp.send(JSON.stringify(payload));
    }

}

//----------------------------------------------------------------
var BoxListener = function(el, updAmount){
    var data = JSON.parse(el.getAttribute("data"));
    var indK = data[0];
    var qty = data[1];
    var isInCart = data[2];
    var price = data[3];

    var productDiv = srchProductDiv(el);
    var errPlace = productDiv.previousSibling;

    var listener =  function() {
        el.removeEventListener("click", listener);
        document.body.style.cursor = "wait";

        var payload = {};
        payload.indK = indK;
        var method = isInCart ? "DELETE" : "POST"

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState == XMLHttpRequest.DONE) {
                if (xhttp.status === 200) {
                    updAmount(Math.round((isInCart ? -1 : 1) * qty * price / 100));
                    isInCart = !isInCart;
                    toggleActive(el, isInCart);
                    errPlace.innerHTML="";
                } else {
                    errPlace.innerHTML = "Error: " + xhttp.status + ' - ' + xhttp.statusText + ': ' + xhttp.responseText;
                }
                el.addEventListener("click", listener);
                document.body.style.cursor = "";
            }
        }
        xhttp.open(method, "/pcart", true);
        xhttp.setRequestHeader("Content-Type", "application/json");
        xhttp.send(JSON.stringify(payload));
    }
    return listener;
}

//----------------------------------------------------------------
var addQtySelectorListener = function(el, updAmount){
    var tmtHandler;
    var data = JSON.parse(el.getAttribute("data"));
    var lotK = data[0];
    var pkgSz = data[4].contents;
    var pkgQty = Math.floor(data[1] / pkgSz);
    var cartQty = pkgQty*pkgSz;
    var price = data[3];
    var maxPkgQty = Math.floor(data[2] / pkgSz);


    var productDiv = srchProductDiv(el);
    var errPlace = productDiv.previousSibling;

    var pkgQtyEl = el.getElementsByClassName("pkgqty")[0];

    var minusListener =  function(){
        pkgQty = pkgQty + ((pkgQty > 0) ? -1 : 0);
        pkgQtyEl.innerHTML = pkgQty;
        clearTimeout(tmtHandler);
        tmtHandler = window.setTimeout(function(){sendRequest(pkgQty)}, 2000);
    }
    var minusBtn = el.getElementsByClassName("minus")[0];
    minusBtn.addEventListener("click", minusListener);

    var plusListener =  function(){
        if (pkgQty >= maxPkgQty) {
            errPlace.innerHTML = "La cantidad seleccionada supera la cantidad disponible";
        } else {
            pkgQty++;
            pkgQtyEl.innerHTML = pkgQty;
            clearTimeout(tmtHandler);
            tmtHandler = window.setTimeout(function(){sendRequest(pkgQty)}, 2000);
        }
    }
    var plusBtn = el.getElementsByClassName("plus")[0];
    plusBtn.addEventListener("click", plusListener);


    var sendRequest = function() {
        minusBtn.removeEventListener("click", minusListener);
        plusBtn.removeEventListener("click", plusListener);

        var payload = {};
        payload.lotK = lotK;
        var method;
        if (pkgQty === 0){
            method = "DELETE";
        } else {
            method = "POST";
            payload.qty = pkgQty*pkgSz;
        };
            
        document.body.style.cursor = "wait";
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState == XMLHttpRequest.DONE) {
                if (xhttp.status === 200) {
                    updAmount(Math.round((pkgQty*pkgSz-cartQty)*price/100));
                    totalEl.innerHTML = formatNumber(total);
                    cartQty=pkgQty * pkgSz;
                    toggleActive(el, cartQty > 0 ? true : false);
                    errPlace.innerHTML="";
                } else {
                    pkgQty=cartQty/pkgSz;
                    pkgQtyEl.innerHTML = pkgQty;
                    errPlace.innerHTML = "Error: " + xhttp.status + ' - ' + xhttp.statusText + ': ' + xhttp.responseText;
                }
                minusBtn.addEventListener("click", minusListener);
                plusBtn.addEventListener("click", plusListener);
                document.body.style.cursor = "";
            }
        }
        xhttp.open(method, "/pcart", true);
        xhttp.setRequestHeader("Content-Type", "application/json");
        xhttp.send(JSON.stringify(payload));
    }
}

//----------------------------------------------------------------

var totalEl = document.getElementById("total");
var total = parseInt(totalEl.innerHTML.replace(/\./g,''));

var cellListener = function (cell){
    var amtContEl = cell.querySelector(".total2"),
        amtEl = amtContEl.firstChild,
        amt = parseInt(amtEl.innerHTML.replace(/\./g,'')),

        updItemAmount = function(dAmount){
            amt += dAmount;
            amtEl.innerHTML = formatNumber(amt);
            if (amt > 0) {
                amtContEl.style = ""
            } else {
                amtContEl.style = "display:none";
            }
        total += dAmount;
        totalEl.innerHTML = formatNumber(total);
        };

    var i;

    var boxes = cell.getElementsByClassName("box");
    var boxesQty = boxes.length;
    for (i=0; i<boxesQty; i++){
        var listener = new BoxListener (boxes[i], updItemAmount);
        boxes[i].addEventListener("click", listener);
    }
    var inputs = cell.getElementsByClassName("input");
    var inputsQty = inputs.length;
    for (i=0; i<inputsQty; i++){
        var listener = new InputListener (inputs[i], updItemAmount);
        inputs[i].addEventListener("change", listener);
    }

    var qtysels = cell.getElementsByClassName("qtyselector");
    var qtyselsQty = qtysels.length;
    for (i=0; i<qtyselsQty; i++){
        addQtySelectorListener (qtysels[i], updItemAmount);
    }
}

//-------------------------------------------------------------------
var cells = document.getElementsByClassName("cell"),
    masonryEl = document.getElementsByClassName("masonry")[0],
    cellsQty = cells.length,
    i = 0,
    totalH = 0;
for(;i < cellsQty; i++){
    totalH += cells[i].offsetHeight;
    cellListener(cells[i]);
}

var cellAvgH = Math.round(totalH / cellsQty),
    cellWidth = cells[0].offsetWidth,
    msnWidth = masonryEl.offsetWidth,
    colQty = Math.floor(msnWidth / cellWidth),
    gutter = colQty > 1 ? Math.round((msnWidth - colQty*cellWidth)/(colQty-1)) : 0;

console.log(gutter);
console.log(cellWidth);


var masonry = function(){
    var w=document.documentElement.clientWidth;
    for(var i=4; i>=1; i--){
        if (i*cellWidth + (i-1)*gutter < w) break;
    };


    console.log(i);       
    var masonryH = Math.round(totalH / i);
    masonryEl.style.height = (cellAvgH + masonryH +"px");
        
}



window.onload = masonry;
window.onresize = masonry;


}());
