"use strict"
var counter = 1;
var rowsCont = document.getElementById("rowsCont");
var compiledTmpl = _.template(document.getElementById("rowtmpl").innerHTML);
var placeSel = document.getElementById("place");
var itemSel = document.getElementById("item");
var qtyInp = document.getElementById("qty");
var amInp = document.getElementById("amount");
var rows = [];
var toShow = {};
var addRow = function (){
    var newRow = {};
    newRow.k = counter;
    newRow.v = {};
    newRow.v.place = _.toNumber(placeSel.options[placeSel.selectedIndex].value);
    newRow.v.item = _.toNumber(itemSel.options[itemSel.selectedIndex].value);
    newRow.v.qty = _.round(_.toNumber(qtyInp.value)*100);
    newRow.v.amount = _.toNumber(amInp.value);
    newRow.place = placeSel.options[placeSel.selectedIndex].text;
    newRow.item = itemSel.options[itemSel.selectedIndex].text;
    rows.push(newRow);
    var nr = document.createElement("tr");
    nr.setAttribute("id", "row" + counter);
    nr.innerHTML = compiledTmpl(newRow);
    rowsCont.appendChild(nr);
    counter += 1;
}   
var delRow = function(i){
    console.log(i);
    _.remove(rows, function(o){ return o.k == i; })
    var row4del = document.getElementById("row" + i);
    rowsCont.removeChild(row4del);
}
var addRowBtn = document.getElementById("addRowBtn");
addRowBtn.onclick = addRow;
var sendBtn = document.getElementById("sendData");
sendBtn.onclick = sendData;
var errPlace = document.getElementById("errPlace");
function sendData (){
    sendBtn.disabled = true;

    var header = {};

    var vendorSel = document.getElementById("vendor")
    header.vendor = _.toNumber(vendorSel.options[vendorSel.selectedIndex].value);
    header.docday = document.getElementById("docday").value
    header.docnum = document.getElementById("docnum").value
    header.note = document.getElementById("note").value

    

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == XMLHttpRequest.DONE) {
            if (xhttp.status === 200) {
                window.location.replace("/protected/html/purchase/new");
            } else {
                sendBtn.disabled = false;
                errPlace.innerHTML = "Error: " + xhttp.status + ' - ' + xhttp.statusText + ': ' + xhttp.responseText;
            }
        }
    }
    xhttp.open("POST", "/protected/purchase", true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    var data2send = [ header, _.map(rows, function (o){ return o.v;})];
    xhttp.send(JSON.stringify(data2send));
}
