"use strict"
var button = document.getElementById("sendData");
var errPlace = document.getElementById("errPlace");
button.onclick = sendData;
function sendData (){
    button.disabled = true;
    var name = document.getElementById("name").value;
    var phone = document.getElementById("phone").value;

    var courrier = {};
    courrier.name = name;
    courrier.phone = phone;
    courrier.ordnum = 1000;

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == XMLHttpRequest.DONE) {
            if (xhttp.status === 200) {
                window.location.replace("/protected/html/courriers");
            } else {
                button.disabled = false;
                errPlace.innerHTML = "Error: " + xhttp.status + ' - ' + xhttp.statusText + ': ' + xhttp.responseText;
            }
        }
    }
    xhttp.open("POST", "/protected/courriers", true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send(JSON.stringify(courrier));
}


