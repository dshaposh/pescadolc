{ mkDerivation, aeson, base, base64-bytestring, bytestring
, containers, contravariant, contravariant-extras, cookie
, data-default-class, entropy, hasql, hasql-transaction
, http-api-data, http-media, lucid, lucid-extras, network
, raw-strings-qq, req, servant, servant-multipart, servant-server
, stdenv, text, time, transformers, vault, vector, wai, warp
}:
mkDerivation {
  pname = "pescadolc";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [
    aeson base base64-bytestring bytestring containers contravariant
    contravariant-extras cookie data-default-class entropy hasql
    hasql-transaction http-api-data http-media lucid lucid-extras
    network raw-strings-qq req servant servant-multipart servant-server
    text time transformers vault vector wai warp
  ];
  homepage = "pescadolc.com";
  license = stdenv.lib.licenses.bsd3;
}
