{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Html.Admin where

import GHC.Int (Int16, Int64)
import GHC.Word (Word32)
import Text.RawString.QQ (r)
import Data.Text as T
import Data.Text.Lazy.Encoding (decodeUtf8)
import Data.Text.Lazy (toStrict)
import Data.Maybe (isJust, fromMaybe, fromJust)
import Data.Time (Day, LocalTime, utcToLocalTime, minutesToTimeZone, TimeZone)
import Data.Time.Clock (UTCTime)
import Data.Vector as V
import Control.Monad (when)
import Lucid.Base (Html, HtmlT, toHtml, makeAttribute)
import Lucid.Html5
import Data.Time.Format(defaultTimeLocale, formatTime)

import Control.Applicative((<|>))
import Control.Monad.IO.Class (liftIO)
import Entities.Common (FromDb, tshow, Key2, DbEntity, Qty, Amount(Amount), unwrapQty, multQty)

import qualified Entities.User as UE
import qualified Entities.Client as CE
import qualified Entities.Order as OE
import qualified Db.Order as OD
import Html.Common (wrapper2, contOrders, contAdm, selectHtml, phoneCheckJs, dblCheckJs)
import Hasql.Connection (Connection)
import Db (execStmt)
import qualified Entities.Inventory as IE
import qualified Db.Inventory as ID
import qualified Sms
import qualified Cms

import Network.Socket (SockAddr, hostAddressToTuple)

smsService :: UE.Session -> (Connection, Sms.Conf) -> SockAddr-> HtmlT IO ()
smsService sess (conn, smsConf) socket = do
    contAdm (Just sess) $ html
    where
    html :: HtmlT IO ()
    html = do
        div_ [] $ toHtml $ tshow socket
        h1_ "Estato del Servicio"
        div_ [class_ "docheader"] $ do
            srvStatus <- liftIO $ Sms.getServiceStatus (conn, smsConf)
            let Sms.ServiceStatus{..} = srvStatus 
            div_ [class_ "docrow"] $ do
                div_ [class_ "label"] "Balance"
                div_ [class_ "value"] $ toHtml $ tshow balance
            div_ [class_ "docrow"] $ do
                div_ [class_ "label"] "Limit"
                div_ [class_ "value"] $ toHtml $ tshow limit
        h1_ "Enviar un SMS de prueba"
        form_ $ do
            div_ [ class_ "formline" ] $ do
                label_ "Telefono"
                input_ [id_ "phone", type_ "text", placeholder_ "telefono", oninput_ phoneCheckJs ] 
            div_ [ class_ "formline" ] $ do
                button_ [id_ "sendSms", type_ "button" ] "Enviar"
        div_ [ id_ "response" ] "" 
        h1_ "Log de mensajes enviados"
        table_ $ do
            thead_ $ do
                tr_ $ do
                    th_ "ip"
                    th_ "Telefono"
                    th_ "Hora"
                    --th_ "data"
            logs <- liftIO $ execStmt conn Sms.getLogs () 
            tbody_ $ V.mapM_ logRow logs
        script_ [ src_ "/static/smsservice.js" ] ("" :: T.Text) 
    
    tz :: TimeZone
    tz = minutesToTimeZone $ fromIntegral $ UE.tzshift sess
    logRow :: (Sms.SmsLog, UTCTime) -> HtmlT IO ()
    logRow (Sms.SmsLog{..}, ts) =
        tr_ $ do
            td_ $ toHtml $ printIp ip
            td_ $ toHtml phone
            td_ $ toHtml $ T.pack $ formatTime defaultTimeLocale "%d/%m %T" $ utcToLocalTime tz ts
            --td_ $ toHtml $ tshow servJson
printIp :: Word32 -> T.Text
printIp ip = tshow a1 <> ":" <> tshow a2 <> ":" <> tshow a3 <> ":" <> tshow a4
    where
    (a1, a2, a3, a4) = hostAddressToTuple ip

delivery :: UE.Session -> Connection -> HtmlT IO ()
delivery sess conn = do
    contAdm (Just sess) html
    where
    html :: HtmlT IO ()
    html = do
        h1_ "Fecha de entrega"
        div_ [ id_ "errDay" ] "" 
        form_ $ do
            div_ [ class_ "formline" ] $ do
                label_ "Precio"
                dlvDay <- liftIO $ execStmt conn OD.getDlvDay ()
                input_ [id_ "day", type_ "date", value_ (tshow dlvDay)] 
            div_ [ class_ "formline" ] $ do
                button_ [id_ "sendDay", type_ "button" ] "Guardar Fecha"

        h1_ "Precio de entrega"
        div_ [ id_ "errPrice" ] "" 
        form_ $ do
            div_ [ class_ "formline" ] $ do
                label_ "Precio"
                input_ [id_ "price", type_ "text" ] 
            div_ [ class_ "formline" ] $ do
                button_ [id_ "sendPrice", type_ "button" ] "Guardar precio"
        h2_ "Historia"
        table_ $ do
            tr_ $ do
                th_ "Precio"
                th_ "La hora"
            prices <- liftIO $ execStmt conn OD.histDlvPrice ()
            tbody_ $ V.mapM_ row prices
        script_ [ src_ "/static/delivery.js" ] ("" :: T.Text) 

    tz :: TimeZone
    tz = minutesToTimeZone $ fromIntegral $ UE.tzshift sess

    row :: (Amount, UTCTime) -> HtmlT IO ()
    row (amount, ts) =
        tr_ $ do
            td_ $ toHtml $ tshow amount
            td_ $ toHtml $ T.pack $ formatTime defaultTimeLocale "%d/%m %T" $ utcToLocalTime tz ts

visitorLog :: UE.Session -> Connection -> HtmlT IO ()
visitorLog sess conn = do
    contAdm (Just sess) html
    where
    html :: HtmlT IO ()
    html = do
        h1_ "La lista de los visitantes"
        table_ $ do
            tr_ $ do
                th_ "ip"
                th_ "User-Agent"
                th_ "La hora"
                th_ "# de productos"
                th_ "Valor"
            log <- liftIO $ execStmt conn CE.getLog ()
            tbody_ $ V.mapM_ row log

    tz :: TimeZone
    tz = minutesToTimeZone $ fromIntegral $ UE.tzshift sess

    row :: (CE.VisitorLog, UTCTime, Int64, Int64) -> HtmlT IO ()
    row (logEntry, ts, cnt, amt) =
        tr_ $ do
            td_ $ toHtml $ printIp ip
            td_ $ toHtml userAgent 
            td_ $ toHtml $ T.pack $ formatTime defaultTimeLocale "%d/%m %T" $ utcToLocalTime tz ts
            td_ $ toHtml $ tshow cnt
            td_ $ toHtml $ tshow amt
        where
        CE.VisitorLog{..} = logEntry

cms :: Maybe UE.Session -> Connection -> HtmlT IO ()
cms mSess conn = do
    contAdm mSess $ html
    where
    html :: HtmlT IO ()
    html = do
        div_ [ id_ "errPlace" ] ""
        h1_ "La Gestion del contenido statico"
        form_ $ do
            div_ [ class_ "formline" ] $ do
                label_ "Nombre"
                let resources :: V.Vector Cms.Resource = V.fromList $ enumFrom minBound
                let rsrOpt = V.toList $ V.map (\r -> (tshow r, tshow r)) resources
                selectHtml [id_ "name"] rsrOpt ""
            div_ [ class_ "formline" ] $ do
                label_ "Archivo"
                input_ [type_ "file", id_ "content"]
            div_ [ class_ "formline" ] $ do
                button_ [type_ "button", id_ "sendData"] "Guardar"
        script_ [ src_ "/static/cms.js" ] ("" :: T.Text)
