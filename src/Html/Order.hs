{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE DuplicateRecordFields #-}

module Html.Order where

import GHC.Int (Int64)
import Text.RawString.QQ (r)
import Data.Text as T
import Data.Text.Lazy.Encoding (decodeUtf8)
import Data.Text.Lazy (toStrict)
import Data.Maybe (isJust, fromMaybe, fromJust)
import Data.Time (Day, LocalTime, utcToLocalTime, minutesToTimeZone, TimeZone)
import Data.Time.Clock (UTCTime)
import Data.Vector as V
import Control.Monad (when)
import Lucid.Base (Html, HtmlT, toHtml, makeAttribute)
import Lucid.Html5
import Data.Aeson (toEncoding, encode)
import Data.Time.Format(defaultTimeLocale, formatTime)

import Control.Applicative((<|>))
import Control.Monad.IO.Class (liftIO)
import Entities.Common (FromDb, tshow, Key2, DbEntity, Qty, Amount(Amount), unwrapQty, multQty)
import qualified Entities.Order as OE
import qualified Entities.Client as CE
import qualified Entities.User as UE
import qualified Db.Order as OD
import Html.Common (wrapper2, contOrders, contAdm, selectHtml, phoneCheckJs, dblCheckJs)
import Hasql.Connection (Connection)
import Db (execStmt)
import qualified Entities.Inventory as IE
import qualified Db.Inventory as ID
import qualified Html.Inventory as IH


courriers :: Maybe UE.Session -> Connection -> HtmlT IO ()
courriers mSess conn = do
    list <- liftIO $ execStmt conn OD.getCourriers False
    contAdm mSess $ html list
    where
    html :: V.Vector (FromDb OE.Courrier) -> HtmlT IO ()
    html items = do
        h1_ "Mensajeros"
        table_ $ do
            tr_ $ do
                th_ "Nombre"
                th_ "Teléfono"
            tbody_ $ V.mapM_ row items
        where
        row :: FromDb OE.Courrier-> HtmlT IO ()
        row itemDb =
            tr_ $ do
                td_ $ toHtml name
                td_ $ toHtml phone
            where
            (item, key, tag) = itemDb
            OE.Courrier{..} = item
    

courrierNew :: Maybe UE.Session -> Connection -> HtmlT IO ()
courrierNew mSess conn = do
    contAdm mSess $ html
    where
    html :: HtmlT IO ()
    html = do
        div_ [ id_ "errPlace" ] ""
        h1_ "Crear nuevo mensajero"
        form_ $ do
            div_ [ class_ "formline" ] $ do
                label_ "Nombres"
                input_ [type_ "text", id_ "name"]
            div_ [ class_ "formline" ] $ do
                label_ "Teléfono"
                input_ [type_ "text", id_ "phone", oninput_ phoneCheckJs]
            div_ [ class_ "formline" ] $ do
                button_ [type_ "button", id_ "sendData"] "Guardar"
        script_ [ src_ "/static/newcourrier.js" ] ("" :: T.Text)


--------------------------------------------------------------------------            
orders :: Maybe UE.Session -> Connection -> T.Text -> HtmlT IO ()
orders mSess conn pat = do
    list <- liftIO $ execStmt conn OD.ordersByPhone pat
    contOrders mSess $ html list
    where
    html :: V.Vector (FromDb OE.OrderHdr) -> HtmlT IO ()
    html orders = do
        h1_ "Consultar a los órdenes de los clientes"
        form_ [method_ "get", action_ "/protected/html/orders" ] $ do
            div_ [class_ "formline"] $ do
                label_ "Teléfono"
                input_ [ name_ "phone", value_ pat ]
            div_ [class_ "formline"] $ do
                button_ "Buscar"
        table_ $ do
            tr_ $ do
                th_ "Código"
                th_ "Teléfono"
                th_ "Fecha"
                th_ "Status"
                th_ "Valor"
                th_ "Nombre"
                th_ "Dirección"
            tbody_ $ V.mapM_ row orders
        where
        row :: FromDb OE.OrderHdr-> HtmlT IO ()
        row (order, key, (status, _)) =
            tr_ $ do
                td_ $ a_ [ href_ $ "/protected/html/orders/" <> (tshow key)] $ toHtml $ tshow key
                td_ $ a_ [ href_ $ "/protected/html/orders?phone=" <> (tshow phone)] $ toHtml $ tshow phone
                td_ $ toHtml $ tshow dlvDay
                td_ $ toHtml $ tshow status
                td_ $ toHtml $ tshow total
                td_ $ toHtml $ name
                td_ $ toHtml $ address
            where
            OE.OrderHdr{..} = order


--------------------------------------------------------------------------            
orderSummary :: Maybe UE.Session -> Connection -> Key2 OE.OrderHdr -> HtmlT IO ()
orderSummary mSess conn ordK = do
    list <- liftIO $ execStmt conn OD.ordHistory ordK
    contOrders mSess $ html list
    where
    html :: V.Vector (OE.OKey, UTCTime, OE.Status) -> HtmlT IO ()
    html orders = do
        h1_ "Resumen de la orden"
        div_ [ id_ "errPlace" ] "" 
        (order, _, (status, docK)) <- liftIO $ execStmt conn OD.orderHdrByK ordK
        let OE.OrderHdr{..} = order
        div_ [class_ "docheader"] $ do
            div_ [class_ "docrow"] $ do
                div_ [class_ "label"] "Teléfono"
                div_ [class_ "value"] $ toHtml $ tshow phone
            div_ [class_ "docrow"] $ do
                div_ [class_ "label"] "Nombre"
                div_ [class_ "value"] $ toHtml name
            div_ [class_ "docrow"] $ do
                div_ [class_ "label"] "Dirección"
                div_ [class_ "value"] $ toHtml address
            div_ [class_ "docrow"] $ do
                div_ [class_ "label"] "Fecha"
                div_ [class_ "value"] $ toHtml $ tshow dlvDay
            div_ [class_ "docrow"] $ do
                div_ [class_ "label"] "Costo de entrega"
                div_ [class_ "value"] $ toHtml $ tshow dlvCost
            div_ [class_ "docrow"] $ do
                div_ [class_ "label"] "Valor total"
                div_ [class_ "value"] $ toHtml $ tshow total
            div_ [class_ "docrow"] $ do
                div_ [class_ "label"] "Estatus"
                div_ [class_ "value"] $ toHtml $ tshow status

        pcRows <- liftIO $ execStmt conn ID.ordRowsStmt ordK
        div_ [ id_ "pcart"] $ V.mapM_ pCartRow pcRows

        h2_ "Historia de la order"
        form_ $ do
            button_ [id_ "sendData", type_ "button", value_ (tshow ordK) ] "Borrar último estatus"
        table_ $ do
            tr_ $ do
                th_ "Código"
                th_ "La hora"
                th_ "Estatus"
            tbody_ $ V.mapM_ histRow orders
        script_ [ src_ "/static/ordersummary.js"] ("" :: T.Text)

        where
        pCartRow :: (IE.OrderRow2, IE.Item, T.Text) -> HtmlT IO ()
        pCartRow (orow, item, uname) =
            div_ [class_ "pitem", makeAttribute "merchkey" jsonK ] $ do
                div_ [ class_ "prow1" ] $ do
                    div_ [ class_ "pname" ] $ toHtml xName
                    div_ [ class_ "pprice" ] $ toHtml $ (tshow price) <> "/" <> uname
                    div_ [ class_ "pqty" ] $ toHtml (tshow qty <> uname)
                    div_ [ class_ "pamount" ] $ toHtml $ tshow amount
                div_ [ class_ "pinstr" ] $ toHtml instr
            where
            price = Amount $ (fromIntegral amount) * 100 `div` (fromIntegral qty)
            IE.OrderRow2{..} = orow
            IE.MerchLot2 lotK qty = merch
            IE.Item{..} = item
            jsonK = toStrict $ decodeUtf8 $ fromJust $ (fmap (encode . IE.AnyMerch . IE.MerchInd2) indK) <|> (Just $ encode $ merch)
            xName = case size of
                IE.NoSize -> name
                IE.Fixed s -> name <> " " <> (tshow pkQty) <> "ea x" <> (tshow $ (fromIntegral s) / 100) <> uname
                    where
                    pkQty = (unwrapQty qty) `div` (unwrapQty s)
                IE.Variable -> name <> " 1ud x " <> (tshow qty) <> uname

        histRow :: (OE.OKey, UTCTime, OE.Status) -> HtmlT IO ()
        histRow (key, tstamp, status) =
            tr_ $ do
                td_ $ toHtml $ tshow key
                td_ $ toHtml $ T.pack $ formatTime defaultTimeLocale "%d/%m %T" tstamp
                td_ $ toHtml $ tshow status

----------------------------------------------------------------------------
ords2Pay :: Maybe UE.Session -> Connection -> HtmlT IO ()
ords2Pay mSess conn = do
    list <- liftIO $ execStmt conn OD.ords2Pay ()
    contOrders mSess $ html list
    where
    html :: V.Vector (FromDb OE.OrderHdr) -> HtmlT IO ()
    html orders = do
        h1_ "Las ordenes pendientes por recibir el pago"
        div_ [ id_ "errPlace" ] "" 
        form_ $ do
            div_ [class_ "formline"] $ do
                button_ [ id_ "sendData", type_ "button" ] "Guardar el pago"
        table_ $ do
            tr_ $ do
                th_ "Código"
                th_ "Teléfono"
                th_ "Fecha"
                th_ "Valor"
                th_ "Pagado?"
            tbody_ $ V.mapM_ row orders
        script_ [src_ "/static/ords2pay.js" ] ("" :: T.Text)
        where
        row :: FromDb OE.OrderHdr-> HtmlT IO ()
        row (order, key, (status, _)) =
            tr_ $ do
                td_ $ a_ [ href_ $ "/protected/html/orders/" <> (tshow key)] $ toHtml $ tshow key
                td_ $ a_ [ href_ $ "/protected/html/orders?phone=" <> (tshow phone)] $ toHtml $ tshow phone
                td_ $ toHtml $ tshow dlvDay
                td_ $ toHtml $ tshow total
                td_ $ input_ [type_ "radio", name_ "order", id_ (tshow key) ]
            where
            OE.OrderHdr{..} = order

ords2Prepare :: Maybe UE.Session -> Connection -> HtmlT IO ()
ords2Prepare mSess conn = do
    list <- liftIO $ execStmt conn OD.ords2PrpStmt ()
    contOrders mSess $ html list
    where
    html :: V.Vector (FromDb OE.OrderHdr, OE.OrderRow 'IE.Lot3, Qty) -> HtmlT IO ()
    html orders = do
        h1_ "Las ordenes pendientes por preparar (sin articulos individuales)"
        div_ [ id_ "errPlace" ] "" 
        table_ $ do
            tr_ $ do
                th_ "Código"
                th_ "Teléfono"
                th_ "Tiempo en espera"
                th_ "Articulo"
                th_ "Lot"
                th_ "Cantidad total"
                th_ "Preparado"
                th_ "Pendiente por preparar"
            tbody_ $ V.mapM_ row orders
        where
        row :: (FromDb OE.OrderHdr, OE.OrderRow 'IE.Lot3, Qty) -> HtmlT IO ()
        row ((order, hdrK, (status, _)), orow, prpqty) =
            tr_ $ do
                td_ $ a_ [ href_ $ "/protected/html/orders/" <> (tshow hdrK)] $ toHtml $ tshow hdrK 
                td_ $ a_ [ href_ $ "/protected/html/orders?phone=" <> (tshow phone)] $ toHtml $ tshow phone
                td_ $ toHtml $ tshow dlvDay
                td_ $ "TBD-item"
                td_ $ "TBD-lot"
                td_ $ toHtml $ tshow qty
                td_ $ toHtml $ tshow prpqty
                td_ $ a_ [ href_ $ "/protected/html/orders/rows/" <> (tshow rowK)]
                        $ toHtml $ tshow $ qty-prpqty
            where
            OE.OrderHdr{..} = order
            OE.OrderRow merch amount instr rowK = orow
            IE.MerchLot2 merchK qty = merch
            IE.MerchLotK itemK lotK = merchK

----------------------------------------------------------------------------
orderRow :: Maybe UE.Session -> Connection -> Key2 (OE.OrderRow 'IE.Lot3) -> HtmlT IO ()
orderRow mSess conn rowK = do
    contOrders mSess $ html
    where
    html :: HtmlT IO ()
    html = do
        orow <- liftIO $ execStmt conn OD.getOrderRowByK rowK
        let (OE.OrderRow merch amount instr rowK, status) = orow
            IE.MerchLot2 merchK qty = merch
        h1_ "WorkOrder para pedido del cliente"
        div_ [id_ "errPlace"] ""
        div_ [class_ "docheader"] $ do
            div_ [class_ "docrow"] $ do
                div_ [class_ "label"] "Cantidad"
                div_ [class_ "value"] $ toHtml $ tshow qty
            div_ [class_ "docrow"] $ do
                div_ [class_ "label"] "Valor"
                div_ [class_ "value"] $ toHtml $ tshow amount
            div_ [class_ "docrow"] $ do
                div_ [class_ "label"] "Preparación"
                div_ [class_ "value"] $ toHtml instr
        form_ $ do
            div_ [ class_ "formline" ] $ do
                label_ "Lugar - not used"
                places <- liftIO $ execStmt conn ID.getPlaces True
                let placeOpt = V.toList $ V.map (\(IE.Place name _, key, _)
                            -> (name, T.pack $ show key)) places
                selectHtml [id_ "place"] placeOpt ""
            div_ [ class_ "formline" ] $ do
                button_ [type_ "button", id_ "sendData" ] "Guardar"
        
        balances <- liftIO $ execStmt conn ID.stockMerchLot merchK
        picklist <- liftIO $ execStmt conn OD.getPickList rowK
        table_ $ do
            thead_ $ do
                tr_ $ do
                    th_ $ "Lugar"
                    th_ $ "Cantidad disponible"
                    th_ $ "Cantidad asignada"
                    th_ $ "Cantidad seleccionada"
            tbody_ [ id_ "picklist" ] $ V.mapM_ (row picklist) balances
        script_ [ type_ "text/data", id_ "rowK" ] $ toHtml $ tshow rowK
        script_ [src_ "/static/orderrow.js" ] ("" :: T.Text)
        where
        row :: V.Vector (Key2 IE.Place, Qty) -> (IE.Stock3 'IE.Lot3, T.Text) -> HtmlT IO ()
        row picklist ((IE.StockLot stockK qty cost), plname) = do
            let (IE.StockLotK _ placeK) = stockK
                asignedQty = fromMaybe 0 $ fmap snd $ V.find (\(p, _) -> p == placeK) picklist
            tr_ $ do
                td_ $ toHtml plname
                td_ $ toHtml $ tshow qty
                td_ $ toHtml $ tshow asignedQty
                td_ $ input_ [ type_ "text", id_ (tshow placeK) ]

        


----------------------------------------------------------------------------
ords2Deliver :: Maybe UE.Session -> Connection -> HtmlT IO ()
ords2Deliver mSess conn = do
    list <- liftIO $ execStmt conn OD.ords2Deliver ()
    contOrders mSess $ html list
    where
    html :: V.Vector (FromDb OE.OrderHdr, T.Text) -> HtmlT IO ()
    html orders = do
        h1_ "Las ordenes pendientes por entregar al cliente"
        div_ [ id_ "errPlace" ] "" 
        button_ [ id_ "sendData" ] "Guardar la entrega"
        table_ $ do
            tr_ $ do
                th_ "Código"
                th_ "Teléfono"
                th_ "Nombre"
                th_ "Mensajero"
                th_ "Fecha"
                th_ "Valor"
                th_ "Seleccionar"
            tbody_ $ V.mapM_ row orders
        script_ [src_ "/static/ords2deliver.js" ] ("" :: T.Text)
        where
        row :: (FromDb OE.OrderHdr, T.Text) -> HtmlT IO ()
        row ((order, key, (status, docK)), courrier) =
            tr_ $ do
                td_ $ a_ [ href_ $ "/protected/html/orders/" <> (tshow key)] $ toHtml $ tshow key
                td_ $ a_ [ href_ $ "/protected/html/orders?phone=" <> (tshow phone)] $ toHtml $ tshow phone
                td_ $ toHtml $ name
                td_ $ toHtml $ courrier
                td_ $ toHtml $ tshow dlvDay
                td_ $ toHtml $ tshow total
                td_ $ input_ [type_ "radio", name_ "order", id_ (tshow docK) ]
            where
            OE.OrderHdr{..} = order

----------------------------------------------------------------------------------
ords2Dispatch :: Maybe UE.Session -> Connection -> HtmlT IO ()
ords2Dispatch mSess conn = do
    list <- liftIO $ execStmt conn OD.ords2Dispatch ()
    contOrders mSess $ html list
    where
    html :: V.Vector (FromDb OE.OrderHdr) -> HtmlT IO ()
    html orders = do
        h1_ "Las ordenes pendientes por despachar"
        div_ [ id_ "errPlace" ] "" 
        form_ $ do
            div_ [ class_ "formline" ] $ do
                crrs <- liftIO $ execStmt conn OD.getCourriers False
                let crrsOpt = V.toList $ V.map (\((OE.Courrier name _ _), key, _) -> (name, tshow key)) crrs
                label_ "Mensajero"
                selectHtml [id_ "courrier" ] crrsOpt ""
            div_ [ class_ "formline" ] $ do
                label_ "Comentario"
                input_ [type_ "text", id_ "note"]
            div_ [ class_ "formline" ] $ do
                button_ [type_ "button", id_ "sendData"] "Guardar"
        table_ $ do
            tr_ $ do
                th_ "Código"
                th_ "Teléfono"
                th_ "Fecha"
                th_ "Valor"
                th_ "Despachar?"
                th_ "Dirección"
            tbody_ $ V.mapM_ row orders
        script_ [src_ "/static/ords2dispatch.js" ] ("" :: T.Text)
        where
        row :: FromDb OE.OrderHdr-> HtmlT IO ()
        row (order, key, (status, pmtK)) =
            tr_ $ do
                td_ $ a_ [ href_ $ "/protected/html/orders/" <> (tshow key)] $ toHtml $ tshow key
                td_ $ a_ [ href_ $ "/protected/html/orders?phone=" <> (tshow phone)] $ toHtml $ tshow phone
                td_ $ toHtml $ tshow dlvDay
                td_ $ toHtml $ tshow total
                td_ $ input_ [type_ "checkbox", name_ "order", id_ (tshow pmtK) ]
                td_ $ toHtml $ address
            where
            OE.OrderHdr{..} = order

----------------------------------------------------------------------------------
payments :: Maybe UE.Session -> Connection -> HtmlT IO ()
payments mSess conn = do
    list <- liftIO $ execStmt conn OD.paymentsStmt ()
    contOrders mSess $ html list
    where
    html :: V.Vector (Key2 (OE.Order 'OE.Paid), FromDb OE.OrderHdr, Maybe Int64) -> HtmlT IO ()
    html pmts = do
        h1_ "Los pagos recibidos"
        table_ $ do
            thead_ $ do
                tr_ $ do
                    th_ "Código"
                    th_ "Teléfono"
                    th_ "Fecha"
                    th_ "Valor"
            tbody_ $ V.mapM_ row pmts
        where
        row :: (Key2 (OE.Order 'OE.Paid), FromDb OE.OrderHdr, Maybe Int64) -> HtmlT IO ()
        row (pmtK, (hdr, hdrK, (status, lastK)), mTag) =
            tr_ (if isJust mTag then [class_ "deleted"] else []) $ do
                td_ $ a_ [ href_ $ "/protected/html/orders/" <> (tshow hdrK)] $ toHtml $ tshow hdrK 
                td_ $ a_ [ href_ $ "/protected/html/orders?phone=" <> (tshow phone)] $ toHtml $ tshow phone
                td_ $ toHtml $ tshow dlvDay
                td_ $ toHtml $ tshow total
            where
            OE.OrderHdr{..} = hdr

----------------------------------------------------------------------------------
dispatches :: Maybe UE.Session -> Connection -> Day -> Day -> HtmlT IO ()
dispatches mSess conn dayFrom dayTo = do
    list <- liftIO $ execStmt conn OD.dspHdrsByDates (dayFrom, dayTo)
    contOrders mSess $ html list
    where
    html :: V.Vector (DbEntity OE.DspHdr) -> HtmlT IO ()
    html dsps = do
        h1_ "Los despachos realizados"
        form_ [ method_ "get", action_ "/protected/html/orders/dispatches" ] $ do
            input_ [type_ "date", value_ (tshow dayTo), name_ "dayTo"]
            div_ [class_ "formline"] $ do
                label_ "Desde"
                input_ [type_ "date", value_ (tshow dayFrom), name_ "dayFrom"]
            div_ [class_ "formline"] $ do
                label_ "Hasta"
                input_ [type_ "date", value_ (tshow dayTo), name_ "dayTo"]
            div_ [class_ "formline"] $ do
                button_ "Actualizar"
        table_ $ do
            thead_ $ do
                tr_ $ do
                    th_ "Código"
                    th_ "Fecha"
                    th_ "Mensajero"
                    th_ "Valor"
                    th_ "Ordenes#"
            tbody_ $ V.mapM_ row dsps
        where
        row :: (DbEntity OE.DspHdr) -> HtmlT IO ()
        row (hdr, hdrK, (amount, qty)) =
            tr_ $ do
                td_ $ a_ [ href_ $ "/protected/html/orders/dispatches/" <> (tshow hdrK)] $ toHtml $ tshow hdrK 
                td_ "tbd"
                td_ $ toHtml $ tshow courrier
                td_ $ toHtml $ tshow amount
                td_ $ toHtml $ tshow qty 
            where
            OE.DspHdr{..} = hdr
