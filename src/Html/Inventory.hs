{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE DataKinds #-}

module Html.Inventory where

import Text.RawString.QQ (r)
import Data.Time.Calendar(Day)
import Data.Maybe (fromJust, isJust)
import Data.Text.Lazy.Encoding (decodeUtf8)
import Data.Text.Lazy (toStrict)
import qualified Data.Text as T
import qualified Data.Text.Encoding as T
import qualified Data.Vector as V
import qualified Data.List as L
import Lucid.Base (Html, HtmlT, toHtml, makeAttribute)
import Lucid.Html5
import Lucid.PreEscaped(preEscapedByteString)
import Data.Aeson (toEncoding, encode)
import Data.Aeson.Encoding (encodingToLazyByteString)

import Control.Monad (when)
import Control.Monad.IO.Class (liftIO)
import Control.Applicative((<|>))
import Entities.Common (FromDb, TagSC(..), Key, Qty, unwrapQty, unwrapKey, multQty, Amount(Amount), tshow, Key2(Key2), prettyPrice) -- , isClosed)
--import qualified Authentication as A
import qualified Entities.User as UE
import qualified Entities.Client as CE
import qualified Entities.Inventory as IE
import qualified Db.Inventory as ID
import Html.Common (wrapper2, selectHtml, intCheckJs, dblCheckJs, contAdm, contInv)
import Hasql.Connection (Connection)
import Db (execStmt)


labels :: Maybe UE.Session -> Connection -> HtmlT IO ()
labels mSess conn = do
    contAdm mSess $ html
    where
    html :: HtmlT IO ()
    html = do
        labels <- liftIO $ execStmt conn ID.getLabelsStmt ()
        h1_ "Presintos"
        div_ [ id_ "errPlace" ] ""
        form_ $ do
            div_ [ class_ "formline" ] $ do
                button_ [type_ "button", id_ "sendData"] "Borrar"
        table_ $ do
            tr_ $ do
                th_ "Presinto"
                th_ "Seleccionar"
            tbody_ $ V.mapM_ row labels
        script_ [src_ "/static/labels.js" ] ("" :: T.Text)
        where
        row :: T.Text -> HtmlT IO ()
        row txt = tr_ $ do
            td_ $ toHtml txt
            td_ $ input_ [type_ "radio", name_ "label", id_ txt ]

labelNew :: Maybe UE.Session -> Connection -> HtmlT IO ()
labelNew mSess conn = do
    contAdm mSess $ html
    where
    html :: HtmlT IO ()
    html = do
        h1_ "Nuevo Presinto"
        div_ [ id_ "errPlace" ] ""
        form_ $ do
            div_ [ class_ "formline" ] $ do
                label_ "Presinto"
                input_ [type_ "text", id_ "label"]
            div_ [ class_ "formline" ] $ do
                button_ [type_ "button", id_ "sendData"] "Guardar"
        script_ [ src_ "/static/newlabel.js" ] ("" :: T.Text)
------------------------------------------------------------------------------
species :: Maybe UE.Session -> Connection -> HtmlT IO ()
species mSess conn = do
    species <- liftIO $ execStmt conn ID.getSpecies False
    contAdm mSess $ html  species
    where
    html :: V.Vector (FromDb IE.Specie) -> HtmlT IO ()
    html species = do
        h1_ "Tipos de pescado"
        table_ $ do
            tr_ $ do
                th_ "id"
                th_ "Nombre"
                th_ "Unidad"
                th_ "Precio"
            tbody_ $ V.mapM_ row species
        where
        row :: FromDb IE.Specie -> HtmlT IO ()
        row specieDb =
            tr_ $ do
                td_ $ a_ [href_ ("/protected/html/species/" <> (tshow key))] $ toHtml $ tshow key
                td_ $ toHtml name
                td_ $ toHtml uomName
                td_ $ toHtml $ tshow price
            where
            (specie, key, (uomName, _, _)) = specieDb
            IE.Specie{..} = specie

specieNew :: Maybe UE.Session -> Connection -> HtmlT IO ()
specieNew mSess conn = do
    contAdm mSess $ html
    where
    html :: HtmlT IO ()
    html = do
        div_ [ id_ "errPlace" ] ""
        h1_ "Crear nuevo especie"
        form_ $ do
            div_ [ class_ "formline" ] $ do
                label_ "Nombre"
                input_ [type_ "text", id_ "name"]
            div_ [ class_ "formline" ] $ do
                label_ "Unidad de Medida"
                uoms <- liftIO $ execStmt conn ID.getUoms False
                let uomOpt = V.toList $ V.map (\(IE.Uom name _, key, _) -> (name, tshow key)) uoms
                selectHtml [id_ "uom"] uomOpt ""
            div_ [ class_ "formline" ] $ do
                label_ "Precio de venta"
                input_ [type_ "text", id_ "price", oninput_ intCheckJs]
            div_ [ class_ "formline", style_ "display: none" ] $ do
                label_ "Imagen"
                input_ [type_ "file", id_ "image"]
            div_ [ class_ "formline" ] $ do
                button_ [type_ "button", id_ "sendData"] "Guardar"
        script_ [ src_ "/static/newspecie.js" ] ("" :: T.Text)

specie :: Maybe UE.Session -> Connection -> Key2 IE.Specie -> HtmlT IO ()
specie mSess conn specieK = do
    contAdm mSess $ html
    where
    html :: HtmlT IO ()
    html = do
        specieDb <- liftIO $ execStmt conn ID.getSpecieById specieK
        let (specie, _, (uomName, _, _)) = specieDb
        let IE.Specie{..} = specie
        h1_ "Revisar especie"
        div_ [ id_ "errPlace" ] ""
        input_ [id_ "specieK", hidden_ "hidden", value_ $ tshow specieK ]
        div_ [class_ "docheader"] $ do
            div_ [ class_ "docrow" ] $ do
                button_ [type_ "button", id_ "deleteSpecie" ] "Borrar"
            div_ [ class_ "docrow" ] $ do
                div_ [class_ "label"] "Nombre"
                div_ [class_ "value"] $ toHtml name
            div_ [ class_ "docrow" ] $ do
                div_ [class_ "label"] "Unidad de medida"
                div_ [class_ "value"] $ toHtml uomName
            div_ [ class_ "docrow" ] $ do
                div_ [class_ "label"] "Precio"
                div_ [class_ "value"] $ input_ [type_ "text", id_ "price", value_ (tshow price)]
            div_ [ class_ "docrow" ] $ do
                button_ [type_ "button", id_ "sendPrice" ] "Guardar precio"
            div_ [ class_ "docrow" ] $ do
                div_ [class_ "label"] "Descripción"
                div_ [class_ "value"] $ textarea_ [id_ "description", rows_ "18", cols_ "60"] ""
            div_ [ class_ "docrow" ] $ do
                div_ [class_ "label"] "Imagen"
                div_ [class_ "value"] $ input_ [type_ "file", id_ "image"]
            div_ [ class_ "docrow" ] $ do
                button_ [type_ "button", id_ "sendImage" ] "Guardar imagen/descripción"
            div_ $ img_ [src_ $ "/species/" <> (tshow specieK) <> "/image" ]
        script_ [ src_ "/static/specie.js" ] ("" :: T.Text)

------------------------------------------------------------------------------
cuttingsPage :: Maybe UE.Session -> Connection -> HtmlT IO ()
cuttingsPage mSess conn = do
    cuttings <- liftIO $ execStmt conn ID.getCuttings False
    contAdm mSess $ html cuttings
    where
    html :: V.Vector (FromDb IE.Cutting) -> HtmlT IO ()
    html cuttings = do
        h1_ "Tipos de corte"
        table_ $ do
            tr_ $ do
                th_ "id"
                th_ "Nombre"
            tbody_ $ V.mapM_ row cuttings
        where
        row :: FromDb IE.Cutting -> HtmlT IO ()
        row cuttingDb =
            tr_ $ do
                td_ $ toHtml $ T.pack $ show key
                td_ $ toHtml name
            where
            (cutting, key, tag) = cuttingDb
            IE.Cutting{..} = cutting

------------------------------------------------------------------------------
items :: Maybe UE.Session -> Connection -> HtmlT IO ()
items mSess conn = do
    itemsExt <- liftIO $ execStmt conn ID.getItems False 
    contAdm mSess $ html itemsExt
    where
    html :: V.Vector (FromDb IE.Item, IE.ItemDescr) -> HtmlT IO ()
    html items = do
        h1_ "Articulos"
        table_ $ do
            tr_ $ do
                th_ "id"
                th_ "Especie"
                th_ "Preparación"
                th_ "Nombre"
                th_ "Se mide en"
                th_ "Tamaño"
                th_ "Ajuste de Precio"
                th_ "Precio Final"
            tbody_ $ V.mapM_ row items
        where
        row :: (FromDb IE.Item, IE.ItemDescr) -> HtmlT IO ()
        row (itemDb, (spnam, cutnam, uonam, price)) =
            tr_ [ class_ $ if isClosed then "deleted" else ""] $ do
                td_ $ a_ [ href_ $ "/protected/html/items/" <> (tshow key) ] $ toHtml $ T.pack $ show key
                td_ $ toHtml spnam
                td_ $ toHtml cutnam
                td_ $ toHtml name
                td_ $ toHtml uonam
                td_ $ toHtml $ T.pack $ show size
                td_ $ toHtml $ tshow $ (fromIntegral markup) / 100
                td_ $ toHtml $ tshow $ price
            where
            (item, key, tag) = itemDb
            IE.Item{..} = item
            TagSC{..} = tag
    
item :: Maybe UE.Session -> Connection -> Key2 IE.Item -> HtmlT IO ()
item mSess conn itemK = do
    contAdm mSess $ html
    where
    html :: HtmlT IO ()
    html = do
        itemDescr <- liftIO $ execStmt conn ID.getItemById itemK
        let ((item, _, _), (spnam, cutnam, uomnam, spprice)) = itemDescr
            IE.Item{..} = item
        h1_ "Resumen de Articulo"
        input_ [id_ "itemK", hidden_ "hidden", value_ $ tshow itemK ]
        div_ [ id_ "errPlace" ] ""
        div_ [class_ "docheader"] $ do
            div_ [class_ "docrow"] $ do
                div_ [class_ "label"] "Especie"
                div_ [class_ "value"] $ toHtml spnam 
            div_ [class_ "docrow"] $ do
                div_ [class_ "label"] "Presentación"
                div_ [class_ "value"] $ toHtml cutnam
            div_ [class_ "docrow"] $ do
                div_ [class_ "label"] "Desripción"
                div_ [class_ "value"] $ toHtml name
            div_ [class_ "docrow"] $ do
                div_ [class_ "label"] "Unidad de medida"
                div_ [class_ "value"] $ toHtml uomnam
            div_ [class_ "docrow"] $ do
                div_ [class_ "label"] "Dividible"
                div_ [class_ "value"] $ toHtml $ (tshow size) <> uomnam
            div_ [class_ "docrow"] $ do
                div_ [class_ "label"] "Ajuste de Precio"
                div_ [class_ "value"] $ input_ [ id_ "markupInp", value_ $ tshow $ (fromIntegral markup) / 100]
            div_ [class_ "docrow"] $ do
                button_ [type_ "button", id_ "saveMarkup" ] "Guardar markup"
                button_ [type_ "button", id_ "deleteItem" ] "Borrar"
        script_ [ src_ "/static/item.js" ] ("" :: T.Text)

--------------------------------------------------------------------------
vendors :: Maybe UE.Session -> Connection -> HtmlT IO ()
vendors mSess conn = do
    vendors <- liftIO $ execStmt conn ID.getVendors False
    contAdm mSess $ html vendors
    where
    html :: V.Vector (FromDb IE.Vendor) -> HtmlT IO ()
    html vendors = do
        h1_ "Proveedores del pescado"
        table_ $ do
            tr_ $ do
                th_ "id"
                th_ "Nombre"
                th_ "RIF"
                th_ "Teléfono"
            tbody_ $ V.mapM_ row vendors
        where
        row :: FromDb IE.Vendor -> HtmlT IO ()
        row vendorDb =
            tr_ $ do
                td_ $ toHtml $ T.pack $ show key
                td_ $ toHtml name
                td_ $ toHtml taxid
                td_ $ toHtml phone
            where
            (vendor, key, _) = vendorDb
            IE.Vendor{..} = vendor

vendorNew :: Maybe UE.Session -> Connection -> HtmlT IO ()
vendorNew mSess conn = do
    contAdm mSess $ html
    where
    html :: HtmlT IO ()
    html = do
        div_ [ id_ "errPlace" ] ""
        h1_ "Nuevo proveedor"
        form_ $ do
            div_ [ class_ "formline" ] $ do
                label_ "Nombre"
                input_ [type_ "text", id_ "name"]
            div_ [ class_ "formline" ] $ do
                label_ "RIF"
                input_ [type_ "text", id_ "taxid"]
            div_ [ class_ "formline" ] $ do
                label_ "Teléfono"
                input_ [type_ "text", id_ "phone", oninput_ dblCheckJs]
            div_ [ class_ "formline" ] $ do
                button_ [type_ "button", id_ "sendData"] "Guardar"
        script_ [ src_ "/static/newvendor.js" ] ("" :: T.Text)

--------------------------------------------------------------------------
itemNew :: Maybe UE.Session -> Connection -> HtmlT IO ()
itemNew mSess conn = do
    contAdm mSess $ html
    where
    html :: HtmlT IO ()
    html = do
        species <- liftIO $ execStmt conn ID.getSpecies False
        cuttings <- liftIO $ execStmt conn ID.getCuttings False
        uoms <- liftIO $ execStmt conn ID.getUoms False
        let spcOpt = V.toList $ V.map (\((IE.Specie name _ _ _), key, _) -> (name, T.pack $ show key)) species
            cutOpt = V.toList $ V.map (\(IE.Cutting name _, key, _) -> (name, T.pack $ show key)) cuttings
            uomOpt = V.toList $ V.map (\(IE.Uom name _, key, _) -> (name, T.pack $ show key)) uoms
            sizeOpt = [("Dividible", "NoSize"), ("Tamaño fijo", "Fixed"), ("Tamaño variable", "Variable")]
        div_ [ id_ "errPlace" ] ""
        h1_ "Crear nuevo articulo"
        form_ $ do
            div_ [ class_ "formline" ] $ do
                label_ "Especie"
                selectHtml [id_ "specie"] spcOpt ""
            div_ [ class_ "formline" ] $ do
                label_ "Preparación"
                selectHtml [id_ "cutting"] cutOpt ""
            div_ [ class_ "formline" ] $ do
                label_ "Descripción"
                input_ [type_ "text", id_ "name"]
            div_ [ class_ "formline" ] $ do
                label_ "Se mide en"
                selectHtml [id_ "uom"] uomOpt "1"
            div_ [ class_ "formline" ] $ do
                label_ "Tamaño"
                selectHtml [id_ "size"] sizeOpt ""
                input_ [type_ "text", id_ "sizeval", style_ "display: none", oninput_ dblCheckJs]
            div_ [ class_ "formline" ] $ do
                label_ "Markup"
                input_ [type_ "text", id_ "markup", oninput_ dblCheckJs]
            div_ [ class_ "formline" ] $ do
                button_ [type_ "button", id_ "sendData"] "Guardar"
        script_ [ src_ "/static/newitem.js" ] ("" :: T.Text)


---------------------------------------------------------------------------------
purchases :: Maybe UE.Session -> Connection -> HtmlT IO ()
purchases mSess conn = do
    contInv mSess $ html
    where
    html :: HtmlT IO ()
    html = do
        prchs <- liftIO $ execStmt conn ID.getPrchHdrs ()
        h1_ "Listado de compras"
        table_ $ do
            tr_ $ do
                th_ "Código"
                th_ "Fecha"
                th_ "Proveedor"
                th_ "Número del documento"
                th_ "Valor Total"
                th_ "Cantidad de líneas"
                th_ "Nota"
            tbody_ $ V.mapM_ row prchs
        where
        row :: FromDb IE.PrchHdr -> HtmlT IO ()
        row (hdr, hdrK, amount) = do
            let IE.PrchHdr{..} = hdr
            tr_ $ do
                td_ $ a_ [ href_ $ "/protected/html/purchase/" <> (tshow hdrK)] $ toHtml $ tshow hdrK 
                td_ $ toHtml $ tshow docday
                td_ $ toHtml $ tshow vendor
                td_ $ toHtml docnum
                td_ $ toHtml $ tshow amount
                td_ "tbd"
                td_ $ toHtml note


purchaseDoc :: Maybe UE.Session -> Connection -> HtmlT IO ()
purchaseDoc mSess conn = do
    contInv mSess $ html
    where
    html :: HtmlT IO ()
    html = do
        vendors <- liftIO $ execStmt conn ID.getVendors True
        places <- liftIO $ execStmt conn ID.getPlaces True
        items <- liftIO $ execStmt conn ID.getItemsForSel ()
        let vndOpt = V.toList $ V.map (\(IE.Vendor name _ _ _, key, _) -> (name, T.pack $ show key)) vendors
            placeOpt = V.toList $ V.map (\(IE.Place name _, key, _) -> (name, T.pack $ show key)) places
            itemOpt = V.toList $ V.map (\item -> (IE.name ((fst item) :: IE.Item), T.pack $ show $ snd item)) items
        div_ [ id_ "errPlace" ] ""
        h1_ "Compra de pescado"
        form_ $ do
            div_ [ class_ "formline" ] $ do
                label_ "Proveedor"
                selectHtml [id_ "vendor"] vndOpt ""
            div_ [ class_ "formline" ] $ do
                label_ "Fecha del documento"
                input_ [type_ "date", id_ "docday"]
            div_ [ class_ "formline" ] $ do
                label_ "Número del documento"
                input_ [type_ "text", id_ "docnum"]
            div_ [ class_ "formline" ] $ do
                label_ "Comentario"
                input_ [type_ "textarea", id_ "note"]
            div_ [ class_ "formline" ] $ do
                button_ [type_ "button", id_ "sendData"] "Guardar"
        table_ [ class_ "docrows" ] $ do
            thead_ $ do
                tr_ $ do
                    th_ "Lugar"
                    th_ "Articulo"
                    th_ "Cantidad"
                    th_ "Valor" 
                    th_ "Acción" 
            tbody_ [ id_ "rowsCont" ] $ do
                tr_ $ do
                    td_ $ selectHtml [id_ "place"] placeOpt ""
                    td_ $ selectHtml [id_ "item" ] itemOpt ""
                    td_ $ input_ [type_ "text", id_ "qty", oninput_ dblCheckJs ]
                    td_ $ input_ [type_ "text", id_ "amount", oninput_ dblCheckJs ]
                    td_ $ button_ [type_ "button", id_ "addRowBtn"] "Agregar"

        script_ [ src_ "/static/lodash.js" ] ("" :: T.Text)
        script_ [ type_ "text/template", id_ "rowtmpl" ]
            ([r|
                <td><%= place %></td>
                <td><%= item %></td>
                <td><%= v.qty / 100 %></td>
                <td><%= v.amount %></td>
                <td onclick="delRow(<%= k %>)"><button>X</button></td>
            |] :: T.Text)
        script_ [ src_ "/static/purchase.js" ] ("" :: T.Text)

stockSmmr :: Maybe UE.Session -> Connection -> HtmlT IO ()
stockSmmr mSess conn = do
    contInv mSess $ html
    where
    html :: HtmlT IO ()
    html = do
        stock <- liftIO $ execStmt conn ID.merchSmmrStmt () 
        h1_ "Resumen del inventario"
        table_ $ do
            tr_ $ do
                th_ "id"
                th_ "Articulo"
                th_ "Fecha de llegada"
                th_ "Tamaño"
                th_ "UOM"
                th_ "Cantidad total"
                th_ "Valor"
                th_ "Cantidad reservada"
            tbody_ $ V.mapM_ row stock
        where
        row :: (IE.Merch2 'IE.Lot3, T.Text, T.Text, IE.Size, Day, Amount, Qty) -> HtmlT IO ()
        row (merchLot, iname, uname, minsz, arvDay, amount, rsqty) =
            tr_ $ do
                td_ $ a_ [ href_ ("/protected/html/stock/item/" <> (tshow itemK))] $ toHtml $ tshow itemK
                td_ $ toHtml iname
                td_ $ toHtml $ tshow arvDay
                td_ $ toHtml $ T.pack $ show minsz
                td_ $ toHtml uname
                td_ $ toHtml $ tshow qty
                td_ $ toHtml $ tshow amount
                td_ $ toHtml $ tshow rsqty
            where
            IE.MerchLot2 merchLotK qty = merchLot
            IE.MerchLotK itemK tokenK = merchLotK

stockItem :: Maybe UE.Session -> Connection -> Key2 IE.Item -> HtmlT IO ()
stockItem mSess conn itemK = do
    contInv mSess $ html
    where
    html :: HtmlT IO ()
    html = do
        stock <- liftIO $ execStmt conn ID.stockItemStmt itemK
        ((item, _, _), uname) <- liftIO $ execStmt conn ID.getItemById itemK
        h1_ "Resumen del artculo"
        h2_ $ toHtml $ tshow item
        table_ $ do
            tr_ $ do
                th_ "Lugar"
                th_ "Unidades"
                th_ "Cantidad"
                th_ "Valor"
                th_ "Fecha de token"
                th_ "Token"
                th_ "Comentario de token"
            tbody_ $ V.mapM_ row stock
        where
        row :: (IE.Stock, IE.Token, T.Text) -> HtmlT IO ()
        row (stock, tk, pname) =
            tr_ $ do
                td_ $ toHtml pname
                td_ $ toHtml uniqty
                td_ $ toHtml $ tshow qty
                td_ $ toHtml $ tshow amount
                td_ $ toHtml $ tshow day
                td_ $ a_ [ href_ (tshow code) ] $ toHtml code
                td_ $ toHtml note
            where
            (uniqty :: T.Text, lot) = case stock of
                IE.FromLot lot -> ("por medir", lot)
                IE.FromInd _ lot -> ("1", lot)
            IE.Lot stokK qty amount = lot
            IE.StockKey{..} = stokK
            IE.Token{..} = tk

-- Old version. Not used. To delete
markItem :: Maybe UE.Session -> Connection -> HtmlT IO ()
markItem mSess conn = do
    contInv mSess $ html
    where
    html :: HtmlT IO ()
    html = do
        h1_ "Medir un pescado"
        places <- liftIO $ execStmt conn ID.getPlaces True
        items <- liftIO $ execStmt conn ID.getItemsForSel ()
        blces <- liftIO $ execStmt conn ID.getBalancesStmt True 
        lbls <- liftIO $ execStmt conn ID.avlbLabelsStmt ()
        let placeOpt = V.toList $ V.map (\(IE.Place name _, key, _) -> (name, T.pack $ show key)) places
            itemOpt = V.toList $ V.map (\item -> (IE.name ((fst item) :: IE.Item), T.pack $ show $ snd item)) items
            blcesIndexed = V.imap f blces 
                where
                f i b = (b, tshow i)
            blcesOpt = V.toList $ V.map optionBlce blcesIndexed
                where
                optionBlce (((IE.Balance placeK itemK tokenK qty _), iname, uname, pname, tcode), i) = (iname <> "-" <> (tshow qty) <> uname <> " - " <> pname <> " - " <> tcode, i)
            labelsOpt = V.toList $ V.map (\l -> (l, l)) lbls
        div_ [ id_ "errPlace" ] ""
        form_ $ do
            div_ [ class_ "formline" ] $ do
                label_ "Articulo"
                selectHtml [id_ "balance"] blcesOpt ""
            div_ [ class_ "formline" ] $ do
                label_ "Presinto"
                selectHtml [id_ "label"] labelsOpt ""
            div_ [ class_ "formline" ] $ do
                label_ "Articulo nuevo"
                selectHtml [id_ "item"] itemOpt ""
            div_ [ class_ "formline" ] $ do
                label_ "Peso"
                input_ [type_ "text", id_ "qty", oninput_ dblCheckJs]
            div_ [ class_ "formline" ] $ do
                label_ "Donde se guardó"
                selectHtml [id_ "place"] placeOpt ""
            div_ [ class_ "formline" ] $ do
                label_ "Comentario"
                input_ [type_ "text", id_ "note"]
            div_ [ class_ "formline" ] $ do
                button_ [type_ "button", id_ "sendData"] "Guardar"

        script_ [ src_ "/static/lodash.js" ] ("" :: T.Text)
        script_ [ id_ "baldata" ] $ toHtml $ preEscapedByteString $ encodingToLazyByteString $ toEncoding blcesIndexed
        script_ [ src_ "/static/markitem.js" ] ("" :: T.Text)


stock :: Maybe UE.Session -> Connection -> Maybe (Key2 IE.Token3) -> Maybe (Key2 IE.Item) -> Maybe(Key2 IE.Place) -> Maybe (Key2 IE.Token) -> HtmlT IO ()
stock mSess conn mindK mitemK mplaceK mtokenK =
    contInv mSess $ fromJust $ mHtmlInd <|> mHtmlLot

    where
    mHtmlInd = fmap htmlInd mindK
    mHtmlLot = do
        itemK <- mitemK
        placeK <- mplaceK
        tokenK <- mtokenK
        return $ htmlLot itemK tokenK placeK

    htmlInd :: Key2 IE.Token3 -> HtmlT IO ()
    htmlInd indK = do
        h1_ "TBD"

    htmlLot :: Key2 IE.Item -> Key2 IE.Token -> Key2 IE.Place -> HtmlT IO ()
    htmlLot itemK tokenK placeK = do
        h1_ "Inventario"
        div_ [class_ "docheader"] $ do
            div_ [ class_ "docrow" ] $ do
                div_ [class_ "label"] "Articulo"
                itemDb <- liftIO $ execStmt conn ID.getItemById itemK
                let ((item, _, _), _) = itemDb
                div_ [class_ "value"] $ toHtml $ IE.name (item :: IE.Item)
            div_ [ class_ "docrow" ] $ do
                div_ [class_ "label"] "Origen"
                token <- liftIO $ execStmt conn ID.getTokenByK tokenK
                let IE.Token{..}=token
                div_ [class_ "value"] $ toHtml $ "Fecha: " <> (tshow day)
                            <> "; Label: " <> code <> "; Descripcion: " <> note
            div_ [ class_ "docrow" ] $ do
                div_ [class_ "label"] "Bodega"
                place <- liftIO $ execStmt conn ID.getPlaceByK placeK
                div_ [class_ "value"] $ toHtml $ IE.name (place :: IE.Place)
            div_ [ class_ "docrow" ] $ do
                let getQuery = "?item=" <> (tshow itemK)
                        <> "&place=" <> (tshow placeK)
                        <> "&lot=" <> (tshow tokenK)
                button_ [ type_ "button", onclick_ $ "location.href=\'/protected/html/stock/tolabel" 
                        <> getQuery <> "\'"
                        ] "Medir pescado"
                button_ [ type_ "button", onclick_ $ "location.href=\'/protected/html/stock/writeoff" 
                        <> getQuery <> "\'"
                        ] "Dar Baja"

stockLabels :: Maybe UE.Session -> Connection -> HtmlT IO ()
stockLabels mSess conn = 
    contInv mSess html
    where
    html :: HtmlT IO ()
    html = do
        h1_ "Inventario individualisado (con presintos)"
        rows <- liftIO $ execStmt conn ID.getIndsStmt ()
        table_ $ do
            tr_ $ do
                th_ "Presinto"
                th_ "Item"
                th_ "Lote"
                th_ "Lugar"
                th_ "Cantidad"
                th_ "Costo"
                th_ "Seleccionar"
            tbody_ $ V.mapM_ showRow rows
        where
        showRow :: (IE.Token3, Key2 IE.Token3) -> HtmlT IO ()
        showRow (ind, indK) =
            tr_ $ do
                td_ $ toHtml label
                td_ $ "tbd"
                td_ $ "tbd"
                td_ $ "tbd"
                td_ $ toHtml $ tshow qty
                td_ $ toHtml $ tshow cost
                td_ $ a_ [ href_ $ "/protected/html/stock/labeled/" <> (tshow indK)] ">>"
            
            where
            IE.Token3 label stock = ind
            IE.StockLot stockK qty cost = stock

stockLabel :: Maybe UE.Session -> Connection -> Key2 IE.Token3 -> HtmlT IO ()
stockLabel mSess conn indK = 
    contInv mSess html
    where
    html :: HtmlT IO ()
    html = do
        h1_ "Inventario con presinto"
        div_ [ id_ "errPlace" ] ""
        labeledDb <- liftIO $ execStmt conn ID.getLabeledByK indK
        let (labeled, _, (lastDoc, rsrK)) = labeledDb
            IE.Token3 label stock = labeled
            IE.StockLot stockK qty cost = stock
            IE.StockLotK merchK placeK = stockK 
            IE.MerchLotK itemK lotK = merchK
        itemDb <- liftIO $ execStmt conn ID.getItemById itemK
        let (itemE, itemDescr) = itemDb
            (item, _, _) = itemE
        place <- liftIO $ execStmt conn ID.getPlaceByK placeK
        lot <- liftIO $ execStmt conn ID.getTokenByK lotK
        div_ [class_ "docheader"] $ do
            div_ [ class_ "docrow" ] $ do
                div_ [class_ "label"] "Presinto"
                div_ [class_ "value"] $ toHtml label
            div_ [ class_ "docrow" ] $ do
                div_ [class_ "label"] "Articulo"
                div_ [class_ "value"] $ toHtml $ IE.name (item :: IE.Item)
            div_ [ class_ "docrow" ] $ do
                div_ [class_ "label"] "Fecha de llegada"
                div_ [class_ "value"] $ toHtml $ tshow $ IE.day (lot :: IE.Token)
            div_ [ class_ "docrow" ] $ do
                div_ [class_ "label"] "Lote"
                div_ [class_ "value"] $ toHtml $ (IE.code (lot :: IE.Token)) <> "; " <> (IE.note (lot :: IE.Token))
            div_ [ class_ "docrow" ] $ do
                div_ [class_ "label"] "Lugar"
                div_ [class_ "value"] $ toHtml $ IE.name (place :: IE.Place)
            div_ [ class_ "docrow" ] $ do
                div_ [class_ "label"] "Cantidad"
                div_ [class_ "value"] $ toHtml $ tshow qty
            div_ [ class_ "docrow" ] $ do
                div_ [class_ "label"] "Costo"
                div_ [class_ "value"] $ toHtml $ tshow cost
            div_ [ class_ "docrow" ] $ do
                div_ [class_ "label"] "Reservado"
                div_ [class_ "value"] $ toHtml $ tshow rsrK
            div_ [ class_ "docrow" ] $ do
                div_ [class_ "value"] $ button_ [type_ "button", id_ "writeoff"] "Dar baja"
        input_ [ hidden_ "hidden", id_ "labelK", value_ (tshow indK)]
        script_ [ src_ "/static/stocklabel.js" ] ("" :: T.Text)

writeoff :: Maybe UE.Session -> Connection -> Maybe (Key2 IE.Token3) -> Maybe (Key2 IE.Item) -> Maybe(Key2 IE.Place) -> Maybe (Key2 IE.Token) -> HtmlT IO ()
writeoff mSess conn mindK mitemK mplaceK mtokenK =
    contInv mSess $ fromJust $ mHtmlInd <|> mHtmlLot
    where
    mHtmlInd = fmap htmlInd mindK
    mHtmlLot = do
        itemK <- mitemK
        placeK <- mplaceK
        tokenK <- mtokenK
        return $ htmlLot $ IE.StockLotK (IE.MerchLotK itemK tokenK) placeK

    htmlInd :: Key2 IE.Token3 -> HtmlT IO ()
    htmlInd indK = do
        h1_ "TBD... I believe that this shall be deleted. Ind items will be written off by 1 button from ind item page"

    htmlLot :: IE.StockK 'IE.Lot3 -> HtmlT IO ()
    htmlLot stockK = do
        stock <- liftIO $ execStmt conn ID.getStockLot stockK
        h1_ "Dar baja al inventario"
        div_ [ id_ "errPlace" ] ""
        form_ $ do
            div_ [ class_ "formline" ] $ do
                label_ "Cantidad"
                input_ [type_ "text", id_ "qty", oninput_ dblCheckJs]
            div_ [ class_ "formline" ] $ do
                label_ "Comentario"
                input_ [type_ "text", id_ "note"]
            div_ [ class_ "formline" ] $ do
                button_ [type_ "button", id_ "sendData"] "Guardar"
        script_ [ type_ "text/data", id_ "stock" ] $ toHtml $ preEscapedByteString $ encodingToLazyByteString $ toEncoding stock
        script_ [ src_ "/static/writeoff.js" ] ("" :: T.Text)

stockLots :: Maybe UE.Session -> Connection -> HtmlT IO ()
stockLots mSess conn = do
    contInv mSess html

    where
    html :: HtmlT IO ()
    html = do
        h1_ "Inventario no individualisado"
        rows <- liftIO $ execStmt conn ID.getBalancesStmt True 
        table_ $ do
            tr_ $ do
                th_ "Item"
                th_ "Lote"
                th_ "Lugar"
                th_ "Cantidad"
                th_ "Seleccionar"
            tbody_ $ V.mapM_ showRow rows
        where
        showRow :: (IE.Balance, T.Text, T.Text, T.Text, T.Text) -> HtmlT IO ()
        showRow (stock, iname, uname, pname, tname) =
            tr_ $ do
                td_ $ toHtml iname
                td_ $ toHtml tname
                td_ $ toHtml pname
                td_ $ toHtml $ tshow qty <> uname
                td_ $ a_ [ href_ $ "/protected/html/stock" 
                        <> "?item=" <> (tshow item)
                        <> "&lot=" <> (tshow token)
                        <> "&place=" <> (tshow wh)
                        ] ">>"
            
            where
            IE.Balance{..} = stock


labelStock :: Maybe UE.Session -> Connection -> Maybe (Key2 IE.Item) -> Maybe (Key2 IE.Token) -> Maybe (Key IE.Place) -> HtmlT IO ()
labelStock mSess conn mitemK mtokenK mplaceK  = do
    let mHtml = do
            itemK <- mitemK
            tokenK <- mtokenK
            placeK <- mplaceK
            return $ html itemK tokenK placeK
    contInv mSess $ fromJust mHtml
    where
    html :: Key2 IE.Item -> Key2 IE.Token ->Key IE.Place -> HtmlT IO ()
    html itemK tokenK placeK = do
        h1_ "Medir un pescado"
        itemDb <- liftIO $ execStmt conn ID.getItemById itemK
        qtyamt <- liftIO $ execStmt conn ID.getQtyAmtStmt (placeK, unwrapKey itemK, unwrapKey tokenK)
        let (qty, amt) = qtyamt
            ((item, _, tag), descr) = itemDb
            IE.Item{..} = item
        h2_ $ toHtml $ name <> "Cantidad: " <> (tshow qty) <> "Valor: " <> (tshow amt)
        places <- liftIO $ execStmt conn ID.getPlaces True
        items <- liftIO $ execStmt conn ID.getItemsForSel ()
        lbls <- liftIO $ execStmt conn ID.avlbLabelsStmt ()
        let placeOpt = V.toList $ V.map (\(IE.Place name _, key, _) -> (name, T.pack $ show key)) places
            itemOpt = V.toList $ V.map (\item -> (IE.name ((fst item) :: IE.Item), T.pack $ show $ snd item)) $ V.filter (\(item, _) -> (IE.specie item) == specie) items
            labelsOpt = V.toList $ V.map (\l -> (l, l)) lbls
        div_ [ id_ "errPlace" ] ""
        form_ $ do
            div_ [ class_ "formline" ] $ do
                label_ "Presinto"
                selectHtml [id_ "label"] labelsOpt ""
            div_ [ class_ "formline" ] $ do
                label_ "Articulo nuevo"
                selectHtml [id_ "item"] itemOpt ""
            div_ [ class_ "formline" ] $ do
                label_ "Peso"
                input_ [type_ "text", id_ "qty", oninput_ dblCheckJs]
            div_ [ class_ "formline" ] $ do
                label_ "Donde se guardó"
                selectHtml [id_ "place"] placeOpt ""
            div_ [ class_ "formline" ] $ do
                label_ "Comentario"
                input_ [type_ "text", id_ "note"]
            div_ [ class_ "formline" ] $ do
                button_ [type_ "button", id_ "sendData"] "Guardar"

        let stockK = IE.StockLotK (IE.MerchLotK itemK tokenK) (Key2 (fromIntegral placeK))
        script_ [ type_ "text/data", id_ "stockK" ] $ toHtml $ preEscapedByteString $ encodingToLazyByteString $ toEncoding stockK
        script_ [ type_ "text/data", id_ "from" ] $ toHtml $ preEscapedByteString $ encodingToLazyByteString $ toEncoding (placeK, itemK, tokenK) 
        script_ [ src_ "/static/markitem2.js" ] ("" :: T.Text)

