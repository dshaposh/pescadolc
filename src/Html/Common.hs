{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes #-}

module Html.Common where

--import Data.Vector as V
import Text.RawString.QQ (r)
import Data.Maybe (isJust, isNothing, fromJust)
import qualified Data.Text as T
import Lucid.Base (Html, HtmlT, Attribute, toHtml, with)
import Lucid.Html5
import Entities.Common (FromDb, tshow)
import qualified Entities.User as UE
import qualified Entities.Client as CE
import Control.Monad.IO.Class (liftIO)
import Control.Monad (when)
--import Servant (err404, errBody, throwError)


intCheckJs :: T.Text
intCheckJs = [r|
    this.value = this.value.replace(/[^0-9.]/g, '');
    |]
dblCheckJs :: T.Text
dblCheckJs = [r|
    this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
    |]
phoneCheckJs :: T.Text
phoneCheckJs = [r|
    this.value = this.value.replace(/[^0-9]/g, '').replace(/(^0*4?)/g, '').replace(/(^)/g, '04$1');
    |]


wrapper2 :: Maybe UE.Session -> HtmlT IO () -> HtmlT IO ()
wrapper2 mSess content =
    html_ $ do
        head_ $ do
          title_ "PescadoLaCruz"
          link_ [rel_ "stylesheet", type_ "text/css", href_ "/static/styles.css"]
        body_ $ do
            header_ $ do
                div_ [ class_ "item" ] "PescadoLaCruz"
                div_ [ class_ "menu" ] $ do
                    if isNothing mSess then div_ [class_ "item menu"] $ a_ [href_ "/protected/html/login"] "LogIn"
                    else do
                        div_ [class_ "item menu"] $ a_ [href_ "/"] "Tienda"
                        div_ [class_ "item menu"] $ a_ [href_ "/protected/html/stock/summary"] "Inventario"
                        div_ [class_ "item menu"] $ a_ [href_ "/protected/html/orders/2dispatch" ] "Ordenes"
                        div_ [class_ "item menu"] $ a_ [href_ "/protected/html/cuttings"] "Admin"
                        div_ [class_ "item menu"] $ form_ $ do
                                button_ [id_ "logout", type_ "button"]  "LogOut"
                                script_ logoutJs
            div_ [ class_ "main" ] content
            footer_ $ div_ "Pescado La Cruz 2020"
    where
    logoutJs :: T.Text
    logoutJs = [r|
        var loBtn = document.getElementById("logout");
        loBtn.onclick = logout;
        function logout(){
            loBtn.disabled = true;
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (xhttp.readyState == XMLHttpRequest.DONE) {
                    if (xhttp.status === 200) {
                        window.location.replace("/protected");
                    } else {
                        loBtn.disabled = false;
                        alert("Error: " + xhttp.status
                            + ' - ' + xhttp.statusText
                            + ': ' + xhttp.responseText);
                    }
                }
            }
            xhttp.open("POST", "/protected/logout", true);
            xhttp.setRequestHeader("Content-Type", "application/json");
            xhttp.send();
        }
        |]

--notFound :: T.Text -> HtmlT IO ()
--notFound path = do
--    throwError $ err404 { errBody = "Resourse \"" <> path <> "\" has not found" }


contInv :: Maybe UE.Session -> HtmlT IO () -> HtmlT IO ()
contInv mSess content =
    wrapper2 mSess $ do
        div_ [ class_ "submenu" ] $ do
            div_ [class_ "item menu"] $ a_ [href_ "/protected/html/purchases"] "Listado de compras"
            div_ [class_ "item menu"] $ a_ [href_ "/protected/html/purchase/new"] "Compra de pescado"
            div_ [class_ "item menu"] $ a_ [href_ "/protected/html/stock/summary"] "Resumen de inventario"
            div_ [class_ "item menu"] $ a_ [href_ "/protected/html/stock/lots"] "Inventario en Lotes"
            div_ [class_ "item menu"] $ a_ [href_ "/protected/html/stock/labeled"] "Inventario con Presintos"
        div_ [ class_ "content" ] content

contOrders :: Maybe UE.Session -> HtmlT IO () -> HtmlT IO ()
contOrders mSess content =
    wrapper2 mSess $ do
        div_ [ class_ "submenu" ] $ do
            div_ [class_ "item menu"] $ a_ [href_ "/protected/html/orders" ] "Listado de pedidos"
            div_ [class_ "item menu"] $ a_ [href_ "/protected/html/orders/2pay" ] "Ingresar pagos"
            div_ [class_ "item menu"] $ a_ [href_ "/protected/html/orders/rows/2prepare" ] "Preparar los pedidos"
            div_ [class_ "item menu"] $ a_ [href_ "/protected/html/orders/2dispatch" ] "Ingresar despachos"
            div_ [class_ "item menu"] $ a_ [href_ "/protected/html/orders/2deliver" ] "Ingresar entregas"
            div_ [class_ "item menu"] $ a_ [href_ "/protected/html/orders/payments" ] "Los pagos recibidos"
            div_ [class_ "item menu"] $ a_ [href_ "/protected/html/orders/dispatches" ] "Despachos ejecutados"
        div_ [ class_ "content" ] content

contAdm :: Maybe UE.Session -> HtmlT IO () -> HtmlT IO ()
contAdm mSess content =
    wrapper2 mSess $ do
        div_ [ class_ "submenu" ] $ do
            div_ [class_ "item menu"] $ a_ [href_ "/protected/html/users"] "Usuarios"
            div_ [class_ "item menu"] $ a_ [href_ "/protected/html/cuttings"] "Cortes"
            div_ [class_ "item menu"] $ a_ [href_ "/protected/html/species"] "Pescados"
            div_ [class_ "item menu"] $ a_ [href_ "/protected/html/species/new"] "Ingresar Pescado"
            div_ [class_ "item menu"] $ a_ [href_ "/protected/html/vendors"] "Proveedores"
            div_ [class_ "item menu"] $ a_ [href_ "/protected/html/vendors/new"] "Crear proveedor"
            div_ [class_ "item menu"] $ a_ [href_ "/protected/html/courriers"] "Mensajeros"
            div_ [class_ "item menu"] $ a_ [href_ "/protected/html/courriers/new"] "Crear mensajero"
            div_ [class_ "item menu"] $ a_ [href_ "/protected/html/labels"] "Presintos"
            div_ [class_ "item menu"] $ a_ [href_ "/protected/html/labels/new"] "Crear presinto"
            div_ [class_ "item menu"] $ a_ [href_ "/protected/html/items"] "Articulos"
            div_ [class_ "item menu"] $ a_ [href_ "/protected/html/items/new"] "Crear un articulo"
            div_ [class_ "item menu"] $ a_ [href_ "/protected/html/delivery"] "Entrega"
            div_ [class_ "item menu"] $ a_ [href_ "/protected/html/smsservice"] "Servicio SMS"
            div_ [class_ "item menu"] $ a_ [href_ "/protected/html/visitorlog"] "La lista de visitantes"
            div_ [class_ "item menu"] $ a_ [href_ "/protected/html/cms"] "CMS"
        div_ [ class_ "content" ] content

selectHtml :: [Attribute] -> [(T.Text, T.Text)] -> T.Text -> HtmlT IO ()
selectHtml attributes options selItem = select_ attributes
    $ mapM_ optionTag ((""::T.Text, ""::T.Text) : options) 
    where optionTag (descr, optId) = with
            option_ 
            ([ value_ optId ]
                <> (if selItem == optId then [ selected_ "selected" ] else []))
            $ toHtml descr 

