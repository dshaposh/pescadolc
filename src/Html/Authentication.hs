{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GADTs #-}

module Html.Authentication where

import Text.RawString.QQ (r)
import Data.Text as T
import Data.Vector as V
import Lucid.Base (Html, HtmlT, toHtml)
import Lucid.Html5

import Control.Monad.IO.Class (liftIO)
import Entities.Common (DbEntity, tshow)
import qualified Entities.User as UE
import qualified Db.Authentication as AD
import Html.Common (wrapper2, contAdm)
import Hasql.Connection (Connection)
import Db (execStmt)


login :: HtmlT IO ()
login = do
    wrapper2 Nothing html
    where
    html :: HtmlT IO ()
    html = do
        div_ [ class_ "content" ] $ do
            div_ [ id_ "errPlace" ] ""
            form_ $ do
                div_ [ class_ "formline" ] $ do
                    label_ "Login"
                    input_ [id_ "login", type_ "text" ]
                div_ [ class_ "formline" ] $ do
                    label_ "Contraseña"
                    input_ [id_ "pwd", type_ "password" ]
                div_ [ class_ "formline" ] $ do
                    button_ [type_ "button", id_ "sendData" ] "Enviar"
            script_ [ src_ "/static/login.js" ] ("" :: T.Text)

        
users :: Maybe UE.Session -> Connection -> HtmlT IO ()
users mSess conn = do
    list <- liftIO $ execStmt conn AD.getUser ()
    contAdm mSess $ html list
    where
    html :: V.Vector (DbEntity UE.User) -> HtmlT IO ()
    html users = do
        h1_ "Usuarios"
        table_ $ do
            tr_ $ do
                th_ "Nick"
                th_ "Nombres"
                th_ "Correo"
                th_ "Perfil"
                th_ "Bloqueado?"
            tbody_ $ V.mapM_ row users
        where
        row :: DbEntity UE.User -> HtmlT IO ()
        row ((UE.User role nick name email), key, (_, isBlocked)) =
            tr_ $ do
                td_ $ toHtml nick
                td_ $ toHtml name
                td_ $ toHtml email
                td_ $ toHtml $ tshow role
                td_ $ toHtml $ tshow isBlocked 
    

newUser :: Maybe UE.Session -> Connection -> HtmlT IO ()
newUser mSess conn = do
    wrapper2 mSess $ html
    where
    html :: HtmlT IO ()
    html = do
        h1_ "Crear nuevo usuario"
        div_ [ id_ "errPlace" ] ""
        form_ $ do
            div_ [ class_ "formline" ] $ do
                label_ "login"
                input_ [type_ "text", id_ "login"]
            div_ [ class_ "formline" ] $ do
                label_ "Nombres"
                input_ [type_ "text", id_ "name"]
            div_ [ class_ "formline" ] $ do
                label_ "Correo electrónico"
                input_ [type_ "number", id_ "email"]
            div_ [ class_ "formline" ] $ do
                button_ [type_ "button", id_ "sendData"] "Guardar"
        script_ [ src_ "/static/newuser.js" ] ("" :: T.Text)

loginStep1 :: Html ()
loginStep1 = do
    div_ [ id_ "errplace" ] ""
    form_ $ do
        div_ [ class_ "formline" ] $ do
            label_ "Móvil"
            input_ [id_ "phone"]
        div_ [ class_ "formline" ] $ do
            button_ [type_ "button", id_ "senddata" ] "Enviar"

loginStep2 :: Html ()
loginStep2 = do
    div_ [ id_ "errplace" ] ""
    form_ $ do
        div_ [ class_ "formline" ] $ do
            label_ "Código de confirmación"
            input_ [id_ "code"]
        div_ [ class_ "formline" ] $ do
            button_ [type_ "button", id_ "senddata" ] "Enviar"


logoutForm :: Html ()
logoutForm = form_ [method_ "POST"] $ button_ [] "Logout"
