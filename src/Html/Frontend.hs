{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE LambdaCase #-}

module Html.Frontend where

import GHC.Int (Int64)
import Text.RawString.QQ (r)
import qualified Data.Text as T
import Data.Text.Lazy.Encoding (decodeUtf8)
import Data.Text.Lazy (toStrict)
import Data.Maybe (isJust, isNothing, fromMaybe, fromJust)
import Data.Time (Day, LocalTime, utcToLocalTime, minutesToTimeZone, TimeZone)
import Data.Time.Clock (UTCTime, NominalDiffTime)
import qualified Data.Vector as V
import Control.Monad (when)
import Lucid.Base (Html, HtmlT, toHtml, makeAttribute, toHtmlRaw)
import Lucid.Html5
import Data.Aeson (toEncoding, encode)
import Data.Time.Format(defaultTimeLocale, formatTime)

import Control.Applicative((<|>))
import Control.Monad.IO.Class (liftIO)

import Data.Functor.Identity(Identity)
import qualified Control.Monad.Trans.State.Lazy as St
import Network.Socket (SockAddr)

import Entities.Common (FromDb, tshow, Key, Key2, DbEntity, Qty, Amount(Amount), unwrapQty, multQty)

import qualified Entities.Order as OE
import qualified Entities.Client as CE
import qualified Db.Order as OD
import Html.Common (contOrders, contAdm, selectHtml, phoneCheckJs, dblCheckJs)
import Hasql.Connection (Connection)
import Db (execStmt)
import qualified Entities.Inventory as IE
import qualified Db.Inventory as ID
import qualified Cms
import qualified Sms

data StoreSpecie = StoreSpecie IE.Specie (Key2 IE.Specie) Amount [StoreItem]

-- 1st Amount is Price, 2nd - Amount
data StoreItem = StoreItem IE.Item (Key2 IE.Item) IE.Uom Amount Tokens Amount

data Tokens
    = Lots [(IE.Token, Key2 IE.Token, Qty, Qty)]
    | Inds [(IE.Token, Key2 IE.Token, Key2 IE.Token3, Qty, Bool)]


type RowParser a = St.StateT [StoreSpecie] Identity a

parseRow :: ID.StoreData -> RowParser ()
parseRow row = do
    state <- St.get
    if null state then
        St.put [newSpecie]
    else do
        let StoreSpecie currSpecie currSpecieK totalSpecie currItems = head state
        if currSpecie /= specie then 
            St.put $ newSpecie:state
        else do
            let StoreItem currItem currItemK uom price tokens totalItem = head currItems
            if currItemK /= itemK then do
                let storeSpecie' = StoreSpecie currSpecie currSpecieK dTotal $ newItem:currItems
                St.put $ storeSpecie':(tail state)
            else do
                let tokens' = addToken newToken tokens
                    item' = StoreItem currItem currItemK uom price tokens' (totalItem + dTotal)
                    items' = item':(tail currItems)
                    specie' = StoreSpecie currSpecie currSpecieK (totalSpecie + dTotal) items'
                St.put $ specie':(tail state)

    where
    (specie, ((item, itemK, uom, price), token, tokenK, mIndK, qtyAvlb, qtyCart)) = row
    specieK = IE.specie item
    newToken =
        if isNothing mIndK then
            Lots [(token, tokenK, qtyAvlb, qtyCart)]
        else 
            Inds [(token, tokenK, fromJust mIndK, qtyAvlb, if qtyCart /= 0 then True else False)]
    newItem = StoreItem item itemK uom price newToken dTotal
    newSpecie = StoreSpecie specie specieK dTotal [newItem]

    addToken :: Tokens -> Tokens -> Tokens
    addToken (Lots existing) (Lots new1) = Lots $ new1 ++ existing
    addToken (Inds existing) (Inds new1) = Inds $ new1 ++ existing
    addToken existing _ = existing -- Really shal throw an error here. But how? It is a pure function!
    dTotal = Amount $ (fromIntegral price) * (fromIntegral qtyCart) `div` 100


--------------------------------------------------------------------------------
store :: Connection -> CE.Session -> HtmlT IO ()
store conn sess = do
        fromDb <- liftIO $ execStmt conn ID.freeMerchStmt3 $ CE.sid (sess :: CE.Session)
        let storeData = St.execState (V.mapM_ parseRow fromDb) []
            total = sum $ map (\(StoreSpecie _ _ tsp _) -> tsp) storeData
            content = do 
                div_ [ class_ "masonry" ] $ do
                    mapM_ (specieSection total) storeData
                script_ [ type_ "text/data", id_ "user" ] $ encode $ CE.visitor sess
                script_ [ src_ "/static/store.js" ] ("" :: T.Text) 
                --script_ [ type_ "text/data", id_ "test1" ] $ encode fromDb
        frWrapper (Just sess) content (Just total)


    where
    printDay :: IE.Token -> T.Text
    printDay token = T.pack $ formatTime defaultTimeLocale "%d %h" $ IE.day token

    specieSection  :: Amount -> StoreSpecie -> HtmlT IO ()
    specieSection total (StoreSpecie specie specieK amount storeItems) = do
        div_ [ class_ "cell" ] $ do
            div_ [ class_ "total2", style_ $ if amount > 0 then "" else "display: none" ] $ a_ [href_ "/html/pcart"] $ toHtml $ tshow amount 
            div_ [ class_ "title" ] $ h1_ $ toHtml $ IE.name (specie :: IE.Specie)
            div_ [ class_ "photo" ] $ img_ [ src_ $ "/species/" <> (tshow specieK) <> "/image"
                    , onerror_ "this.src=\"/static/images/01.jpg\"", class_ "image" ]
            div_ $ mapM_ itemSection $ storeItems
            div_ [ class_ "end" ] ""

    itemSection  :: StoreItem -> HtmlT IO ()
    itemSection (StoreItem item itemK uom price tokens amount) = do
        case tokens of
            Lots lots -> mapM_ showRow lots
            Inds inds -> do
                div_ [ class_ "errPlace" ] ""
                div_ [ class_ "product" ] $ do
                    div_ [ class_ "title-mini" ] $ do
                        div_ [ class_ "product-descr" ] $ toHtml
                                $ IE.name (item :: IE.Item)
                        div_ [class_ "product-price"] $ toHtml 
                                $ (tshow price) <> "/" <> (IE.name (uom :: IE.Uom))
                    div_ [ class_ "boxer clearfix"] $ mapM_ showBox inds

        where
        showRow (token, tokenK, qtyAvlb, qtyCart) = do
            div_ [ class_ "errPlace" ] ""
            case IE.size item of
                IE.NoSize ->
                    div_ [ class_ "product" ] $ do 
                        showDescription
                        div_ [ class_ "paket-02" ] $
                            input_ [ class_ $ "input" <> (if qtyCart > 0 then " active" else "")
                                , type_ "text" , value_ (tshow qtyCart), oninput_ dblCheckJs
                                , makeAttribute "data" (toStrict $ decodeUtf8 $ encode
                                    (IE.MerchLotK itemK tokenK, qtyCart, qtyAvlb, price)) ]
                IE.Fixed pack ->
                    div_ [ class_ "product" ] $ do 
                        showDescription
                        div_ [ class_ "paket-02" ] $ do
                            div_ [class_ $ "qtyselector" <> (if qtyCart > 0 then " active" else "")
                                    , makeAttribute "data" (toStrict $ decodeUtf8 $ encode
                                        (IE.MerchLotK itemK tokenK, qtyCart, qtyAvlb, price, IE.size item)) ] $ do
                                div_ [class_ "minus"] "−"
                                div_ [class_ "pkgqty"] $ toHtml $ tshow $ (unwrapQty qtyCart) `div` (unwrapQty pack)
                                div_ [class_ "plus"] "+"
                IE.Variable -> mempty

            where
            showDescription :: HtmlT IO ()
            showDescription = do
                div_ [ class_ "paket-01" ] $ do
                    div_ [ class_ "product-descr" ] $ toHtml
                            $ IE.name (item :: IE.Item)
                    div_ [ class_ "product-lot" ] $ toHtml
                            $ (tshow qtyAvlb)
                            <> (IE.name (uom :: IE.Uom))
                            <> " de " <> (printDay token) 
                    div_ [class_ "product-price"] $ toHtml 
                            $ (tshow price) <> "/" <> (IE.name (uom :: IE.Uom))

        showBox :: (IE.Token, Key2 IE.Token, Key2 IE.Token3, Qty, Bool) -> HtmlT IO () 
        showBox (token, tokenK, indK, qty, isInCart) =
            div_ [ class_ $ if isInCart then "box active" else "box",
                    makeAttribute "data" (toStrict $ decodeUtf8 $ encode (indK, qty, isInCart, price))] $ do
                toHtml $ (tshow qty) <> (IE.name (uom :: IE.Uom))
                p_ $ toHtml $ printDay token
            


--------------------------------------------------------------------------------
pcart :: Connection -> CE.Session -> SockAddr -> HtmlT IO ()
pcart conn sess socket = frWrapper (Just sess) content Nothing
    where
    content = do
        div_ [ class_ "content" ] $ do
            fromDb <- liftIO $ execStmt conn ID.pcartStmt sid
            if (length fromDb) == 0 then
                h1_ $ "Su carrito de compras está vacia. Favor visitar nuestra " <> (a_ [href_ "/"] "Tienda")
            else do
                div_ [ id_ "errPlace" ] ""
                h1_ "Carrito de Compras"
                dlvDay <- liftIO $ execStmt conn OD.getDlvDay ()
                div_ [ id_ "pcart"] $ (V.mapM row fromDb)
                        >>= subTotalRow . V.sum
                        >>= (\products -> (deliveryRow >>= (\delivery -> totalRow $ products + delivery)))
                h1_ "Ingresar Datos para la Entrega"
                div_ [ class_ "docheader" ] $ do
                    div_ [class_ "docrow"] $ do
                        div_ [class_ "label"] "Fecha de la Entrega"
                        div_ [class_ "value", id_ "dlvDay"] $ toHtml $ tshow dlvDay
                    div_ [ id_ "orderEl"] ""

                script_ [ type_ "text/template", id_ "tmplOrder" ]
                    ([r|
                        <div class="docrow">
                            <div class="label">Número de Teléfono</div>
                            <div class="inpbtnvalue">
                                <input class="phone" type="text" disabled="disabled" value="0<%= phone %>" >
                                <button type="button" id="newPhoneBtn">Cambiar</button>
                            </div>
                        </div>
                        <div class="docrow">
                            <div class="label">Nombre</div>
                            <div class="value">
                                <input id="clname" type="text">
                            </div>
                        </div>
                        <div class="docrow">
                            <div class="label">Dirección</div>
                            <div class="value">
                                <input id="claddress" type="text">
                            </div>
                        </div>
                        <div class="docrow">
                            <div class="label">He Leído y Aceptado la <a href="/html/DataPolicy">Politica de Privacidad</a></div>
                            <input id="policycheck" type="checkbox">
                        </div>
                        <div class="docrow">
                            <button type="button" id="saveOrderBtn">Finalizar mi Pedido</button>
                        </div>
                        <div class="hint">
                            Al oprimir el boton su pedido será guardado y sus productos reservados.
                            En el caso de que no recibimos <a href="/html/PaymentData">el pago</a> en 20 minutos,
                            la orden será cancelado, la reserva eliminada y los productos volverán a la venta.
                        </div>
                     |] :: T.Text)

                smsDelay <- liftIO $ Sms.getSmsDelay conn socket
                loginWdg smsDelay sess
                script_ [ src_ "/static/lodash.js" ] ("" :: T.Text) 
                script_ [ src_ "/static/pcart.js" ] ("" :: T.Text) 

    CE.Session{..} = sess

    calcRowTotal :: IE.OrderRow2 -> Amount -> Amount
    calcRowTotal row price = Amount $ (fromIntegral qty) * (fromIntegral price) `div` 100
        where
        merch = IE.merch row
        IE.MerchLot2 _ qty = merch
    --calcRowTotal row _ = IE.amount (row :: IE.OrderRow2)

    row :: (IE.OrderRow2, IE.Item, T.Text, Amount) -> HtmlT IO Amount 
    row (orow, item, uname, price) = do
        let rowTotal = calcRowTotal orow price
        div_ [class_ "pitem", makeAttribute "merchkey" jsonK ] $ do
            div_ [ class_ "prow1" ] $ do
                div_ [ class_ "pname" ] $ toHtml xName
                div_ [ class_ "priceqty" ] $ do
                    div_ [ class_ "pprice" ] $ toHtml $ (tshow price) <> "/" <> uname
                    div_ [ class_ "pqty" ] $ toHtml (tshow qty <> uname)
                div_ [ class_ "pamount" ] $ toHtml $ tshow rowTotal 
                div_ [ class_ "pdelete" ] $ button_ [type_ "button"] "X"
            div_ [ class_ "pinstr" ] $ input_ [ class_ "instruction", type_ "text", placeholder_ "Ingrese el Tipo de Corte que Desea"]
        return rowTotal
        where
        IE.OrderRow2{..} = orow
        IE.MerchLot2 lotK qty = merch
        IE.Item{..} = item
        jsonK = toStrict $ decodeUtf8 $ fromJust $ (fmap (encode . IE.AnyMerch . IE.MerchInd2) indK) <|> (Just $ encode $ merch)
        xName = case size of
            IE.NoSize -> name
            IE.Fixed s -> name <> " " <> (tshow pkQty) <> "ea x" <> (tshow $ (fromIntegral s) / 100) <> uname
                where
                pkQty = (unwrapQty qty) `div` (unwrapQty s)
            IE.Variable -> name <> " 1ud x " <> (tshow qty) <> uname

    subTotalRow :: Amount -> HtmlT IO Amount
    subTotalRow total = 
        div_ [class_ "ptotal" ] $
            div_ [ class_ "prow1" ] $ do
                div_ [ class_ "pname" ] "Total de productos"
                div_ [ class_ "priceqty" ] ""
                div_ [ class_ "pamount", id_ "subtotal" ] $ toHtml $ tshow total
                div_ [ class_ "pdelete" ] ""
                return total

    deliveryRow :: HtmlT IO Amount
    deliveryRow = 
        div_ [class_ "ptotal" ] $
            div_ [ class_ "prow1" ] $ do
                dlvPrice <- liftIO $ execStmt conn OD.getDlvPrice ()
                div_ [ class_ "pname" ] "Costo de entrega"
                div_ [ class_ "priceqty" ] ""
                div_ [ class_ "pamount", id_ "dlvPrice" ] $ toHtml $ tshow dlvPrice
                div_ [ class_ "pdelete" ] ""
                return dlvPrice

    totalRow :: Amount -> HtmlT IO ()
    totalRow total =
        div_ [class_ "ptotal" ] $
            div_ [ class_ "prow1" ] $ do
                div_ [ class_ "pname" ] "Total del pedido"
                div_ [ class_ "priceqty" ] ""
                div_ [ class_ "pamount", id_ "total" ] $ toHtml $ tshow total
                div_ [ class_ "pdelete" ] ""

--------------------------------------------------------------------------            
myOrders :: Connection -> CE.Session -> HtmlT IO ()
myOrders conn sess = do
    frWrapper (Just sess) html Nothing
    where
    html :: HtmlT IO ()
    html = div_ [ class_ "content" ] $ do
        let CE.Session{..} = sess
        when (visitor == CE.Anonimous) $ fail "The client is not authenticated"
        let CE.Client{..} = visitor
        when (authst /= CE.Authenticated) $ fail "The phone number is not confirmed"
        let phone = CE.phone (visitor :: CE.Visitor)
            CE.Phone phoneTxt = phone
        orders <- liftIO $ execStmt conn OD.ordersByPhone phoneTxt
        h1_ "Mis Pedidos"
        h2_ $ toHtml $ "Número de Teléfono " <> (tshow phone)
        div_ [ class_ "hint" ] $
                "En el caso de que su pago no se refleja en el estatús de su compra en la "
                 <> "tabla debajo, por favor llámanos por el número 0412-115-8295"
        div_ [ class_ "order rowheader" ] $ do
            div_ [ class_ "code" ] "Código"
            div_ [ class_ "date" ] "Fecha"
            div_ [ class_ "value_status" ] $ do
                div_ "Valor" 
                div_ "Estatus"
            div_ [ class_ "name_address" ] $ do
                div_ "Nombre" 
                div_ "Dirección"
        V.mapM_ row orders
    
        where
        row :: FromDb OE.OrderHdr -> HtmlT IO ()
        row (order, key, (status, _)) = div_ [ class_ "order" ] $ do
            div_ [class_ "code" ] $ a_ [ href_ $ "/html/orders/" <> (tshow key)] $ toHtml $ tshow key
            div_ [class_ "date"]  $ toHtml $ tshow dlvDay
            div_ [class_ "value_status"] $ do
                div_ $ toHtml $ tshow total
                div_ $ toHtml $ tshow status
            div_ [class_ "name_address"] $ do
                div_ $ toHtml $ name
                div_ $ toHtml $ address
            where
            OE.OrderHdr{..} = order

--------------------------------------------------------------------------            
myOrder :: Connection -> CE.Session -> Key2 OE.OrderHdr -> HtmlT IO ()
myOrder conn sess ordK = do
    frWrapper (Just sess) html Nothing
    where
    html :: HtmlT IO ()
    html = div_ [class_ "content"] $ do
        let CE.Session{..} = sess
        when (visitor == CE.Anonimous) $ fail "The client is not authenticated"
        let CE.Client{..} = visitor
        when (authst /= CE.Authenticated) $ fail "The phone number is not confirmed"
        let sessPhone = CE.phone (visitor :: CE.Visitor)
            tz = minutesToTimeZone $ fromIntegral $ CE.tzshift (visitor :: CE.Visitor)
        (order, _, (status, docK)) <- liftIO $ execStmt conn OD.orderHdrByK ordK
        let OE.OrderHdr{..} = order
        when (phone /= sessPhone) $ fail "Are you a hacker? Why do you try to get someone' else order?"
        h1_ "Resumen de la Compra"
        pcRows <- liftIO $ execStmt conn ID.ordRowsStmt ordK
        div_ [ id_ "pcart"] $ do
            V.mapM_ pCartRow pcRows
            pCartTotal "Total productos" $ total - dlvCost 
            pCartTotal "Costo de entrega" dlvCost 
            pCartTotal "Total de la order" total


        div_ [class_ "docheader"] $ do
            div_ [class_ "docrow"] $ do
                div_ [class_ "label"] "Teléfono"
                div_ [class_ "value"] $ toHtml $ tshow phone
            div_ [class_ "docrow"] $ do
                div_ [class_ "label"] "Nombre"
                div_ [class_ "value"] $ toHtml name
            div_ [class_ "docrow"] $ do
                div_ [class_ "label"] "Dirección"
                div_ [class_ "value"] $ toHtml address
            div_ [class_ "docrow"] $ do
                div_ [class_ "label"] "Fecha de entrega"
                div_ [class_ "value"] $ toHtml $ tshow dlvDay
            div_ [class_ "docrow"] $ do
                div_ [class_ "label"] "Estatus"
                div_ [class_ "value"] $ toHtml $ tshow status


        h2_ "Histórico de la Orden de Compra"
        list <- liftIO $ execStmt conn OD.ordHistory ordK
        table_ $ do
            tr_ $ do
                th_ "Código"
                th_ "Fecha y Hora"
                th_ "Estatus"
            tbody_ $ V.mapM_ (histRow tz) list

        where
        pCartRow :: (IE.OrderRow2, IE.Item, T.Text) -> HtmlT IO ()
        pCartRow (orow, item, uname) =
            div_ [class_ "pitem"] $ do
                div_ [ class_ "prow1" ] $ do
                    div_ [ class_ "pname" ] $ toHtml xName
                    div_ [ class_ "pprice" ] $ toHtml $ (tshow price) <> "/" <> uname
                    div_ [ class_ "pqty" ] $ toHtml (tshow qty <> uname)
                    div_ [ class_ "pamount" ] $ toHtml $ tshow amount
                div_ [ class_ "pinstr" ] $ toHtml instr
            where
            price = Amount $ (fromIntegral amount) * 100 `div` (fromIntegral qty)
            IE.OrderRow2{..} = orow
            IE.MerchLot2 lotK qty = merch
            IE.Item{..} = item
            xName = case size of
                IE.NoSize -> name
                IE.Fixed s -> name <> " " <> (tshow pkQty) <> "ea x" <> (tshow $ (fromIntegral s) / 100) <> uname
                    where
                    pkQty = (unwrapQty qty) `div` (unwrapQty s)
                IE.Variable -> name <> " 1ud x " <> (tshow qty) <> uname

        pCartTotal :: T.Text -> Amount -> HtmlT IO ()
        pCartTotal descr amount =
            div_ [class_ "pitem"] $ do
                div_ [ class_ "prow1" ] $ do
                    div_ [ class_ "pname" ] $ toHtml descr 
                    div_ [ class_ "pprice" ] ""
                    div_ [ class_ "pqty" ] ""
                    div_ [ class_ "pamount" ] $ toHtml $ tshow amount

        histRow :: TimeZone -> (OE.OKey, UTCTime, OE.Status) -> HtmlT IO ()
        histRow tz (key, tstamp, status) =
            tr_ $ do
                td_ $ toHtml $ tshow key
                td_ $ toHtml $ T.pack $ formatTime defaultTimeLocale "%d/%m %T" $ utcToLocalTime tz tstamp
                td_ $ toHtml $ tshow status


csm :: Connection -> Maybe CE.Session -> Cms.Resource -> HtmlT IO ()
csm conn mSess resource = frWrapper mSess content Nothing
    where
    content =  
        div_ [ class_ "cmscontent"] $ do
            content <- liftIO $ execStmt conn Cms.getResource resource
            toHtmlRaw content

species :: Connection -> Maybe CE.Session -> HtmlT IO ()
species conn mSess = frWrapper mSess content Nothing
    where
    content = do 
        div_ [ class_ "content"] $ do
            species <- liftIO $ execStmt conn ID.getSpecies True
            descrs <- liftIO $ execStmt conn ID.getSpeciesDescr $ V.map (\(_, k, _) -> k) species
            V.mapM_ showSpecie $ V.zip species descrs
    showSpecie :: (FromDb IE.Specie, T.Text) -> HtmlT IO ()
    showSpecie ((IE.Specie name _ _ _, specieK, _), descr) = do
        div_ [ class_ "species" ] $ do
            h1_ $ toHtml name
            img_ [ src_ $ "/species/" <> (tshow specieK) <> "/image", alt_ "...photo..."]
            br_ []
            toHtmlRaw descr


frWrapper :: Maybe CE.Session -> HtmlT IO () -> Maybe Amount -> HtmlT IO ()
frWrapper mSess content mTotal = do
    doctype_ 
    html_ [lang_ "es" ] $ do
        let authStatus = fmap CE.visitor mSess >>= (\case 
                CE.Client phone (CE.Authenticated) _ -> Just (phone, "Authenticated" :: T.Text)
                CE.Client phone (CE.CodeSent _ _) _ -> Just (phone, "CodeSent")
                _ -> Nothing)
            isAuthenticated (Just (_, "Authenticated")) = True
            isAuthenticated _ = False
            loginSwitch sw = if sw == (isAuthenticated authStatus) then [style_ "display:none"] else []
        head_ $ do
            title_ "PescadoLaCruz"
            link_ [ href_ "/static/images/favicon.png", type_ "image/png", rel_ "shortcut icon" ] 
            link_ [ href_ "/static/frontend.css", type_ "text/css", rel_ "stylesheet" ] 
            meta_ [ name_ "viewport", content_ "width=device-width" ]

            link_ [ href_ "/static/manifest.json", rel_ "manifest" ]  
            meta_ [ name_ "theme-color", content_ "white" ]
            link_ [ href_ "/images/icon-152.png", rel_ "apple-touch-icon" ]   
            meta_ [ name_ "theme-color", content_ "white" ] 
            meta_ [ name_ "apple-mobile-web-app-capable", content_ "yes" ]
            meta_ [ name_ "apple-mobile-web-app-status-bar-style", content_ "black" ]
            meta_ [ name_ "apple-mobile-web-app-title", content_ "Pescado La Cruz" ]
            meta_ [ name_ "msapplication-TileImage", content_ "/images/icon-144.png" ]
            meta_ [ name_ "msapplication-TileColor", content_ "#FFFFFF" ]
        body_ $ do

            div_ [ id_ "menu2" ] $ do
                div_ [ id_ "closeBtn" ] "×"
                h1_ "Pescado La Cruz"
                h2_ "Directamente del Mar a tu Hogar"
                div_ [class_ "menucontainer"] $ do
                    div_ [id_ "logoutErrEl"] ""
                    ul_ [ class_ "menu" ] $ do
                        li_ $ a_ [href_ "/"] "Nuestra Tienda"
                        li_ $ a_ [href_ "/html/pcart"] "Carrito de Compras"
                        li_ ([id_ "myOrdersEl"] <> (loginSwitch False)) $ a_ [href_ "/html/orders"] "Mis Pedidos"
                    ul_ [ class_ "menu" ] $ do
                        li_ $ a_ [href_ "/html/StepByStep"] "Funcionamiento"
                        li_ $ a_ [href_ "/html/PaymentData"] "Datos para los Pagos"
                        li_ $ a_ [href_ "/html/ReturnPolicy"] "Politica de Retornos"
                    ul_ [ class_ "menu" ] $ do
                        li_ $ a_ [href_ "/html/AboutUs"] "Sobre Nosotros"
                        li_ $ a_ [href_ "/html/ourfish"] "Nuestro Pescado"
                        li_ ([id_ "loginEl"] <> (loginSwitch True)) $
                            a_ [href_ "/html/login"] "Login"
                        li_ ([id_ "logoutEl"] <> (loginSwitch False)) "Logout"

                div_ [ id_ "installPromotion", style_ "display:none"] $ do
                    "Facilitar el acceso a la página agregando el enlace a "
                    "la pantalla de inicio "
                    button_ [type_ "button", id_ "installBtn" ] "Instalar"
                div_ [class_ "connectwithus"] $ do
                    div_ [class_ "text"] "Siguenos a traves de:"
                    div_ [class_ "icons"] $ do
                        a_ [target_ "_blank", rel_ "nofollow", href_ "https://facebook.com/pescadolacruz"]
                            $ img_ [src_ "/static/images/icn_facebook.png"]
                        a_ [target_ "_blank", rel_ "nofollow", href_ "https://instagram.com/pescadolacruz"]
                            $ img_ [src_ "/static/images/icn_instagram.png"]
                div_ [ class_ "legal" ] $ do
                    p_ $ "Al usar este servicio Usted está Aceptado los " 
                        <> (a_ [ href_ "/html/TermsOfUse" ] "Términos de Uso")
                        <> " y la "
                        <> (a_ [ href_ "/html/PrivacyPolicy" ] "Politica de Privacidad.")
                    p_ "Para funcionamiento de la pagina se necesita Javascript y Cookies." 
                    p_ $ "Todos los Derechos Reservados. "
                            <> b_ "© 2020 Pescado La Cruz, S.A." 

            div_ [ class_ "background" ] $ do
                div_ [ id_ "homeBtn" ] $ case mTotal of
                    Just total -> do
                        a_ [ href_ "/html/pcart" ] $ img_ [ src_ "/static/images/shop.png", class_ "pading-shop" ]
                        div_ [ class_ "pading-shop"] $ do
                            p_ [id_ "total"] $ toHtml $ tshow total 
                    Nothing -> a_ [ href_ "/" ] $ img_ [ src_ "/static/images/home.png", class_ "pading-shop" ]
                img_ [ id_ "openBtn", src_ "/static/images/menu.png" ]
                div_ [ class_ "head" ] $ do
                    div_ [ class_ "head-01" ] "" 
                    div_ [ class_ "head-02" ] $ img_ [ src_ "/static/images/logotype.png" ]
                    div_ [ class_ "head-03" ] "" 

                script_ [ type_ "text/data", id_ "user" ] $ encode authStatus
                script_ menuJs 

                content

                div_ [ class_ "footer" ] $ do
                    img_ [ src_ "/static/images/logo-footer.jpg", class_ "pading-logo-footer" ]
                    br_ [] 
                    div_ [class_ "connectwithus"] $ do
                        div_ [class_ "text"] "Siguenos a traves de:"
                        div_ [class_ "icons"] $ do
                            a_ [target_ "_blank", rel_ "nofollow", href_ "https://facebook.com/pescadolacruz"]
                                $ img_ [src_ "/static/images/icn_facebook.png"]
                            a_ [target_ "_blank", rel_ "nofollow", href_ "https://instagram.com/pescadolacruz"]
                                $ img_ [src_ "/static/images/icn_instagram.png"]
            script_ mainJs

menuJs :: T.Text
menuJs = [r|
    "use strict"
    var menuEl = document.getElementById("menu2"),
        openBtn = document.getElementById("openBtn"),
        closeBtn = document.getElementById("closeBtn"),
        openAction = function(){
            menuEl.style.display = "block";
        },
        closeAction = function(){
            menuEl.style.display = "none";
        };
    
    openBtn.onclick = openAction;
    closeBtn.onclick = closeAction;

    var loginEl = document.getElementById("loginEl"),
        logoutEl = document.getElementById("logoutEl"),
        myOrdersEl = document.getElementById("myOrdersEl"),
        logoutErrEl = document.getElementById("logoutErrEl");
    logoutEl.onclick = function(){
        logoutEl.style.display = "none";
        var promise = logout();
        promise.then(() => { window.location.replace("/");}
                 ,(err) => { logoutErrEl.innerHTML = err
                           ; logoutEl.style.display = "block"
                           }
                );
    }
    var onLogin = function()
        { logoutEl.style.display = "block"
        ; myOrdersEl.style.display = "block"
        ; loginEl.style.display = "none"
        }

    var logout = function(){
        return new Promise(function(resolve, reject){
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (xhttp.readyState == XMLHttpRequest.DONE) {
                    if (xhttp.status === 200) {
                        
                        logoutEl.style.display = "none";
                        myOrdersEl.style.display = "none";
                        loginEl.style.display = "block";

                        var smsDelay = Math.round(parseFloat(xhttp.responseText));
                        resolve(smsDelay);
                    } else {
                        reject("Error: " + xhttp.status + ' - ' + xhttp.statusText + ': ' + xhttp.responseText);
                    }
                }
            }
            xhttp.open("POST", "/logout", true);
            xhttp.setRequestHeader("Content-Type", "application/json");
            xhttp.send();
        })
    };


    
    |]

mainJs :: T.Text
mainJs = [r|
    window.onload = () => {
        'use strict';

        if ('serviceWorker' in navigator) {
            navigator.serviceWorker
                .register('/static/sw.js', {scope: '/'}).then((reg) => {
                    console.log("Successfully registered service worker. Scope is " + reg.scope);
                    }).catch((err) => {
                        console.log("Registering failed " + err);
                    });
        } else {
            console.log("Service worker cannot be registered on this device");
        }
    }

    var installPromptEl = document.getElementById("installPromotion");
    var deferredPrompt;

    installToHomeScreen = () => {
        installPromptEl.style.display="none";
        deferredPrompt.prompt();
        deferredPrompt.userChoice.then((choiceResult) => {
            if (choiceResult === "accepted") {
                console.log("User accepted the install prompt");
            } else {
                console.log("User dismissed the install prompt");
            }
        }); 
    }


    var showInstallPromotion = () => {
        installPromptEl.style.display="block";
        var ipBtn = document.getElementById("installBtn");
        ipBtn.onclick = installToHomeScreen;
    }

    window.addEventListener('beforeinstallprompt', (e) => {
        console.log("Ready to offer application installation");
        e.preventDefault();
        deferredPrompt = e;
        showInstallPromotion();
    });
    |]

login :: Connection -> CE.Session -> SockAddr -> HtmlT IO ()
login conn sess socket = do
    smsDelay <- liftIO $ Sms.getSmsDelay conn socket
    frWrapper (Just sess) (content smsDelay) Nothing
    where
    content smsDelay = div_ [ class_ "content"] $ do
        loginWdg smsDelay sess
        script_
            ([r|
            var client = JSON.parse(document.getElementById("user").innerHTML);
            loginModule(client, function(phone){
                window.location.replace("/html/orders");
            });
            |]) 



loginWdg :: NominalDiffTime -> CE.Session -> HtmlT IO ()
loginWdg smsDelay sess = content smsDelay            
    where
    CE.Session{..} = sess
    (d1, d2) = if visitor == CE.Anonimous then
            ([], [style_ ("display:none" :: T.Text)]) 
        else 
            let CE.Client{..} = visitor
            in if authst /= CE.Authenticated then ([style_ "display:none"], []) 
                else ([style_ ("display:none" :: T.Text)], [style_ "display:none"]) 

    content smsDelay = do
        div_ [ id_ "errPlace" ] ""
        ---------------------------------------------
        div_ ([ id_ "submitPhone", class_ "docheader" ] <> d1) $ do
            div_ [ class_ "docrow" ] $ do
                div_ [ class_ "label" ] "Número de Teléfono" 
                div_ [ class_ "inpbtnvalue " ] $ do
                    input_ [ class_ "phone", type_ "text", id_ "phoneInp" ]
                    button_ [ type_ "button", id_ "phoneBtn" ] "Confirmar"
            div_ [ class_ "hint" ] "Al oprimir el boton a su teléfono será enviado un SMS con el código de confirmación."
            div_ [ class_ "hint", id_ "submitWaitingHint" ] $ do 
                "El botón se activará en " 
                div_ [ id_ "submitDelayCounter", style_ "display:inline-block" ] ""

        ---------------------------------------------
        div_ ([ id_ "confirmPhone", class_ "docheader" ] <> d2) $ do
            div_ [ class_ "docrow" ] $ do
                div_ [ class_ "label" ] "Número de Teléfono" 
                div_ [ class_ "inpbtnvalue " ] $ do
                    input_ [ class_ "phone", type_ "text", value_ $ fromMaybe "04" $ fmap tshow$ CE.getPhone sess, disabled_ "disabled" ]
                    button_ [ type_ "button", id_ "newPhoneBtn" ] "Cambiar"
            div_ [ class_ "docrow" ] $ do
                div_ [ class_ "label" ] "Código" 
                div_ [ class_ "inpbtnvalue " ] $ do
                    input_ [ class_ "smscode", type_ "text", id_ "codeInp", placeholder_ "Código de SMS" ]
                    button_ [ type_ "button", id_ "confirmBtn" ] "Confirmar"
            div_ [ class_ "hint" ] "El mensaje SMS con el código ha sido enviado a su teléfono"
            div_ [ class_ "hint" ] $ do
                "Se puede exigir el reenvio del código "
                button_ [ type_ "button", id_ "resendBtn" ] "Reenviar SMS"
                div_ [ id_ "confirmWaitingHint" ] $ do 
                    " en " 
                    div_ [ id_ "confirmDelayCounter", style_ "display:inline-block" ] ""

        script_ [ type_ "text/data", id_ "smsDelay" ] $ tshow smsDelay
        script_ [ src_ "/static/clientlogin.js" ] ("" :: T.Text)

