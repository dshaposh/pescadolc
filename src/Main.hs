{-# LANGUAGE OverloadedStrings      #-}
{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE TypeOperators          #-}
{-# LANGUAGE TypeFamilies           #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DuplicateRecordFields #-}


module Main where

import qualified Data.Aeson as AESON
import Data.Maybe (fromMaybe, fromJust, isNothing, isJust)
import qualified Data.Vector as V
import Control.Monad (when)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Trans.Reader (ReaderT, ask, runReaderT)

import qualified System.IO as SIO

import Lucid.Base (renderBS, renderBST)

import ApiType (Html, Image, ProtectedAPI, FullAPI, ClientAPI)

import Data.Time.Calendar (Day, fromGregorian)
import Data.Time.Clock (UTCTime(..), secondsToDiffTime, getCurrentTime, addUTCTime, nominalDay, NominalDiffTime, diffUTCTime)
import qualified Data.Map as M

import Servant.Server (Server, ServerT, serve,  Application, Handler, Context((:.), EmptyContext), serveWithContext, hoistServerWithContext, hoistServer)

import qualified WaiAppStatic.Types as SS
import qualified WaiAppStatic.Storage.Filesystem as SS
import Servant.Server.StaticFiles (serveDirectoryWith)

import qualified Network.HTTP.Types as NHT
import Servant (Proxy(Proxy), (:<|>)(..), err303, err304, err404, err403, err401, errBody, throwError, Headers, Header, addHeader, MimeRender(..), errHeaders)
import Servant.API.Experimental.Auth (AuthProtect)
import Servant.Server.Experimental.Auth (AuthHandler, mkAuthHandler, AuthServerData)
import qualified Servant.Multipart as SMP

import Network.Socket (SockAddr(SockAddrInet))
import Network.Wai.Handler.Warp (setPort, defaultSettings, run)
import Network.Wai.Handler.WarpTLS (runTLS, tlsSettings, tlsSettingsChain)
import Network.Wai.Middleware.EnforceHTTPS (EnforceHTTPSConfig(EnforceHTTPSConfig))
import qualified Network.Wai.Middleware.EnforceHTTPS as EnforceHTTPS
import Network.Wai (Request, requestHeaders, vault, pathInfo, isSecure, responseLBS)
import qualified Web.Cookie as Coo
import HttpApiDataCookie ()
import qualified Data.Vault.Lazy as Vlt

import GHC.Int(Int64, Int16)
import GHC.Word(Word16)
import qualified Data.Text as T
import Text.Printf (printf)
import Data.Text.Encoding (decodeUtf8, encodeUtf8)
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSL
import qualified Data.ByteString.Base64 as B64
--
import System.Entropy (getEntropy)
import System.Environment (getArgs, getExecutablePath)
--
import Hasql.Connection (acquire, Connection, Settings, settings, release)
import Data.IORef(newIORef, readIORef, IORef, modifyIORef')
import Control.Concurrent (threadDelay, forkIO)
import Db(execStmt)
import Entities.Common (Key, Key2(Key2), Amount, Qty, tshow)
import qualified Sms
import qualified Cms
--import Authentication (ClientAuthStatus(..), User(..), Client(..), Staff(..), Phone(..), SessData(..), Credentials(..))
--import qualified Authentication as A
import qualified Html.Common as CH
import qualified Html.Authentication as AH
import qualified Html.Inventory as IH
import qualified Html.Order as OH
import qualified Html.Frontend as FH
import qualified Html.Admin as AdH
import qualified Entities.User as UE
import qualified Entities.Client as CE
import qualified Entities.Order as OE
import qualified Entities.Inventory as IE
import qualified Sms
import qualified Db.Authentication as AD
import qualified Db.Order as OD
import qualified Db.Inventory as ID
--
-- ********* test middleware ********
import Network.Wai (Application, Response, ResponseReceived, mapResponseHeaders)
--

getParams :: IO (Maybe Int, String)
getParams = fmap (\a -> parseArgs (Nothing, ".") a) getArgs
    where
    parseArgs :: (Maybe Int, String) -> [String] -> (Maybe Int, String)
    parseArgs (port, root) ("-p":p:rest) = parseArgs (Just $ read p, root) rest 
    parseArgs (port, root) ("-r":r:rest) = parseArgs (port, r) rest 
    parseArgs (port, root) [] = (port, root) 


myToStdHandler :: (Connection, Sms.Conf) -> MyHandler a -> Handler a
myToStdHandler conf mh = runReaderT mh conf
--
--
type instance AuthServerData (AuthProtect "session") = UE.User
type MyContext = AuthHandler Request UE.User ': '[]

api :: Proxy FullAPI
api = Proxy

proxyMyContext :: Proxy MyContext
proxyMyContext = Proxy

type UserSessions = IORef (M.Map BS.ByteString UE.Session)
userSessInSecs :: Integer
userSessInSecs = 60 * 60 * 16 
type ClientSessions = IORef (M.Map BS.ByteString CE.Session)
clSessInSecs :: Integer
clSessInSecs = 60 * 60 * 8 

dbSettings :: Settings
dbSettings = settings "localhost" 5432 "pescadolc" "pescadolc" "pescadolc"

cancelOldOrders:: Connection -> IO ()
cancelOldOrders conn = do
    curTime <- getCurrentTime
    unpaidOrds <- liftIO $ execStmt conn OD.unpaidOrders ()
    let ageLimit = (fromInteger 900) :: NominalDiffTime
    liftIO $ V.mapM_ (OD.saveCancel conn) $ V.map fst $ V.filter (\(_, t) -> (diffUTCTime curTime t) > ageLimit) unpaidOrds
    threadDelay $ 1000000*60*1
    cancelOldOrders conn

removeStaleSessions:: UserSessions -> ClientSessions -> IO ()
removeStaleSessions usersRef clientsRef = do
    curTime <- getCurrentTime
    modifyIORef' usersRef (\v -> M.filter (\s -> (curTime < UE.expires s)) v)
    modifyIORef' clientsRef (\v -> M.filter (\s -> (curTime < CE.expires s)) v)
    threadDelay $ 1000000*60*10
    removeStaleSessions usersRef clientsRef

type MyHandler = ReaderT (Connection, Sms.Conf) Handler 

instance MimeRender Html (BSL.ByteString) where
    mimeRender _ = id
instance MimeRender Image (BS.ByteString) where
    mimeRender _ = BSL.fromStrict 


serverFull :: String -> ClientSessions -> UserSessions -> Vlt.Key CE.Session -> Vlt.Key UE.Session -> ServerT FullAPI MyHandler
serverFull rootDir clientsS usersS vltCl vltUsr =
         serveDirectoryWith staticSettings
    :<|> serverProtected usersS vltUsr
    :<|> serverClients clientsS vltCl
    :<|> notFound
    where
    staticSettings :: SS.StaticSettings
    staticSettings = (SS.defaultWebAppSettings (rootDir ++ "/static")) {SS.ssMaxAge = SS.MaxAgeSeconds 2400}

    notFound :: [T.Text] -> MyHandler BSL.ByteString
    notFound path = 
        throwError $ err404 { errBody = "Resourse \"" <> (BSL.fromStrict $ encodeUtf8 $ T.intercalate "/" path) <> "\" has not found" }
    --notFound path = liftIO $ renderBST $ CH.notFound

mkCookie :: BS.ByteString -> BS.ByteString -> Integer -> Coo.SetCookie
mkCookie sid name t = Coo.defaultSetCookie
            { Coo.setCookieName = name
            , Coo.setCookieValue = sid
            , Coo.setCookieMaxAge = Just $ secondsToDiffTime t
            , Coo.setCookiePath = Just "/"
            } 

serverClients :: ClientSessions -> Vlt.Key CE.Session -> ServerT ClientAPI MyHandler 
serverClients sessions vltK =
         clientLogin
    :<|> pcartPost
    :<|> pcartDelete
    :<|> orderPost
    :<|> pcart
    :<|> orders
    :<|> order
    :<|> store
    :<|> loginPost
    :<|> loginAskCodePost
    :<|> loginCodePost
    :<|> logoutPost
    :<|> species
    :<|> specieImg
    :<|> cms

    where
    clientLogin :: Vlt.Vault -> SockAddr -> MyHandler BSL.ByteString
    clientLogin vault socket = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err303 {errHeaders = [("Location", "/")]} 
        conn <- askConn
        liftIO $ renderBST $ FH.login conn (fromJust mSess) socket


    pcartPost :: Vlt.Vault -> IE.AnyMerch -> MyHandler ()
    pcartPost vault orow = do
        conn <- askConn
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err403 {errBody = "Session info has not been found"} 
        liftIO $ ID.savePcart conn (fromJust mSess) orow

    pcartDelete :: Vlt.Vault -> IE.AnyMerchK -> MyHandler ()
    pcartDelete vault orow = do
        conn <- askConn
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err403 {errBody = "Session info has not been found"} 
        liftIO $ ID.removeCartRow conn (fromJust mSess) orow

    orderPost :: Vlt.Vault -> (OE.OrderHdr, V.Vector IE.OrderRow) -> MyHandler ()
    orderPost vault order = do
        conn <- askConn
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err403 {errBody = "Session info has not been found"} 
        liftIO $ OD.saveOrder conn (fromJust mSess) order

    pcart :: Vlt.Vault -> SockAddr -> MyHandler BSL.ByteString
    pcart vault socket = do
        let mSess = Vlt.lookup vltK vault
        if isNothing mSess then throwError $ err303 {errHeaders = [("Location", "/")]} 
        else do
            let sess = fromJust mSess
            conn <- askConn
            liftIO $ renderBST $ FH.pcart conn sess socket

    orders :: Vlt.Vault -> MyHandler BSL.ByteString
    orders vault = do
        let mSess = Vlt.lookup vltK vault
        if not (CE.isAuthenticated mSess) then throwError $ err303 {errHeaders = [("Location", "/")]} 
        else do
            let sess = fromJust mSess
            conn <- askConn
            liftIO $ renderBST $ FH.myOrders conn sess

    order :: Vlt.Vault -> Key2 OE.OrderHdr -> MyHandler BSL.ByteString
    order vault k = do
        let mSess = Vlt.lookup vltK vault
        if not (CE.isAuthenticated mSess) then throwError $ err303 {errHeaders = [("Location", "/")]} 
        else do
            let sess = fromJust mSess
            conn <- askConn
            liftIO $ renderBST $ FH.myOrder conn sess k



    store :: Vlt.Vault -> SockAddr -> Maybe T.Text -> MyHandler (Headers '[Header "set-cookie" Coo.SetCookie] BSL.ByteString)
    store vault socket mUserAgent= do
        let mSess = Vlt.lookup vltK vault
        if isNothing mSess then do
            entropy <- liftIO $ getEntropy 15 
            curTime <- liftIO getCurrentTime
            let sid = B64.encode entropy
                cooMaxAge = addUTCTime (realToFrac clSessInSecs) curTime
                newSess = CE.Session CE.Anonimous cooMaxAge sid
            liftIO $ modifyIORef' sessions (M.insert sid newSess)
            conn <- askConn
            response <- liftIO $ renderBST $ FH.store conn newSess
            let SockAddrInet _ ip = socket
                logEntry = CE.VisitorLog sid ip (T.take 150 $ fromMaybe "" mUserAgent)
            liftIO $ execStmt conn CE.saveLogEntry logEntry 
            return $ addHeader (mkCookie sid "clientsid" clSessInSecs) response
        else do
            let sess = fromJust mSess
            conn <- askConn
            response <- liftIO $ renderBST $ FH.store conn sess
            return $ addHeader (mkCookie "123" "trash" 0) response


    calcConfCode :: IO T.Text
    calcConfCode = do
        entropy <- getEntropy 2 
        let (w1:w2:_) = BS.unpack entropy
            w = (((fromIntegral w1) * 256) + (fromIntegral w2)) `mod` (10000 :: Word16)
        return $ if w == 0 then "0000" else T.pack $ printf "%04d" w

        
    loginPost :: Vlt.Vault -> SockAddr -> CE.Credentials ->  MyHandler NominalDiffTime 
    loginPost vault socket crd = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err403 {errBody = "Session info has been lost"} 
        let sess = fromJust mSess
            visitor = CE.visitor sess
        when (visitor /= CE.Anonimous) $ throwError $ err403 {errBody = "Already logged in"} 

        let CE.Credentials phone tzshift = crd
        conf <- ask

        toWait <- liftIO $ Sms.getSmsDelay (fst conf) socket
        when (toWait > 0) $ throwError $ err403 {errBody = "To wait " <> (BSL.fromStrict $ encodeUtf8 $ tshow toWait)} 

        smsCode <- liftIO calcConfCode
        when (phone /= CE.Phone "4777") $ do
            liftIO $ Sms.sendConfCode conf socket phone smsCode
            return ()

        let visitor' = CE.Client phone (CE.CodeSent smsCode 0) tzshift
            sess' = sess {CE.visitor = visitor'}
        liftIO $ modifyIORef' sessions (M.insert (CE.sid (sess :: CE.Session)) sess')
        liftIO $ Sms.getSmsDelay (fst conf) socket

    loginAskCodePost:: Vlt.Vault -> SockAddr -> MyHandler NominalDiffTime
    loginAskCodePost vault socket = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err403 {errBody = "Session info has been lost"} 
        let sess = fromJust mSess
            visitor = CE.visitor sess
        case visitor of
            CE.Client _ (CE.CodeSent _ _) _ -> do
                conf <- ask
                smsCode <- liftIO calcConfCode


                toWait <- liftIO $ Sms.getSmsDelay (fst conf) socket
                when (toWait > 0) $ throwError $ err403 {errBody = "To wait " <> (BSL.fromStrict $ encodeUtf8 $ tshow toWait)} 
                let CE.Client phone _ tzshift = visitor
                liftIO $ Sms.sendConfCode conf socket phone smsCode

                let visitor' = CE.Client phone (CE.CodeSent smsCode 0) tzshift
                    sess' = sess {CE.visitor = visitor'}
                liftIO $ modifyIORef' sessions (M.insert (CE.sid (sess :: CE.Session)) sess')
                liftIO $ Sms.getSmsDelay (fst conf) socket

            _ -> throwError $ err403 {errBody = "Shall be in the status \"CodeSent\""} 

    loginCodePost :: Vlt.Vault -> T.Text -> MyHandler ()
    loginCodePost vault confCode = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err403 {errBody = "Session is lost"} 
        let sess = fromJust mSess
        when (isNothing mSess) $ throwError $ err403 {errBody = "Already authenticated"} 

        let CE.Session visitor _ sid = sess
        case visitor of
            CE.Client phone (CE.CodeSent rightCode tryNum) tz -> do
                when (tryNum > 4) $ throwError $ err403 {errBody = "Demasiado intentos!"}
                if (rightCode == confCode) || (phone == (CE.Phone "4777") && confCode == "7777") then do
                    let client' = CE.Client phone CE.Authenticated tz
                        updSess oldSess = oldSess { CE.visitor = client'}
                    liftIO $ modifyIORef' sessions (\v -> M.adjust (updSess) sid v)
                else do
                    let client' = CE.Client phone (CE.CodeSent rightCode (1+tryNum)) tz
                        updSess oldSess = oldSess { CE.visitor = client'}
                    liftIO $ modifyIORef' sessions (\v -> M.adjust (updSess) sid v)
                    throwError $ err401 {errBody = "Wrong confirmation code"}

            _ -> throwError $ err403 {errBody = "You shall be in the status \"CodeSent\" here!"} 

    
    logoutPost :: Vlt.Vault -> SockAddr -> MyHandler NominalDiffTime
    logoutPost vault socket = case Vlt.lookup vltK vault of
        Nothing -> throwError $ err403 {errBody = "You're not logged in"} 
        Just sess -> do
            conn <- askConn
            let CE.Session{..} = sess
                sess' sess = sess { CE.visitor = CE.Anonimous}
            liftIO $ modifyIORef' sessions (\v -> M.adjust sess' sid v)
            liftIO $ Sms.getSmsDelay conn socket

    species :: Vlt.Vault -> MyHandler BSL.ByteString
    species vault = do
        conn <- askConn
        let mSess = Vlt.lookup vltK vault
        liftIO $ renderBST $ FH.species conn mSess

    specieImg :: Key2 IE.Specie -> Maybe UTCTime -> MyHandler (Headers '[Header "Cache-Control" T.Text, Header "Last-Modified" UTCTime] BS.ByteString)
    specieImg specieK mUserVersion = do
        conn <- askConn
        (image, ts) <- liftIO $ execStmt conn ID.getImage specieK
        if mUserVersion == Just ts then
            throwError $ err304 {errBody = "Not modified"} 
        else
            return $ addHeader "public, max-age=1200" $ addHeader ts image

    cms :: Vlt.Vault -> Cms.Resource -> MyHandler BSL.ByteString
    cms vault resource = do
        let mSess = Vlt.lookup vltK vault
        conn <- askConn
        liftIO $ renderBST $ FH.csm conn mSess resource




serverProtected :: UserSessions -> Vlt.Key UE.Session -> ServerT ProtectedAPI MyHandler 
serverProtected sessions vltK = 
         home
    :<|> login
    :<|> loginPost
    :<|> logoutPost
    :<|> users
    :<|> cuttings

    :<|> species
    :<|> specieNew
    :<|> specie
    :<|> speciePost
    :<|> specieDelete
    :<|> speciePatch
    :<|> specieImagePost

    :<|> vendors
    :<|> vendorNew
    :<|> vendorNewPost
    :<|> courriers
    :<|> courrierNew
    :<|> courrierNewPost
    :<|> labels
    :<|> labelNew
    :<|> labelPost
    :<|> labelDelete

    :<|> items
    :<|> item
    :<|> itemDelete
    :<|> itemPatch
    :<|> itemNew
    :<|> itemNewPost

    :<|> purchases
    :<|> prchNew
    :<|> prchNewPost
    :<|> stockSmmr
    :<|> stockItem
    :<|> markItem
    :<|> stockLabels
    :<|> stockLabel
    :<|> labelWriteOffPost
    :<|> stock
    :<|> stockWrOff
    :<|> stockWrOffPost
    :<|> stockLots
    :<|> markItem2
    :<|> markItemPost
    :<|> ords
    :<|> ords2Pay
    :<|> ords2Prepare
    :<|> ords2Dispatch
    :<|> ords2Deliver
    :<|> orderRow
    :<|> orderRowPreparePost
    :<|> ordPaidPost
    :<|> ordDeliveredPost
    :<|> pmts
--    :<|> pmtDelete
    :<|> dspPost
    :<|> dsps
    :<|> ordSummary
    :<|> ordStatusDelete
    :<|> delivery 
    :<|> deliveryPricePost
    :<|> deliveryDayPost
    :<|> smsService
    :<|> smsServicePost
    :<|> visitorLog

    :<|> cms 
    :<|> cmsUploadPost
    where
        
    home :: Vlt.Vault -> MyHandler BSL.ByteString
    home vault = do
        let mSess = Vlt.lookup vltK vault
        if isNothing mSess then
            login
        else
            stockSmmr vault

    login :: MyHandler BSL.ByteString
    login = do
        conn <- askConn
        liftIO $ renderBST $ AH.login

    loginPost :: Vlt.Vault -> UE.Credentials ->  MyHandler (Headers '[Header "set-cookie" Coo.SetCookie] ())
    loginPost vault crd = do
        let mSession = Vlt.lookup vltK vault
        when (isJust mSession) $ throwError $ err403 {errBody = "Already logged in"} 
        entropy <- liftIO $ getEntropy 15 
        curTime <- liftIO getCurrentTime
        let sid = B64.encode entropy
            cooMaxAge = addUTCTime (realToFrac userSessInSecs) curTime
            UE.Credentials login pwd tzshift = crd
        conn <- askConn
        mUserDb <- liftIO $ execStmt conn AD.checkPwd (login, pwd)
        let mNewSess = fmap (\((UE.User r n _ _), k) -> UE.Session k n r cooMaxAge tzshift sid) mUserDb
        case mNewSess of
            Just newSess -> do
                liftIO $ modifyIORef' sessions (M.insert sid newSess)
                return $ addHeader (mkCookie sid "usersid" userSessInSecs) () 
            Nothing -> throwError $ err401 {errBody = "Incorrect user name or password"}

    logoutPost :: Vlt.Vault -> MyHandler (Headers '[Header "set-cookie" Coo.SetCookie] BSL.ByteString)
    logoutPost vault = case Vlt.lookup vltK vault of
        Nothing -> throwError $ err403 {errBody = "You're not logged in"} 
        Just sess -> do
            liftIO $ modifyIORef' sessions (M.delete $ UE.sid sess)
            loginHtml <- login
            return $ addHeader (mkCookie "0" "usersid" 0) loginHtml 

    users :: Vlt.Vault -> MyHandler BSL.ByteString
    users vault = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err303 {errHeaders = [("Location", "/protected/")]}
        conn <- askConn
        liftIO $ renderBST $ AH.users mSess conn

    cuttings :: Vlt.Vault -> MyHandler BSL.ByteString
    cuttings vault = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err303 {errHeaders = [("Location", "/protected/")]}
        conn <- askConn
        liftIO $ renderBST $ IH.cuttingsPage mSess conn

-------------------------------------------------------------
    species :: Vlt.Vault -> MyHandler BSL.ByteString
    species vault = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err303 {errHeaders = [("Location", "/protected/")]}
        conn <- askConn
        liftIO $ renderBST $ IH.species mSess conn

    specieNew :: Vlt.Vault -> MyHandler BSL.ByteString
    specieNew vault = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err303 {errHeaders = [("Location", "/protected/")]}
        conn <- askConn
        liftIO $ renderBST $ IH.specieNew mSess conn

    specie :: Vlt.Vault -> Key2 IE.Specie -> MyHandler BSL.ByteString
    specie vault specieK = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err303 {errHeaders = [("Location", "/protected/")]}
        conn <- askConn
        liftIO $ renderBST $ IH.specie mSess conn specieK

    speciePost :: Vlt.Vault -> IE.Specie -> MyHandler (Key IE.Specie)
    speciePost vault specie = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err403 {errBody = "You're not logged in"}
        conn <- askConn
        liftIO $ execStmt conn ID.saveSpecie specie

    specieDelete :: Vlt.Vault -> Key2 IE.Specie -> MyHandler ()
    specieDelete vault specieK = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err403 {errBody = "You're not logged in"}
        conn <- askConn
        liftIO $ execStmt conn ID.deleteSpecie specieK

    speciePatch :: Vlt.Vault -> Key2 IE.Specie -> Amount -> MyHandler ()
    speciePatch vault specieK price = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err403 {errBody = "You're not logged in"}
        conn <- askConn
        liftIO $ OD.setSpeciePrice conn specieK price

    specieImagePost :: Vlt.Vault -> Key2 IE.Specie -> SMP.MultipartData SMP.Mem -> MyHandler ()
    specieImagePost vault specieK mpData = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err403 {errBody = "You're not logged in"}
        let files = SMP.files mpData
            inputs = SMP.inputs mpData
        conn <- askConn

        let mDescr = lookup "description" $ map (\(SMP.Input d v) -> (d, v)) inputs
        when (isJust mDescr) $ do
            let descr' = fromJust mDescr 
            when (T.length descr' > 0) $ liftIO $ execStmt conn ID.saveSpecieDescr (specieK, descr')

        when (length files == 1) $ do
            let image = SMP.fdPayload $ head files
            when (BSL.length image > 0) $ liftIO $ execStmt conn ID.saveImage (specieK, BSL.toStrict image)

-------------------------------------------------------------
    vendors :: Vlt.Vault -> MyHandler BSL.ByteString
    vendors vault = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err303 {errHeaders = [("Location", "/protected/")]}
        conn <- askConn
        liftIO $ renderBST $ IH.vendors mSess conn

    vendorNew :: Vlt.Vault -> MyHandler BSL.ByteString
    vendorNew vault = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err303 {errHeaders = [("Location", "/protected/")]}
        conn <- askConn
        liftIO $ renderBST $ IH.vendorNew mSess conn

    vendorNewPost :: Vlt.Vault -> IE.Vendor-> MyHandler (Key IE.Vendor)
    vendorNewPost vault vendor = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err403 {errBody = "You're not logged in"}
        conn <- askConn
        liftIO $ execStmt conn ID.saveVendor vendor

-------------------------------------------------------------
    courriers :: Vlt.Vault -> MyHandler BSL.ByteString
    courriers vault = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err303 {errHeaders = [("Location", "/protected/")]}
        conn <- askConn
        liftIO $ renderBST $ OH.courriers mSess conn

    courrierNew :: Vlt.Vault -> MyHandler BSL.ByteString
    courrierNew vault = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err303 {errHeaders = [("Location", "/protected/")]}
        conn <- askConn
        liftIO $ renderBST $ OH.courrierNew mSess conn

    courrierNewPost :: Vlt.Vault -> OE.Courrier -> MyHandler (Key OE.Courrier)
    courrierNewPost vault courrier = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err403 {errBody = "You're not logged in"}
        conn <- askConn
        liftIO $ execStmt conn OD.saveCourrier courrier

    labels :: Vlt.Vault -> MyHandler BSL.ByteString
    labels vault = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err303 {errHeaders = [("Location", "/protected/")]}
        conn <- askConn
        liftIO $ renderBST $ IH.labels mSess conn

    labelNew :: Vlt.Vault -> MyHandler BSL.ByteString
    labelNew vault = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err303 {errHeaders = [("Location", "/protected/")]}
        conn <- askConn
        liftIO $ renderBST $ IH.labelNew mSess conn

    labelPost :: Vlt.Vault -> T.Text -> MyHandler ()
    labelPost vault label = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err403 {errBody = "You're not logged in"}
        conn <- askConn
        liftIO $ execStmt conn ID.saveLabelStmt $ IE.Label label

    labelDelete :: Vlt.Vault -> T.Text -> MyHandler ()
    labelDelete vault txt = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err403 {errBody = "You're not logged in"}
        conn <- askConn
        liftIO $ execStmt conn ID.deleteLabelStmt txt

-----------------------------------------------------------------------------------
    items :: Vlt.Vault -> MyHandler BSL.ByteString
    items vault = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err303 {errHeaders = [("Location", "/protected/")]}
        conn <- askConn
        liftIO $ renderBST $ IH.items mSess conn

    item :: Vlt.Vault -> Key2 IE.Item -> MyHandler BSL.ByteString
    item vault itemK = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err303 {errHeaders = [("Location", "/protected/")]}
        conn <- askConn
        liftIO $ renderBST $ IH.item mSess conn itemK

    itemDelete :: Vlt.Vault -> Key2 IE.Item -> MyHandler ()
    itemDelete vault itemK = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err403 {errBody = "You're not logged in"}
        conn <- askConn
        liftIO $ execStmt conn ID.deleteItem itemK

    itemPatch :: Vlt.Vault -> Key2 IE.Item -> Int16 -> MyHandler ()
    itemPatch vault itemK markup = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err403 {errBody = "You're not logged in"}
        conn <- askConn
        liftIO $ ID.setMarkup conn itemK markup

    itemNew :: Vlt.Vault -> MyHandler BSL.ByteString
    itemNew vault = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err303 {errHeaders = [("Location", "/protected/")]}
        conn <- askConn
        liftIO $ renderBST $ IH.itemNew mSess conn

    itemNewPost :: Vlt.Vault -> IE.Item -> MyHandler (Key2 IE.Item)
    itemNewPost vault item = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err403 {errBody = "You're not logged in"}
        conn <- askConn
        liftIO $ ID.saveItem conn item
        

-----------------------------------------------------------------------------------
    purchases :: Vlt.Vault -> MyHandler BSL.ByteString
    purchases vault = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err303 {errHeaders = [("Location", "/protected/")]}
        conn <- askConn
        liftIO $ renderBST $ IH.purchases mSess conn

    prchNew :: Vlt.Vault -> MyHandler BSL.ByteString
    prchNew vault = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err303 {errHeaders = [("Location", "/protected/")]}
        conn <- askConn
        liftIO $ renderBST $ IH.purchaseDoc mSess conn

    prchNewPost :: Vlt.Vault -> IE.PrchDoc -> MyHandler (Key IE.PrchHdr)
    prchNewPost vault prchDoc = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err403 {errBody = "You're not logged in"}
        conn <- askConn
        liftIO $ ID.savePrchDoc conn prchDoc

-----------------------------------------------------------------------------------
    stockSmmr :: Vlt.Vault -> MyHandler BSL.ByteString
    stockSmmr vault = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err303 {errHeaders = [("Location", "/protected/")]}
        conn <- askConn
        liftIO $ renderBST $ IH.stockSmmr mSess conn

    stockItem :: Vlt.Vault -> Key2 IE.Item -> MyHandler BSL.ByteString
    stockItem vault itemK = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err303 {errHeaders = [("Location", "/protected/")]}
        conn <- askConn
        liftIO $ renderBST $ IH.stockItem mSess conn itemK

    markItem :: Vlt.Vault -> MyHandler BSL.ByteString
    markItem vault = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err303 {errHeaders = [("Location", "/protected/")]}
        conn <- askConn
        liftIO $ renderBST $ IH.markItem mSess conn

    stockLabels :: Vlt.Vault -> MyHandler BSL.ByteString
    stockLabels vault = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err303 {errHeaders = [("Location", "/protected/")]}
        conn <- askConn
        liftIO $ renderBST $ IH.stockLabels mSess conn

    stockLabel :: Vlt.Vault -> Key2 IE.Token3 -> MyHandler BSL.ByteString
    stockLabel vault indK = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err303 {errHeaders = [("Location", "/protected/")]}
        conn <- askConn
        liftIO $ renderBST $ IH.stockLabel mSess conn indK

    labelWriteOffPost :: Vlt.Vault -> Key2 IE.Token3 -> MyHandler ()
    labelWriteOffPost vault indK = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err403 {errBody = "You're not logged in"}
        conn <- askConn
        liftIO $ ID.labelWrOff conn indK
------------------------------------
    stock :: Vlt.Vault -> Maybe (Key2 IE.Token3) -> Maybe (Key2 IE.Item) -> Maybe (Key2 IE.Token) -> Maybe (Key2 IE.Place) -> MyHandler BSL.ByteString
    stock vault mIndK mitemK mtokenK mplaceK = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err303 {errHeaders = [("Location", "/protected/")]}
        conn <- askConn
        liftIO $ renderBST $ IH.stock mSess conn mIndK mitemK mplaceK mtokenK

    stockWrOff :: Vlt.Vault -> Maybe (Key2 IE.Token3) -> Maybe (Key2 IE.Item) -> Maybe (Key2 IE.Token) -> Maybe (Key2 IE.Place) -> MyHandler BSL.ByteString
    stockWrOff vault mIndK mitemK mtokenK mplaceK = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err303 {errHeaders = [("Location", "/protected/")]}
        conn <- askConn
        liftIO $ renderBST $ IH.writeoff mSess conn mIndK mitemK mplaceK mtokenK

    stockWrOffPost :: Vlt.Vault -> IE.WriteOff 'IE.Lot3 -> MyHandler (Key2 (IE.WriteOff 'IE.Lot3))
    stockWrOffPost vault wroff = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err403 {errBody = "You're not logged in"}
        conn <- askConn
        liftIO $ ID.lotWrOff conn wroff
------------------------------------------

    stockLots :: Vlt.Vault -> MyHandler BSL.ByteString
    stockLots vault = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err303 {errHeaders = [("Location", "/protected/")]}
        conn <- askConn
        liftIO $ renderBST $ IH.stockLots mSess conn

    markItem2 :: Vlt.Vault -> Maybe (Key2 IE.Item) -> Maybe (Key IE.Place) -> Maybe (Key2 IE.Token) -> MyHandler BSL.ByteString
    markItem2 vault mitemK mplaceK mtokenK = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err303 {errHeaders = [("Location", "/protected/")]}
        conn <- askConn
        liftIO $ renderBST $ IH.labelStock mSess conn mitemK mtokenK mplaceK

    markItemPost :: Vlt.Vault -> IE.MarkItemDoc -> MyHandler (Key IE.MarkItemDoc)
    markItemPost vault doc = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err403 {errBody = "You're not logged in"}
        conn <- askConn
        liftIO $ ID.saveMarkItemDoc conn doc

    ords :: Vlt.Vault -> Maybe T.Text -> MyHandler BSL.ByteString
    ords vault pat = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err303 {errHeaders = [("Location", "/protected/")]}
        conn <- askConn
        liftIO $ renderBST $ OH.orders mSess conn $ fromMaybe "" pat

    ords2Pay :: Vlt.Vault -> MyHandler BSL.ByteString
    ords2Pay vault = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err303 {errHeaders = [("Location", "/protected/")]}
        conn <- askConn
        liftIO $ renderBST $ OH.ords2Pay mSess conn

    ords2Prepare :: Vlt.Vault -> MyHandler BSL.ByteString
    ords2Prepare vault = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err303 {errHeaders = [("Location", "/protected/")]}
        conn <- askConn
        liftIO $ renderBST $ OH.ords2Prepare mSess conn

    ords2Dispatch :: Vlt.Vault -> MyHandler BSL.ByteString
    ords2Dispatch vault = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err303 {errHeaders = [("Location", "/protected/")]}
        conn <- askConn
        liftIO $ renderBST $ OH.ords2Dispatch mSess conn

    ords2Deliver :: Vlt.Vault -> MyHandler BSL.ByteString
    ords2Deliver vault = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err303 {errHeaders = [("Location", "/protected/")]}
        conn <- askConn
        liftIO $ renderBST $ OH.ords2Deliver mSess conn

    orderRow :: Vlt.Vault -> Key2 (OE.OrderRow 'IE.Lot3) -> MyHandler BSL.ByteString
    orderRow vault rowK = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err303 {errHeaders = [("Location", "/protected/")]}
        conn <- askConn
        liftIO $ renderBST $ OH.orderRow mSess conn rowK

    orderRowPreparePost :: Vlt.Vault -> Key2 (OE.OrderRow 'IE.Lot3) -> V.Vector (Key2 IE.Place, Qty) -> MyHandler ()
    orderRowPreparePost vault rowK picklist = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err403 {errBody = "You're not logged in"}
        conn <- askConn
        liftIO $ OD.prepareOrdLot conn rowK picklist

    ordPaidPost :: Vlt.Vault -> OE.Order 'OE.Paid -> MyHandler ()
    ordPaidPost vault payment = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err403 {errBody = "You're not logged in"}
        conn <- askConn
        liftIO $ OD.savePayment conn payment

    ordDeliveredPost :: Vlt.Vault -> Key2 (OE.Order 'OE.Dispatched) -> MyHandler ()
    ordDeliveredPost vault payment = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err403 {errBody = "You're not logged in"}
        conn <- askConn
        liftIO $ OD.saveDelivery conn payment

    pmts :: Vlt.Vault -> MyHandler BSL.ByteString
    pmts vault = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err303 {errHeaders = [("Location", "/protected/")]}
        conn <- askConn
        liftIO $ renderBST $ OH.payments mSess conn


    dspPost :: Vlt.Vault -> (OE.DspHdr, V.Vector (Key2 (OE.Order 'OE.Paid))) -> MyHandler ()
    dspPost vault dsp = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err403 {errBody = "You're not logged in"}
        conn <- askConn
        liftIO $ OD.saveDispatch conn dsp

    dsps :: Vlt.Vault -> Maybe Day -> Maybe Day -> MyHandler BSL.ByteString
    dsps vault dayFrom dayTo = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err303 {errHeaders = [("Location", "/protected/")]}
        conn <- askConn
        let defFrom = fromGregorian 2020 4 1 
        let defTo = fromGregorian 2025 12 31 
        liftIO $ renderBST $ OH.dispatches mSess conn (fromMaybe defFrom dayFrom) (fromMaybe defTo dayTo)

    ordSummary :: Vlt.Vault -> Key2 OE.OrderHdr -> MyHandler BSL.ByteString
    ordSummary vault ordK = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err303 {errHeaders = [("Location", "/protected/")]}
        conn <- askConn
        liftIO $ renderBST $ OH.orderSummary mSess conn ordK

    ordStatusDelete :: Vlt.Vault -> Key2 OE.OrderHdr -> MyHandler ()
    ordStatusDelete vault ordK = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err403 {errBody = "You're not logged in"}
        conn <- askConn
        liftIO $ OD.statusBack conn ordK

    delivery :: Vlt.Vault -> MyHandler BSL.ByteString
    delivery vault = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err303 {errHeaders = [("Location", "/protected/")]}
        conn <- askConn
        liftIO $ renderBST $ AdH.delivery (fromJust mSess) conn

    deliveryPricePost :: Vlt.Vault -> Amount ->  MyHandler ()
    deliveryPricePost vault price = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err403 {errBody = "You're not logged in"}
        conn <- askConn
        liftIO $ execStmt conn OD.saveDlvPrice price

    deliveryDayPost :: Vlt.Vault -> Day ->  MyHandler ()
    deliveryDayPost vault day = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err403 {errBody = "You're not logged in"}
        conn <- askConn
        liftIO $ execStmt conn OD.setDlvDay day

    smsService :: Vlt.Vault -> SockAddr -> MyHandler BSL.ByteString
    smsService vault socket = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err303 {errHeaders = [("Location", "/protected/")]}
        conf <- ask
        liftIO $ renderBST $ AdH.smsService (fromJust mSess) conf socket

    smsServicePost :: Vlt.Vault -> SockAddr -> T.Text -> MyHandler [Sms.SendStatus]
    smsServicePost vault socket phone = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err403 {errBody = "You're not logged in"}
        conf <- ask
        liftIO $ Sms.sendTest conf socket phone

    visitorLog :: Vlt.Vault -> MyHandler BSL.ByteString
    visitorLog vault = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err303 {errHeaders = [("Location", "/protected/")]}
        conn <- askConn
        liftIO $ renderBST $ AdH.visitorLog (fromJust mSess) conn

    cms :: Vlt.Vault -> MyHandler BSL.ByteString
    cms vault = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err303 {errHeaders = [("Location", "/protected/")]}
        conn <- askConn
        liftIO $ renderBST $ AdH.cms mSess conn

    cmsUploadPost :: Vlt.Vault -> SMP.MultipartData SMP.Mem -> MyHandler ()
    cmsUploadPost vault mpData = do
        let mSess = Vlt.lookup vltK vault
        when (isNothing mSess) $ throwError $ err403 {errBody = "You're not logged in"}

        let files = SMP.files mpData
        when (length files /= 1) $ throwError $ err403 {errBody = "There shall be only one file"} 
        let rsrData = SMP.fdPayload $ head files
            rsrName = SMP.fdInputName $ head files
            mResource = lookup rsrName $ map (\a -> (tshow a , a)) $ enumFrom minBound
        when (isNothing mResource) $ throwError $ err403 {errBody = "Resource name is wrong"}
        conn <- askConn
        liftIO $ execStmt conn Cms.saveResource (fromJust mResource, BSL.toStrict rsrData)
        return ()


askConn = fst <$> ask

main :: IO ()
main = do
    smsConfH <- SIO.openFile "sms.conf" SIO.ReadMode
    smsTokenId <- BS.hGetLine smsConfH
    smsPwd <- BS.hGetLine smsConfH 
    let smsConf = Sms.Conf smsTokenId smsPwd
    SIO.hClose smsConfH

    (mPort, rootDir) <- getParams
    eiConn <- acquire dbSettings
    case eiConn of
        Right conn -> do
            putStrLn "Connected to database"
            clientsS <- newIORef M.empty
            usersS <- newIORef M.empty
            vltUserK <- Vlt.newKey 
            vltClientK <- Vlt.newKey 
--            testUser <- liftIO $ Db.execStmt conn User.Db.fetchUserStmt (Key 1) 
--            testLe <- liftIO $ Db.execStmt conn LegEnt.Db.fetchDefaultStmt $ User.key testUser
--            curTime <- liftIO getCurrentTime
--            let testSession = Session.Dc testUser testLe (addUTCTime (realToFrac 5000) curTime)
--            allSessions <- newIORef $ M.singleton "123456789" testSession
--
            forkIO (cancelOldOrders conn)
            forkIO (removeStaleSessions usersS clientsS)

            let application = myMiddleware2 (clientsS, usersS, vltClientK, vltUserK) (serve api $ hoistServer api (myToStdHandler (conn, smsConf)) $ serverFull rootDir clientsS usersS vltClientK vltUserK)
            if isNothing mPort then do
                let tlsOpts = tlsSettingsChain
                        ( rootDir ++ "/tls/crt")
                        [ rootDir ++ "/tls/ca-bundle"
                        ] $ rootDir ++ "/tls/key"
                    warpOpts = setPort 443 defaultSettings
                    httpsConf = EnforceHTTPS.defaultConfig
                forkIO $ run 80 $ removeWwwMw $ EnforceHTTPS.withConfig httpsConf $ \_ respond -> respond $ responseLBS NHT.status200 [] ""
                runTLS tlsOpts warpOpts $ removeWwwMw $ EnforceHTTPS.withConfig httpsConf application
            else
                run (fromJust mPort) application



            --run 8081 (serveWithContext api genAuthServerContext $ server conn)
            --run 8081 (myMiddleware2 (userSessions, allSessions, vaultSidK, vaultUserK) (serveWithContext api (genAuthServerContext (vaultSidK, allSessions)) $ hoistServerWithContext api proxyMyContext (myToStdHandler conn) $ serverFull allSessions vaultSidK vaultUserK))
--            run 8081 (serveWithContext api (genAuthServerContext allSessions) $ hoistServerWithContext api proxyMyContext (myToStdHandler conn) $ serverRdr allSessions)
        Left errMsg -> putStrLn $ fromMaybe "Cannot connect to database. Unspecified error." $ fmap (T.unpack . decodeUtf8) errMsg


removeWwwMw :: Application -> Application
removeWwwMw baseApp = \req responseFunc ->
    let mHost' =  lookup "host" (requestHeaders req) >>= BS.stripPrefix "www." 
        connType = if isSecure req then "https://" else "http://"
    in
        if isJust mHost' then
            responseFunc $ responseLBS NHT.status301 [(NHT.hContentType, "text/plain"), (NHT.hLocation, connType <> (fromJust mHost'))] "Redirect"
        else
            baseApp req responseFunc 
    
myMiddleware2 :: (ClientSessions, UserSessions, Vlt.Key CE.Session, Vlt.Key UE.Session) -> Application -> Application
myMiddleware2 (clientsS, usersS, vltClientK, vltUserK) baseApp = \req responseFunc -> do
    let headers = requestHeaders req
        pathInfo' = pathInfo req
    if (length pathInfo') > 0 && (head pathInfo') == "protected" then do
        usersS' <- liftIO $ readIORef usersS
        let mUserS = (lookup "cookie" headers) >>= (\c -> lookup "usersid" $ Coo.parseCookies c)
                >>= (\cp -> M.lookup cp usersS')
        case mUserS of
            Just userS -> do
                let vault' = Vlt.insert vltUserK userS (vault req)
                    req' = req { vault = vault' }
                baseApp req' responseFunc
            Nothing ->
                baseApp req responseFunc
    else do
        clientsS' <- liftIO $ readIORef clientsS
        let mClientS = (lookup "cookie" headers) >>= (\c -> lookup "clientsid" $ Coo.parseCookies c)
                >>= (\cp -> M.lookup cp clientsS')
        case mClientS of
            Just client -> do
                let vault' = Vlt.insert vltClientK client (vault req)
                    req' = req { vault = vault' }
                baseApp req' $ responseFunc . addHeaderMw ("Service-Worker-Allowed", "/") 
            Nothing ->
                baseApp req $ responseFunc . addHeaderMw ("Service-Worker-Allowed", "/") 

    where
    --addHeaderMw :: Header -> Response -> Response
    addHeaderMw h = mapResponseHeaders (\hs -> h:hs)

