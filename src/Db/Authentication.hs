{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Db.Authentication where


import Text.RawString.QQ (r)
import qualified Data.Text as T
import qualified Data.Vector as V
import Data.Functor.Contravariant (contramap)
import Hasql.Statement (Statement(..))
import qualified Hasql.Decoders as D
import qualified Hasql.Encoders as E

import Db(Encodable(..), Decodable(..), execStmt)
import qualified Entities.Common as E
import qualified Entities.User as UE 
import qualified Entities.Client as CE 


instance Encodable CE.Phone where
    encoder = contramap (\(CE.Phone n) -> n) $ E.param $ E.text
instance Decodable CE.Phone where
    decoder = CE.Phone <$> decoder

instance Encodable UE.UserRole where
    encoder :: E.Params UE.UserRole
    encoder = contramap (\case
        UE.Admin -> "adm" :: T.Text
        UE.Operator -> "opr") encoder
instance Decodable UE.UserRole where
    decoder :: D.Row UE.UserRole
    decoder = (\case
        ("adm" :: T.Text) -> UE.Admin
        "opr" -> UE.Operator)
        <$> decoder


--instance Encodable CE.Client where
--    encoder :: E.Params CE.Client
--    encoder = contramap (\(CE.Client ph) -> ph) encoder
--instance Decodable CE.Client where
--    decoder :: D.Row CE.Client
--    decoder = CE.Client <$> decoder

instance Encodable UE.User where
    encoder :: E.Params UE.User
    encoder = contramap (\(UE.User role nick name email) -> (role, nick, name, email)) encoder
instance Decodable UE.User where
    decoder :: D.Row UE.User
    decoder = UE.User
        <$> decoder
        <*> decoder
        <*> decoder
        <*> decoder

saveUser :: Statement (UE.User, T.Text) (E.Key2 UE.User)
saveUser = Statement query encoder (D.singleRow decoder) True
    where
    query = [r|
        insert into staff (roles, nick, name, email, pwd)
        values ($1, $2, $3, $4, $5)
        returning id
    |]

getUser:: Statement () (V.Vector (E.DbEntity UE.User))
getUser = Statement query E.unit (D.rowVector decoder) True
    where
    query = [r|
        select s.roles, s.nick, s.name, s.email, s.id, s.savedat, s.isblocked
        from staff s
    |]

checkPwd :: Statement (T.Text, T.Text) (Maybe (UE.User, E.Key2 UE.User))
checkPwd = Statement query encoder (D.rowMaybe decoder) True
    where
    query = [r|
        select s.roles, s.nick, s.name, s.email, s.id
        from staff s
        where s.nick = $1 and s.pwd = $2 and (not s.isblocked)
    |]

