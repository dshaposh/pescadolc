{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}

-- {-# LANGUAGE TypeInType #-}

module Db.Inventory where

import Text.RawString.QQ (r)
--import Data.Maybe (fromJust)
import GHC.Int(Int16, Int64)
import Data.Time.Calendar(Day, Day(ModifiedJulianDay))
import Data.Time.Clock (UTCTime(utctDay), getCurrentTime)
import Data.Maybe(fromJust, isJust, isNothing)
import qualified Data.Text as T
import qualified Data.ByteString as BS
import Data.Text.Encoding (decodeUtf8)
import qualified Data.Vector as V
import Control.Monad (when)
import Control.Monad.IO.Class (liftIO)

import Data.Functor.Contravariant (contramap)
import Hasql.Connection (Connection)
import Hasql.Statement (Statement(..))
import qualified Hasql.Decoders as D
import qualified Hasql.Encoders as E
import Hasql.Session (run, statement, Session)
import qualified Hasql.Transaction.Sessions as Trans
import qualified Hasql.Transaction as Trans


import Db(Encodable(..), Decodable(..), execStmt, crtTrxTagStmt)
import qualified Entities.Inventory as I
import qualified Entities.Common as E
import qualified Entities.Client as CE
import qualified Entities.Order as OE


instance Encodable I.Item where
    encoder :: E.Params I.Item
    encoder = contramap (\(I.Item n o s c u m mr) -> (n, o, s, c, u, m, mr)) encoder
instance Encodable (V.Vector I.Item) where
    encoder :: E.Params (V.Vector I.Item)
    encoder = contramap (fmap (\(I.Item n o s c u m mr) -> (n, o, s, c, u, m, mr))) encoder
instance Decodable I.Item where
    decoder :: D.Row I.Item
    decoder = I.Item
        <$> decoder
        <*> decoder
        <*> decoder
        <*> decoder
        <*> decoder
        <*> decoder
        <*> decoder

saveItems :: Statement (V.Vector I.Item) (V.Vector (E.Key I.Item))
saveItems = Statement query encoder (D.rowVector decoder) True
    where
    query = [r|
        insert into item (name, ordnum, specie, cutting, uom, minsztp, minszval, markup)
        select unnest($1), unnest($2), unnest($3), unnest($4), unnest($5), unnest($6), unnest($7), unnest($8)
        returning id
        |]

saveItemStmt :: Statement I.Item (E.Key2 I.Item)
saveItemStmt = Statement query encoder (D.singleRow decoder) True
    where
    query = [r|
        insert into item (name, ordnum, specie, cutting, uom, minsztp, minszval, markup)
        values ($1, $2, $3, $4, $5, $6, $7, $8)
        returning id
        |]

getItemById :: Statement (E.Key2 I.Item) (E.FromDb I.Item, I.ItemDescr)
getItemById = Statement query encoder (D.singleRow decoder) True
    where
    query = [r|
        select name, ordnum, specie, cutting, uom, minsztp, minszval, markup, id, savedat, isclosed
            , sname, cname, uname, price
        from item_v 
        where id=$1
        |]

deleteItem :: Statement (E.Key2 I.Item) ()
deleteItem = Statement query encoder D.unit True
    where
    query = [r|
        update item set isclosed=true where id=$1
        |]
patchItem :: Statement (E.Key2 I.Item, Int16) ()
patchItem = Statement query encoder D.unit True
    where
    query = [r|
        update item set markup=$2 where id=$1
        |]

getItems :: Statement (Bool) (V.Vector (E.FromDb I.Item, I.ItemDescr))
getItems = Statement query encoder (D.rowVector decoder) True
    where
    query = [r|
        select name, ordnum, specie, cutting, uom, minsztp, minszval, markup, id, savedat, isclosed
            , sname, cname, uname, price
        from item_v
        where ($1 or (not isclosed))
        order by specie asc
        |]

getItemsForSel :: Statement () (V.Vector (I.Item, E.Key I.Item))
getItemsForSel = Statement query E.unit (D.rowVector decoder) True
    where
    query = [r|
        select name, ordnum, specie, cutting, uom, minsztp, minszval, markup, id
        from item
        where (not isclosed)
        order by ordnum asc
        |]

setMarkupTrx :: (E.Key2 I.Item, Int16) -> Trans.Transaction ()
setMarkupTrx (itemK, markup) = do
    --specieK <- Trans.statement itemK getSpecieK
    spPrice <- Trans.statement itemK getSpeciePrice
    let price = E.prettyPrice $ ((fromIntegral markup) * (fromIntegral spPrice)) `div` 100
    Trans.statement (itemK, markup, price) setMarkup 

    where
    setMarkup :: Statement (E.Key2 I.Item, Int16, E.Amount) ()
    setMarkup = Statement query encoder D.unit True
        where
        query = [r|
            update item set markup=$2, price=$3 where id=$1
            |]
    getSpeciePrice :: Statement (E.Key2 I.Item) E.Amount
    getSpeciePrice = Statement query encoder (D.singleRow decoder) True
        where
        query = [r|
            select s.price
            from specie s
            inner join item i
            on s.id=i.specie
            where i.id=$1
            |]

setMarkup :: Connection -> E.Key2 I.Item -> Int16 -> IO ()
setMarkup conn itemK markup  = do
    eiResult <- run (Trans.transaction Trans.ReadCommitted Trans.Write dbTransaction) conn
    case eiResult of
        Left err -> fail $ show err
        Right result -> return result
    where
    dbTransaction = do
        setMarkupTrx (itemK, markup)


saveItem :: Connection -> I.Item -> IO (E.Key2 I.Item)
saveItem conn item = do
    eiResult <- run (Trans.transaction Trans.ReadCommitted Trans.Write dbTransaction) conn
    case eiResult of
        Left err -> fail $ show err
        Right result -> return result
    where
    dbTransaction = do
        itemK <- Trans.statement item saveItemStmt
        setMarkupTrx (itemK, I.markup item)
        return itemK


----------------------------------------------------------------
instance Decodable I.Specie where
    decoder :: D.Row I.Specie
    decoder = I.Specie
        <$> decoder
        <*> decoder
        <*> decoder
        <*> decoder
instance Encodable I.Specie where
    encoder :: E.Params I.Specie
    encoder = contramap (\(I.Specie n o u p) -> (n, o, u, p)) encoder

getSpecies :: Statement (Bool) (V.Vector (E.FromDb I.Specie))
getSpecies = Statement query encoder (D.rowVector decoder) True
    where
    query = [r|
        select s.name, s.ordnum, s.uom, s.price, s.id, u.name, s.savedat, s.isclosed
        from specie s
        inner join uom u
        on s.uom=u.id
        where ($1 or (not s.isclosed))
        |]
getSpeciesDescr :: Statement (V.Vector (E.Key I.Specie)) (V.Vector T.Text)
getSpeciesDescr = Statement query encoder (D.rowVector decoder) True
    where
    query = [r|
        select s.description
        from specie s
        inner join (
            select d.id, row_number() over () as rnum from unnest($1) as d(id) 
        ) d
        on s.id=d.id
        order by d.rnum asc
        |]

getSpecieById :: Statement (E.Key2 I.Specie) (E.FromDb I.Specie)
getSpecieById = Statement query encoder (D.singleRow decoder) True
    where
    query = [r|
        select s.name, s.ordnum, s.uom, s.price, s.id, u.name, s.savedat, s.isclosed
        from specie s
        inner join uom u
        on s.uom=u.id
        where s.id=$1
        |]

getImage :: Statement (E.Key2 I.Specie) (BS.ByteString, UTCTime)
getImage = Statement query encoder (D.singleRow decoder) True
    where
    query = [r|
        select image, imgsavedat
        from specie
        where id=$1
        |]

saveSpecie :: Statement (I.Specie) (E.Key2 I.Specie)
saveSpecie = Statement query encoder (D.singleRow decoder) True
    where
    query = [r|
        insert into specie (name, ordnum, uom, price)
        values ($1, $2, $3, $4)
        returning id
        |]

deleteSpecie :: Statement (E.Key2 I.Specie) ()
deleteSpecie = Statement query encoder D.unit True
    where
    query = [r|
        update specie set isclosed=true where id=$1
        |]

saveImage :: Statement (E.Key2 I.Specie, BS.ByteString) ()
saveImage= Statement query encoder D.unit True
    where
    query = [r|
        update specie set image=$2, imgsavedat=(now() at time zone 'utc') where id=$1
        |]

saveSpecieDescr :: Statement (E.Key2 I.Specie, T.Text) ()
saveSpecieDescr = Statement query encoder D.unit True
    where
    query = [r|
        update specie set description=$2 where id=$1
        |]

patchSpecie :: Statement (E.Key2 I.Specie, E.Amount) ()
patchSpecie = Statement query encoder D.unit True
    where
    query = [r|
        update specie set price=$2 where id=$1
        |]

----------------------------------------------------------------
instance Decodable I.Cutting where
    decoder :: D.Row I.Cutting
    decoder = I.Cutting
        <$> decoder
        <*> decoder
getCuttings :: Statement (Bool) (V.Vector (E.FromDb I.Cutting))
getCuttings = Statement query encoder (D.rowVector decoder) True
    where
    query = [r|
        select name, ordnum, id, savedat, isclosed
        from cutting
        where ($1 or (not isclosed))
        |]
----------------------------------------------------------------
instance Decodable I.Uom where
    decoder :: D.Row I.Uom
    decoder = I.Uom
        <$> decoder
        <*> decoder
getUoms :: Statement (Bool) (V.Vector (E.FromDb I.Uom))
getUoms = Statement query encoder (D.rowVector decoder) True
    where
    query = [r|
        select name, ordnum, id, savedat, isclosed
        from uom
        where ($1 or (not isclosed))
        |]

----------------------------------------------------------------
instance Decodable I.Place where
    decoder :: D.Row I.Place
    decoder = I.Place
        <$> decoder
        <*> decoder
getPlaceByK :: Statement (E.Key2 I.Place) I.Place
getPlaceByK = Statement query encoder (D.singleRow decoder) True
    where
    query = [r|
        select name, ordnum from place where id=$1
        |]
getPlaces :: Statement (Bool) (V.Vector (E.FromDb I.Place))
getPlaces = Statement query encoder (D.rowVector decoder) True
    where
    query = [r|
        select name, ordnum, id, savedat, isclosed
        from place
        where ($1 or (not isclosed))
        |]

----------------------------------------------------------------
instance Decodable I.Vendor where
    decoder :: D.Row I.Vendor
    decoder = I.Vendor
        <$> decoder
        <*> decoder
        <*> decoder
        <*> decoder
instance Encodable I.Vendor where
    encoder :: E.Params I.Vendor
    encoder = contramap (\(I.Vendor name ordnum taxid phone) -> (name, ordnum, taxid, phone)) encoder

getVendors :: Statement (Bool) (V.Vector (E.FromDb I.Vendor))
getVendors = Statement query encoder (D.rowVector decoder) True
    where
    query = [r|
        select name, ordnum, taxid, phone, id, savedat, isclosed
        from vendor
        where ($1 or (not isclosed))
        |]
saveVendor :: Statement (I.Vendor) (E.Key I.Vendor)
saveVendor = Statement query encoder (D.singleRow decoder) True
    where
    query = [r|
        insert into vendor (name, ordnum, taxid, phone)
        values ($1, $2, $3, $4) 
        returning id
        |]

----------------------------------------------------------------
instance Decodable I.Token where
    decoder :: D.Row I.Token
    decoder = I.Token
        <$> decoder
        <*> decoder
        <*> decoder
instance Encodable I.Token where
    encoder :: E.Params I.Token
    encoder = contramap (\(I.Token code day note) -> (code, day, note)) encoder
saveTokenStmt :: Statement I.Token (E.Key I.Token)
saveTokenStmt = Statement query encoder (D.singleRow decoder) True
    where
    query = [r|
        insert into token (code, day, note)
        values ($1, $2, $3)
        returning id
        |]
getTokenByK :: Statement (E.Key2 I.Token) I.Token 
getTokenByK = Statement query encoder (D.singleRow decoder) True
    where
    query = [r|
        select code, day, note from token where id=$1
        |]

----------------------------------------------------------------
instance Decodable I.Label where
    decoder :: D.Row I.Label
    decoder = I.Label <$> decoder
instance Encodable I.Label where
    encoder :: E.Params I.Label
    encoder = contramap (\(I.Label name) -> name) encoder
saveLabelStmt :: Statement I.Label ()
saveLabelStmt = Statement query encoder D.unit True
    where
    query = [r|
        insert into label (name)
        values ($1)
        |]
deleteLabelStmt :: Statement T.Text ()
deleteLabelStmt = Statement query encoder D.unit True
    where
    query = [r|
        delete from label where name=$1
        |]
getLabelsStmt :: Statement ()  (V.Vector T.Text)
getLabelsStmt = Statement query encoder (D.rowVector decoder) True
    where
    query = [r|
        select name from label
        |]

avlbLabelsStmt :: Statement ()  (V.Vector T.Text)
avlbLabelsStmt = Statement query encoder (D.rowVector decoder) True
    where
    query = [r|
        select name 
        from label l
        left join (
            select l.label
            from labeled l
            where l.place is not null
        ) m
        on m.label=l.name
        where (m.label is null)
        |]
----------------------------------------------------------------
instance Decodable I.PrchHdr where
    decoder :: D.Row I.PrchHdr
    decoder = I.PrchHdr
        <$> decoder
        <*> decoder
        <*> decoder
        <*> decoder
instance Encodable I.PrchHdr where
    encoder :: E.Params I.PrchHdr
    encoder = contramap (\(I.PrchHdr vend docday docnum note) -> (vend, docday, docnum, note)) encoder
getPrchHdr :: Statement (E.Key I.PrchHdr) (E.FromDb I.PrchHdr)
getPrchHdr = Statement query encoder (D.singleRow decoder) True
    where
    query = [r|
        select vendor, docday, docnum, note, id, amount
        from purchase_header
        where id = $1
        |]

getPrchHdrs :: Statement () (V.Vector (E.FromDb I.PrchHdr))
getPrchHdrs = Statement query encoder (D.rowVector decoder) True
    where
    query = [r|
        select vendor, docday, docnum, note, id, amount
        from purchase_header
        |]

savePrchHdrStmt :: Statement (I.PrchHdr, E.Key I.Token) (E.Key I.PrchHdr)
savePrchHdrStmt = Statement query encoder (D.singleRow decoder) True
    where
    query = [r|
        insert into purchase_header (vendor, docday, docnum, note, token)
        values ($1, $2, $3, $4, $5)
        returning id
        |]

----------------------------------------------------------------
instance Decodable I.PrchRow where
    decoder :: D.Row I.PrchRow
    decoder = I.PrchRow
        <$> decoder
        <*> decoder
        <*> decoder
        <*> decoder
instance Encodable (V.Vector I.PrchRow) where
    encoder :: E.Params (V.Vector I.PrchRow)
    encoder = contramap (fmap (\(I.PrchRow item place qty am) -> (item, place, qty, am))) encoder

getPrchRow :: Statement (E.Key I.PrchRow) (E.FromDb I.PrchRow)
getPrchRow = Statement query encoder (D.singleRow decoder) True
    where
    query = [r|
        select item, place, qty, amount, id, header
        from purchase_row
        where id = $1
        |]

crtPrchRowsStmt :: Statement (V.Vector I.PrchRow, E.Key I.PrchHdr) (V.Vector (E.Key I.PrchRow))
crtPrchRowsStmt = Statement query encoder (D.rowVector decoder) True
    where
    query = [r|
        insert into purchase_row (item, place, header)
        select unnest ($1), unnest($2), $5
        returning id
        |]


postPrchTrx :: (V.Vector (E.Key I.PrchRow), V.Vector E.Qty, V.Vector E.Amount) -> Trans.Transaction ()
postPrchTrx (rowKs, qtys, amts) = do
    trxK <- Trans.statement "pr" crtTrxTagStmt
    (hdrK, tknK) <- Trans.statement rowKs getHdrTkn -- shall throw an error if the rows belong to different headers
    Trans.statement (rowKs, qtys, amts) updPrchRowsStmt --OK
    Trans.statement (hdrK, V.sum amts) updPrchHdrStmt
    Trans.statement rowKs crtBalanceStmt
    Trans.statement rowKs crtMerchsStmt
    Trans.statement (rowKs, qtys, amts) updBalancesStmt
    Trans.statement (rowKs, qtys, amts) updMerchsStmt

    Trans.statement (hdrK, trxK, tknK, rowKs, qtys, amts) crtPrchTrxStmt
    where
    getHdrTkn :: Statement (V.Vector (E.Key I.PrchRow)) (E.Key I.PrchHdr, E.Key I.Token)
    getHdrTkn = Statement query encoder (D.singleRow decoder) True
        where
        query = [r|
            select distinct h.id, h.token
            from unnest ($1) as u(id)
            inner join purchase_row r
            on u.id=r.id
            inner join purchase_header h
            on h.id = r.header
            |]
    updPrchRowsStmt :: Statement (V.Vector (E.Key I.PrchRow), V.Vector E.Qty, V.Vector E.Amount) ()
    updPrchRowsStmt = Statement query encoder D.unit True
        where
        query = [r|
            update purchase_row r set qty=r.qty+d.qty, amount=r.amount+d.amount
            from unnest($1, $2, $3) as d(id, qty, amount)
            where r.id=d.id
            |]
    crtMerchsStmt :: Statement (V.Vector (E.Key I.PrchRow)) ()
    crtMerchsStmt = Statement query encoder D.unit True
        where
        query = [r|
            insert into merchandise_lot (item, token)
            select distinct r.item, h.token
            from unnest($1) as n(rowid)
            inner join purchase_row r
            on r.id = n.rowid
            inner join purchase_header h
            on r.header = h.id
            left join merchandise_lot m
            on m.item=r.item and m.token=h.token
            where m.item is null
            |]
    crtBalanceStmt :: Statement (V.Vector (E.Key I.PrchRow)) ()
    crtBalanceStmt = Statement query encoder D.unit True
        where
        query = [r|
            insert into inventory_balance (item, token, place)
            select r.item, h.token, r.place
            from unnest($1) as n(rowid)
            inner join purchase_row r
            on r.id = n.rowid
            inner join purchase_header h
            on r.header = h.id
            left join inventory_balance b
            on b.item=r.item and b.token=h.token and b.place=r.place
            where b.item is null
            |]

    updBalancesStmt :: Statement (V.Vector (E.Key I.PrchRow), V.Vector E.Qty, V.Vector E.Amount) ()
    updBalancesStmt = Statement query encoder D.unit True
        where
        query = [r|
            update inventory_balance b set qty=b.qty+d.qty, amount=b.amount+d.amount
            from unnest($1, $2, $3) as d(id, qty, amount)
            inner join purchase_row r
            on r.id = d.id
            inner join purchase_header h
            on h.id = r.header
            where b.item=r.item and b.token=h.token and b.place=r.place
            |]

    updMerchsStmt :: Statement (V.Vector (E.Key I.PrchRow), V.Vector E.Qty, V.Vector E.Amount) ()
    updMerchsStmt = Statement query encoder D.unit True
        where
        query = [r|
            update merchandise_lot m set qty=m.qty+f.qty, amount=m.amount+f.amount
            from (
                select r.item, r.header, sum(d.qty) as qty, sum(d.amount) as amount
                from unnest($1, $2, $3) as d(id, qty, amount)
                inner join purchase_row r
                on r.id = d.id
                group by r.item, r.header
            ) f
            inner join purchase_header h
            on h.id = f.header
            where m.item=f.item and m.token=h.token
            |]

    updPrchHdrStmt :: Statement (E.Key I.PrchHdr, E.Amount) ()
    updPrchHdrStmt = Statement query encoder D.unit True
        where
        query = [r|
            update purchase_header h set amount=h.amount+$2 where h.id=$1
            |]
    crtPrchTrxStmt :: Statement (E.Key I.PrchHdr, Int64, E.Key I.Token, V.Vector (E.Key I.PrchRow), V.Vector E.Qty, V.Vector E.Amount) ()
    crtPrchTrxStmt = Statement query encoder D.unit True
        where
        query = [r|
            insert into purchase_trx (header, trxtag, token, docrow, item, place, qty, amount)
            select $1, $2, $3, r.id, r.item, r.place, d.qty, d.amount
            from unnest($4, $5, $6) as d(id, qty, amount) 
            inner join purchase_row r
            on r.id = d.id
            |]


savePrchDoc :: Connection -> I.PrchDoc -> IO (E.Key I.PrchHdr)
savePrchDoc conn (hdr, rows) = do
    eiResult <- run (Trans.transaction Trans.ReadCommitted Trans.Write dbTransaction) conn
    case eiResult of
        Left err -> fail $ show err
        Right result -> return result
    where
    I.PrchHdr{..} = hdr
    dbTransaction = do
        let token = I.Token docnum docday $ E.tshow hdr 
        tknK <- Trans.statement token saveTokenStmt
        hdrK <- Trans.statement (hdr, tknK) savePrchHdrStmt
        rowKs <- Trans.statement (rows, hdrK) crtPrchRowsStmt
        let (qtys, amts) = V.unzip $ V.map (\(I.PrchRow _ _ qty am) -> (qty, am)) rows
        postPrchTrx (rowKs, qtys, amts)

        return hdrK


instance Decodable I.Where where
    decoder :: D.Row I.Where
    decoder = (\(tag :: T.Text) key -> case tag of
        "pl" -> I.AtPlace key
        "wo" -> I.AtWo key
        "so" -> I.AtSo key)
        <$> decoder
        <*> decoder
instance Encodable I.Where where
    encoder :: E.Params I.Where
    encoder = contramap f encoder
        where
        f :: I.Where -> (T.Text, Int64)
        f (I.AtPlace key) = ("pl", key)
        f (I.AtWo key) = ("wo", key)
        f (I.AtSo key) = ("so", key)

--instance Decodable I.Origin where
--    decoder :: D.Row I.Origin
--    decoder = (\(tag :: T.Text) key -> case tag of
--        "pr" -> I.FromPrch key
--        "wo" -> I.FromWo key)
--        <$> decoder
--        <*> decoder
--instance Encodable I.Origin where
--    encoder :: E.Params I.Origin
--    encoder = contramap f encoder
--        where
--        f :: I.Origin -> (T.Text, Int64)
--        f (I.FromPrch key) = ("pr", key)
--        f (I.FromWo key) = ("wo", key)

------------------------------------------------------------------
--instance Decodable I.WoHdr where
--    decoder :: D.Row I.WoHdr
--    decoder = I.WoHdr
--        <$> decoder
--        <*> decoder
--        <*> decoder
--        <*> decoder
--instance Encodable I.WoHdr where
--    encoder :: E.Params I.WoHdr
--    encoder = contramap (\(I.WoHdr origin docday docnum note) -> (origin, docday, docnum, note)) encoder
--
--getWoHdr :: Statement (E.Key I.WoHdr) (E.FromDb I.WoHdr)
--getWoHdr = Statement query encoder (D.singleRow decoder) True
--    where
--    query = [r|
--        select oritp, oriid, day, docnum, note, id, isclosed, savedat
--        from workoder_header
--        where id = $1
--        |]
--
--saveWoHdr :: Statement (I.WoHdr) (E.Key I.WoHdr)
--saveWoHdr = Statement query encoder (D.singleRow decoder) True
--    where
--    query = [r|
--        insert into workoder_header (oritp, oriid, docday, docnum, note)
--        values ($1, $2, $3, $4, $5)
--        returning id
--        |]

----------------------------------------------------------------


instance Decodable I.Size where
    decoder :: D.Row I.Size
    decoder = (\(tag :: T.Text) size -> case tag of
        "nl" -> I.NoSize
        "fs" -> I.Fixed $ fromJust size
        "vs" -> I.Variable)
        <$> decoder
        <*> decoder

instance Encodable I.Size where
    encoder :: E.Params I.Size
    encoder = contramap f encoder
        where
        f :: I.Size -> (T.Text, Maybe E.Qty)
        f I.NoSize = ("nl", Nothing)
        f (I.Fixed size) = ("fs", Just size)
        f I.Variable = ("vs", Nothing)

instance Encodable (V.Vector I.Size) where
    encoder :: E.Params (V.Vector I.Size)
    encoder = contramap (fmap f) encoder
        where
        f :: I.Size -> (T.Text, Maybe E.Qty)
        f I.NoSize = ("nl", Nothing)
        f (I.Fixed size) = ("fs", Just size)
        f I.Variable = ("vs", Nothing)

instance Decodable I.Balance where
    decoder :: D.Row I.Balance
    decoder = I.Balance
        <$> decoder
        <*> decoder
        <*> decoder
        <*> decoder
        <*> decoder


merchSmmrStmt :: Statement () (V.Vector (I.Merch2 'I.Lot3, T.Text, T.Text, I.Size, Day, E.Amount, E.Qty))
merchSmmrStmt = Statement query E.unit (D.rowVector decoder) True
    where
    query = [r|
        select m.item, m.token, m.qty, i.name, u.name, i.minsztp, i.minszval, t.day, m.amount, m.rsqty
        from (
            select m.item, m.token, sum(m.qty) :: int as qty
                    , sum(m.amount) :: bigint as amount, sum(m.rsqty) as rsqty
            from merchandise m
            group by m.item, m.token
            having sum(m.qty) > 0
        ) m
        inner join item i
        on m.item=i.id
        inner join token t
        on m.token=t.id
        inner join uom u
        on i.uom=u.id
        order by i.specie asc
        |]

stockItemStmt :: Statement (E.Key2 I.Item) (V.Vector (I.Stock, I.Token, T.Text))
stockItemStmt = Statement query encoder (D.rowVector decoder) True
    where
    query = [r|
        select b.tag, b.token2, b.item, b.token, b.place, b.qty, b.amount
            , b.tcode, b.tday, b.tnote
            , b.pname
        from inventory_balance_v b
        where b.item = $1
    |]

stockMerchLot :: Statement (I.MerchK 'I.Lot3) (V.Vector (I.Stock3 'I.Lot3, T.Text))
stockMerchLot = Statement query encoder (D.rowVector decoder) True
    where
    query = [r|
        select b.item, b.token, b.place, b.qty, b.amount, p.name
        from inventory_balance b
        inner join place p
        on p.id=b.place
        where b.item = $1 and b.token = $2
    |]

getStockLot :: Statement (I.StockK 'I.Lot3) (I.Stock3 'I.Lot3)
getStockLot = Statement query encoder (D.singleRow decoder) True
    where
    query = [r|
        select b.item, b.token, b.place, b.qty, b.amount
        from inventory_balance b
        where b.item = $1 and b.token = $2 and b.place=$3
    |]

instance Decodable I.Token2 where
    decoder :: D.Row I.Token2
    decoder = I.Token2
        <$> decoder
        <*> decoder
        <*> decoder
        <*> decoder
        <*> decoder
        <*> decoder
instance Encodable I.Token2 where
    encoder :: E.Params I.Token2
    encoder = contramap (\(I.Token2 code token item qty amount wh) -> (code, token, item, qty, amount, wh)) encoder

instance Decodable I.MarkItemDoc where
    decoder :: D.Row I.MarkItemDoc
    decoder = I.MarkItemDoc
        <$> decoder
        <*> decoder
        <*> decoder
instance Encodable I.MarkItemDoc where
    encoder :: E.Params I.MarkItemDoc
    encoder = contramap (\(I.MarkItemDoc origin token note) -> (origin, token, note)) encoder


saveMarkItemDoc :: Connection -> I.MarkItemDoc -> IO (E.Key I.MarkItemDoc)
saveMarkItemDoc conn doc = do
    currTime <- liftIO getCurrentTime --temporary. for saving in label creation doc
    eiResult <- run (Trans.transaction Trans.ReadCommitted Trans.Write $ dbTransaction currTime) conn
    case eiResult of
        Left err -> fail $ show err
        Right result -> return result
    where
    I.MarkItemDoc{..} = doc
    I.Token2{..} = token2
    dbTransaction currTime = do
        (qtyTotal, amountTotal) <- Trans.statement from getQtyAmtStmt
        let amtDelta = round $ ((fromIntegral qty) :: Double) / (fromIntegral qtyTotal) * (fromIntegral $ E.unwrapAmount amountTotal)
            token2' = token2 { I.amount = amtDelta } :: I.Token2
            doc' = doc {I.token2 = token2'} :: I.MarkItemDoc
        -- to check that label is not occupied
        docK <- Trans.statement doc' saveMarkItemDocStmt
        Trans.statement (from, (-1)*qty, (-1)*amtDelta) updBalanceStmt
        let (_, itemK, tokenK) = from
        Trans.statement (I.MerchLot2 (I.MerchLotK (E.Key2 itemK) (E.Key2 tokenK)) qty , amtDelta) updMerchLotStmt

        let I.AtPlace place = wh
            labeled = I.Token3 code
                $ I.StockLot
                    (I.StockLotK (I.MerchLotK (E.Key2 item) (E.Key2 token)) (E.Key2 place))
                    qty
                    amount
        trxK <- Trans.statement "lb" crtTrxTagStmt
        lblK <- Trans.statement (trxK, place, utctDay currTime, note) saveLabeledMvmt
        Trans.statement (labeled, lblK) saveLabeled

        return docK

    saveMarkItemDocStmt :: Statement I.MarkItemDoc (E.Key I.MarkItemDoc)
    saveMarkItemDocStmt = Statement query encoder (D.singleRow decoder) True
        where
        query = [r|
            insert into markitem (place, item, token
                , tkcode, tkitem, qty, amount, docwhtp, docwhid, note
                , tkwhtp, tkwhid)
            values ($1, $2, $3, $4, $6, $7, $8, $9, $10, $11, $9, $10)
            returning id
        |]

    saveLabeledMvmt :: Statement (Int64, Int64, Day, T.Text) (E.Key2 I.Token3)
    saveLabeledMvmt = Statement query encoder (D.singleRow decoder) True
        where
        query = [r|
            insert into labeled_mvmt (tp, trxid, place, day, note)
            values ('cr', $1, $2, $3, $4)
            returning id
        |]
    saveLabeled :: Statement (I.Token3, E.Key2 I.Token3) ()
    saveLabeled = Statement query encoder D.unit True
        where
        query = [r|
            insert into labeled (label, item, token, place, qty, cost, id, lastdoc)
            values ($1, $2, $3, $4, $5, $6, $7, $7)
        |]

    -- Shall be removed, gerenal version shall be used instead (below)
    updMerchLotStmt :: Statement ((I.Merch2 'I.Lot3), E.Amount) ()
    updMerchLotStmt = Statement query encoder D.unit True
        where
        query = [r|
            update merchandise_lot m set qty=m.qty-$3, amount=m.amount-$4
            where m.item=$1 and m.token=$2
            |]

    -- Shall be removed, gerenal version shall be used instead (below)
    updBalanceStmt :: Statement (E.Key I.Balance, E.Qty, E.Amount) ()
    updBalanceStmt = Statement query encoder D.unit True
        where
        query = [r|
            update inventory_balance b set qty=b.qty+$4, amount=b.amount+$5
            where b.place=$1 and b.item=$2 and b.token=$3
            |]

updBalanceStmt :: Statement (I.StockK 'I.Lot3, E.Qty, E.Amount) ()
updBalanceStmt = Statement query encoder D.unit True
    where
    query = [r|
        update inventory_balance b set qty=b.qty+$4, amount=b.amount+$5
        where b.item=$1 and b.token=$2 and b.place=$3
        |]
updMerchLotStmt :: Statement ((I.MerchK 'I.Lot3), E.Qty, E.Amount, E.Qty) ()
updMerchLotStmt = Statement query encoder D.unit True
    where
    query = [r|
        update merchandise_lot m set qty=m.qty+$3, amount=m.amount+$4, rsqty=m.rsqty+$5
        where m.item=$1 and m.token=$2
        |]


getBalancesStmt :: Statement Bool (V.Vector (I.Balance, T.Text, T.Text, T.Text, T.Text))
getBalancesStmt = Statement query encoder (D.rowVector decoder) True
    where
    query = [r|
        select b.place, b.item, b.token, b.qty, b.amount
            , i.name, u.name, p.name, t.code
        from inventory_balance b
        inner join item i
        on i.id = b.item
        inner join token t
        on t.id = b.token
        inner join place p
        on p.id=b.place
        inner join uom u
        on u.id=i.uom
        where b.qty > 0
        |]
getIndsStmt :: Statement () (V.Vector (I.Token3, E.Key2 I.Token3))
getIndsStmt = Statement query encoder (D.rowVector decoder) True
    where
    query = [r|
        select l.label, l.item, l.token, l.place, l.qty, l.cost, l.id
        from labeled l
        where l.place is not null
            |]


getStoreFront :: Statement () (V.Vector (I.Stock, I.Item, Day, T.Text))
getStoreFront = Statement query E.unit (D.rowVector decoder) True
    where
    query = [r|
        select b.tag, b.token2, b.item, b.token, b.place, b.qty, b.amount
            , b.iname, b.irdnum, b.ispecie, b.icutting, b.iuom, b.iminsztp, b.iminszval, b.imarkup
            , b.tday, b.uname
        from inventory_balance_v b
        where b.qty > 0 and (b.iminsztp <> 'vs' or b.tag = 'id')
        |]


getQtyAmtStmt :: Statement (E.Key I.Balance) (E.Qty, E.Amount)
getQtyAmtStmt = Statement query encoder (D.singleRow decoder) True
    where
    query = [r|
        select b.qty, b.amount
        from inventory_balance b
        where b.place = $1 and b.item = $2 and b.token = $3
        |]

--------------------------------------------------------------------------------
instance Decodable I.StockKey where
    decoder :: D.Row I.StockKey
    decoder = I.StockKey
        <$> decoder
        <*> decoder
        <*> decoder
instance Decodable I.Token22 where
    decoder :: D.Row I.Token22
    decoder = I.Token22
        <$> decoder
instance Decodable I.Lot where
    decoder :: D.Row I.Lot
    decoder = I.Lot
        <$> decoder
        <*> decoder
        <*> decoder
instance Decodable I.Stock where
    decoder :: D.Row I.Stock
    decoder = (\(tag :: T.Text) tokenK lot -> case tag of
        "lt" -> I.FromLot lot
        "id" -> I.FromInd (fromJust tokenK) lot)
        <$> decoder
        <*> decoder
        <*> decoder

--------------------------------------------------------------------------------
instance Decodable (I.MerchK 'I.Lot3) where
    decoder :: D.Row (I.MerchK 'I.Lot3)
    decoder = I.MerchLotK
        <$> decoder
        <*> decoder
instance Decodable (I.MerchK 'I.Ind3) where
    decoder :: D.Row (I.MerchK 'I.Ind3)
    decoder = I.MerchIndK
        <$> decoder
instance Decodable (Maybe (I.MerchK 'I.Ind3)) where
    decoder :: D.Row (Maybe (I.MerchK 'I.Ind3))
    decoder = (fmap I.MerchIndK)
        <$> decoder

instance Encodable (I.MerchK 'I.Lot3) where
    encoder :: E.Params (I.MerchK 'I.Lot3)
    encoder = contramap (\(I.MerchLotK item token) -> (item, token)) encoder
--instance Encodable (Maybe (I.MerchK 'I.Lot3)) where
--    encoder :: E.Params (Maybe (I.MerchK 'I.Lot3))
--    encoder = contramap (fmap (\(I.MerchLotK item token) -> (item, token))) encoder
instance Encodable (V.Vector (I.MerchK 'I.Lot3)) where
    encoder :: E.Params (V.Vector (I.MerchK 'I.Lot3))
    encoder = contramap (fmap (\(I.MerchLotK item token) -> (item, token))) encoder
instance Encodable (I.MerchK 'I.Ind3) where
    encoder :: E.Params (I.MerchK 'I.Ind3)
    encoder = contramap (\(I.MerchIndK indk) -> indk) encoder
instance Encodable (V.Vector (I.MerchK 'I.Ind3)) where
    encoder :: E.Params (V.Vector (I.MerchK 'I.Ind3))
    encoder = contramap (fmap (\(I.MerchIndK indk) -> indk)) encoder

instance Decodable I.AnyMerchK where
    decoder :: D.Row I.AnyMerchK
    decoder = (\(tag :: T.Text) indK i t -> case tag of
        "lot" -> I.AnyMerchK (I.MerchLotK (fromJust i) (fromJust t))
        "ind" -> I.AnyMerchK (I.MerchIndK (fromJust indK)))
        <$> decoder
        <*> decoder
        <*> decoder
        <*> decoder
instance Encodable I.AnyMerchK where
    encoder :: E.Params I.AnyMerchK
    encoder = contramap anyMerchK2tp encoder
instance Encodable (V.Vector I.AnyMerchK) where
    encoder :: E.Params (V.Vector I.AnyMerchK)
    encoder = contramap (fmap anyMerchK2tp) encoder
anyMerchK2tp :: I.AnyMerchK -> (Maybe (E.Key2 I.Token3), Maybe (E.Key2 I.Item), Maybe (E.Key2 I.Token))
anyMerchK2tp (I.AnyMerchK (I.MerchLotK itemK tokenK)) = (Nothing, Just itemK, Just tokenK)
anyMerchK2tp (I.AnyMerchK (I.MerchIndK indK)) = (Just indK, Nothing, Nothing)

--------------------------------------------------------------------------------
instance Decodable (I.StockK 'I.Lot3) where
    decoder :: D.Row (I.StockK 'I.Lot3)
    decoder = I.StockLotK
        <$> decoder
        <*> decoder
instance Decodable (I.StockK 'I.Ind3) where
    decoder :: D.Row (I.StockK 'I.Ind3)
    decoder = I.StockIndK
        <$> decoder

instance Encodable (I.StockK 'I.Lot3) where
    encoder :: E.Params (I.StockK 'I.Lot3)
    encoder = contramap (\(I.StockLotK mrchK pl) -> (mrchK, pl)) encoder
instance Encodable (V.Vector (I.StockK 'I.Lot3)) where
    encoder :: E.Params (V.Vector (I.StockK 'I.Lot3))
    encoder = contramap (fmap (\(I.StockLotK mrchK pl) -> (mrchK, pl))) encoder
instance Encodable (I.StockK 'I.Ind3) where
    encoder :: E.Params (I.StockK 'I.Ind3)
    encoder = contramap (\(I.StockIndK indk) -> indk) encoder

--------------------------------------------------------------------------------
--instance Decodable (I.Merch 'I.Lot3) where
--    decoder :: D.Row (I.Merch 'I.Lot3)
--    decoder = I.MerchLot
--        <$> decoder
--        <*> decoder
--        <*> decoder
--        <*> decoder
--instance Decodable (I.Merch 'I.Ind3) where
--    decoder :: D.Row (I.Merch 'I.Ind3)
--    decoder = I.MerchInd
--        <$> decoder
--        <*> decoder

--instance Encodable (I.Merch 'I.Lot3) where
--    encoder :: E.Params (I.Merch 'I.Lot3)
--    encoder = contramap (\(I.MerchLot mrchK qty am rsqty) -> (mrchK, qty, am, rsqty)) encoder
--instance Encodable (I.Merch 'I.Ind3) where
--    encoder :: E.Params (I.Merch 'I.Ind3)
--    encoder = contramap (\(I.MerchInd indk mord) -> (indk, mord)) encoder

--------------------------------------------------------------------------------
instance Decodable (I.Merch2 'I.Lot3) where
    decoder :: D.Row (I.Merch2 'I.Lot3)
    decoder = I.MerchLot2
        <$> decoder
        <*> decoder
instance Encodable (I.Merch2 'I.Lot3) where
    encoder :: E.Params (I.Merch2 'I.Lot3)
    encoder = contramap (\(I.MerchLot2 mrchK qty) -> (mrchK, qty)) encoder
instance Encodable (V.Vector (I.Merch2 'I.Lot3)) where
    encoder :: E.Params (V.Vector (I.Merch2 'I.Lot3))
    encoder = contramap (fmap (\(I.MerchLot2 mrchK qty) -> (mrchK, qty))) encoder

instance Decodable (I.Merch2 'I.Ind3) where
    decoder :: D.Row (I.Merch2 'I.Ind3)
    decoder = I.MerchInd2
        <$> decoder
instance Encodable (I.Merch2 'I.Ind3) where
    encoder :: E.Params (I.Merch2 'I.Ind3)
    encoder = contramap (\(I.MerchInd2 indK) -> indK) encoder
instance Encodable (V.Vector (I.Merch2 'I.Ind3)) where
    encoder :: E.Params (V.Vector (I.Merch2 'I.Ind3))
    encoder = contramap (fmap (\(I.MerchInd2 indK) -> indK)) encoder


instance Decodable I.AnyMerch where
    decoder :: D.Row I.AnyMerch
    decoder = (\(tp :: T.Text) lK q indK -> case tp of
        "lot" -> I.AnyMerch $ I.MerchLot2 lK q
        "ind" -> I.AnyMerch $ I.MerchInd2 $ fromJust indK)
        <$> decoder
        <*> decoder
        <*> decoder
        <*> decoder
--instance Encodable I.AnyMerch where
--    encoder :: E.Params I.AnyMerch
--    encoder = contramap anyMerch2tp encoder
--instance Encodable (V.Vector I.AnyMerch) where
--    encoder :: E.Params (V.Vector I.AnyMerch)
--    encoder = contramap (fmap anyMerch2tp) encoder
anyMerch2tp :: I.AnyMerch -> (Maybe (E.Key2 I.Token3), Maybe (I.MerchK 'I.Lot3), Maybe E.Qty)
anyMerch2tp (I.AnyMerch (I.MerchLot2 lotK qty)) = (Nothing, Just lotK, Just qty)
anyMerch2tp (I.AnyMerch (I.MerchInd2 indK)) = (Just indK, Nothing, Nothing)


freeMerchStmt :: Statement () (V.Vector (I.AnyMerchK, E.Qty, E.Amount, E.Qty, I.Item, Day, T.Text))
freeMerchStmt = Statement query encoder (D.rowVector decoder) True
    where
    query = [r|
        select m.tp, m.indk, m.item, m.token, m.qty, m.amount, m.rsqty
            , m.iname, m.irdnum, m.ispecie, m.icutting, m.iuom, m.iminsztp, m.iminszval, m.imarkup
            , m.tday, m.uname
        from merchandise m
        where m.qty-m.rsqty > 0
        |]

freeMerchStmt2 :: Statement BS.ByteString (V.Vector (I.SalesOffer, E.Qty, I.Item, Day, T.Text))
freeMerchStmt2 = Statement query encoder (D.rowVector decoder) True
    where
    query = [r|
        select m.indk, m.item, m.token, m.qty - m.rsqty, i.price
            , m.incart
            , i.name, i.ordnum, i.specie, i.cutting, i.uom, i.minsztp, i.minszval, i.markup
            , t.day, u.name
        from ( 
                select 'lot' as tp, null as indk, l.item, l.token, l.qty, l.amount, l.rsqty
                    , coalesce (p.qty, 0) as incart
                from merchandise_lot l
                left join (
                    select item, token, qty
                    from pcart_lot
                    where sid=$1 and so is null
                ) p
                on l.item=p.item and l.token=p.token
                where l.qty-l.rsqty > 0
            union all
                select 'ind' as tp, m.id, m.item, m.token, m.qty, m.cost, 0
                    , case coalesce (p.ind, 0) when 0 then 0 else m.qty end 
                from labeled m
                left join (
                    select ind
                    from pcart_ind 
                    where sid=$1 and so is null
                ) p
                on m.id=p.ind
                where (m.rsrid = 0) and (m.place is not null)
        ) m
        inner join item i
        on i.id=m.item
        inner join token t
        on t.id=m.token
        inner join uom u
        on u.id=i.uom
        where i.price > 0
        |]

type StoreData =
    ( I.Specie
    ,   ( (I.Item, E.Key2 I.Item, I.Uom, E.Amount)
        , I.Token
        , E.Key2 I.Token
        , Maybe (E.Key2 I.Token3)
        , E.Qty
        , E.Qty
        )
    )
freeMerchStmt3 :: Statement BS.ByteString (V.Vector StoreData)
freeMerchStmt3 = Statement query encoder (D.rowVector decoder) True
    where
    query = [r|
        select s.name, s.ordnum, s.uom, s.price
            , i.name, i.ordnum, i.specie, i.cutting, i.uom, i.minsztp, i.minszval, i.markup
            , m.item
            , u.name, u.ordnum
            , i.price
            , t.code, t.day, t.note
            , m.token
            , m.indk
            , m.qty - m.rsqty
            , m.incart
        from ( 
                select 'lot' as tp, null as indk, l.item, l.token, l.qty, l.amount, l.rsqty
                    , coalesce (p.qty, 0) as incart
                from merchandise_lot l
                left join (
                    select item, token, qty
                    from pcart_lot
                    where sid=$1 and so is null
                ) p
                on l.item=p.item and l.token=p.token
                where l.qty-l.rsqty > 0
            union all
                select 'ind' as tp, m.id, m.item, m.token, m.qty, m.cost, 0
                    , case coalesce (p.ind, 0) when 0 then 0 else m.qty end 
                from labeled m
                left join (
                    select ind
                    from pcart_ind 
                    where sid=$1 and so is null
                ) p
                on m.id=p.ind
                where (m.rsrid = 0) and (m.place is not null)
        ) m
        inner join item i
        on i.id=m.item
        inner join token t
        on t.id=m.token
        inner join uom u
        on u.id=i.uom
        inner join specie s
        on s.id = i.specie
        where i.price > 0 and (i.minsztp <> 'vs' or m.indk is not null)
        order by s.ordnum, s.id, i.ordnum, i.id, t.day
        |]

--                select 'ind' as tp, m.id, m.item, m.token, m.qty, m.amount, 0
--                    , case coalesce (p.ind, 0) when 0 then 0 else m.qty end 
--                from markitem m
--                left join (
--                    select ind
--                    from pcart_ind 
--                    where sid=$1 and so is null
--                ) p
--                on m.id=p.ind
--                where (m.reserved2) = 0 and (not m.isused)

calcPrices :: Connection -> V.Vector (I.Merch2 'I.Lot3) -> IO (V.Vector (E.Amount))
calcPrices conn lots = do
    eiResult <- run (Trans.transaction Trans.ReadCommitted Trans.Write dbTransaction) conn
    case eiResult of
        Left err -> fail $ show err
        Right result -> return result
    where
    dbTransaction = do
        let priceStep = 10000
            lotKs = V.map (\(I.MerchLot2 lot qty) -> lot) lots
            itemKs = V.map (\(I.MerchLotK itemK _) -> itemK) lotKs
        qtysamts <- Trans.statement lotKs getQtysAmts
        markups <- Trans.statement itemKs getMarkups
        return $ V.map (\((qty, amt), markup) -> E.Amount $ priceStep * ((fromIntegral amt) * (fromIntegral markup) `div` ((fromIntegral qty) * priceStep))) $ V.zip qtysamts markups

    getQtysAmts :: Statement (V.Vector (I.MerchK 'I.Lot3)) (V.Vector (E.Qty, E.Amount))
    getQtysAmts = Statement query encoder (D.rowVector decoder) True
        where
        query = [r|
            select m.qty, m.amount
            from merchandise m
            inner join unnest ($1, $2) as d(item, token)
            on m.item=d.item and m.token=d.token
            |]

    getMarkups :: Statement (V.Vector (E.Key2 I.Item)) (V.Vector Int16)
    getMarkups = Statement query encoder (D.rowVector decoder) True
        where
        query = [r|
            select i.markup
            from item i
            inner join unnest ($1) as d(item)
            on i.id=d.item
            |]


--------------------------------------------------------------------------------
instance Decodable (I.Stock3 'I.Lot3) where
    decoder :: D.Row (I.Stock3 'I.Lot3)
    decoder = I.StockLot
        <$> decoder
        <*> decoder
        <*> decoder
instance Decodable (I.Stock3 'I.Ind3) where
    decoder :: D.Row (I.Stock3 'I.Ind3)
    decoder = I.StockInd
        <$> decoder

instance Encodable (I.Stock3 'I.Lot3) where
    encoder :: E.Params (I.Stock3 'I.Lot3)
    encoder = contramap (\(I.StockLot stK qty am) -> (stK, qty, am)) encoder
instance Encodable (V.Vector (I.Stock3 'I.Lot3)) where
    encoder :: E.Params (V.Vector (I.Stock3 'I.Lot3))
    encoder = contramap (fmap (\(I.StockLot stK qty am) -> (stK, qty, am))) encoder
--instance Encodable (I.Stock3 'I.Ind3) where
--    encoder :: E.Params (I.Stock3 'I.Ind3)
--    encoder = contramap (\(I.StockInd indk) -> indk) encoder

--------------------------------------------------------------------------------
instance Decodable I.Token3 where
    decoder :: D.Row I.Token3
    decoder = I.Token3
        <$> decoder
        <*> decoder

instance Encodable I.Token3 where
    encoder :: E.Params I.Token3
    encoder = contramap (\(I.Token3 l st) -> (l, st)) encoder

--calcRevenuesTrx :: V.Vector I.AnyMerchK -> Trans.Transaction (V.Vector E.Amount)
--calcRevenuesTrx keys = do
--    let priceStep = 10000;
--    costMarkups <- Trans.statement keys getMerchsCostStmt
--    return $ V.map (\(qty, amount, markup) -> round (priceStep * round((fromIntegral amount) /(fromIntegral qty) * (fromIntegral markup) / priceStep) * (fromIntegral qty) / 100)) costMarkups 

--    where
--    getMerchsCostStmt :: Statement (V.Vector I.AnyMerchK) (V.Vector (E.Qty, E.Amount, Int16))
--    getMerchsCostStmt = Statement query encoder (D.rowVector decoder) True
--        where
--        query = [r|
--            select m.qty, m.amount, m.imarkup
--            from merchandize m
--            inner join (
--                select unnest($1), unnest($2), unnest($3)
--            ) d(indk, itemk, tokenk)
--            where (m.indk=d.indk) or (m.item=d.itemk and m.token=d.tokenk)
--            |]


sepAnyMerch :: I.AnyMerch -> (Maybe (I.Merch2 'I.Lot3), Maybe (I.Merch2 'I.Ind3))
sepAnyMerch (I.AnyMerch (I.MerchLot2 k q)) = (Just (I.MerchLot2 k q), Nothing)
sepAnyMerch (I.AnyMerch (I.MerchInd2 k)) = (Nothing, Just (I.MerchInd2 k))

removeCartRowTrx :: BS.ByteString -> I.AnyMerchK -> Trans.Transaction ()
removeCartRowTrx sid key =
    case key of
        I.AnyMerchK (I.MerchIndK k) -> Trans.statement (sid, (I.MerchIndK k)) cleanCartInd
        I.AnyMerchK (I.MerchLotK i t) -> Trans.statement (sid, (I.MerchLotK i t)) cleanCartLot

    where
    cleanCartLot :: Statement (BS.ByteString, I.MerchK 'I.Lot3)  ()
    cleanCartLot = Statement query encoder D.unit True
        where
        query = [r|
            delete from pcart_lot where sid = $1 and item=$2 and token=$3 and so is null
            |]
    cleanCartInd :: Statement (BS.ByteString, I.MerchK 'I.Ind3) ()
    cleanCartInd = Statement query encoder D.unit True
        where
        query = [r|
            delete from pcart_ind where sid = $1 and ind=$2 and so is null
            |]


removeCartRow :: Connection -> CE.Session -> I.AnyMerchK -> IO ()
removeCartRow conn sess key = do
    eiResult <- run (Trans.transaction Trans.ReadCommitted Trans.Write dbTransaction) conn
    case eiResult of
        Left err -> fail $ show err
        Right result -> return result
    where
    CE.Session{..} = sess

    dbTransaction = removeCartRowTrx sid key


savePcart :: Connection -> CE.Session -> I.AnyMerch -> IO ()
savePcart conn sess pCart = do
    eiResult <- run (Trans.transaction Trans.ReadCommitted Trans.Write dbTransaction) conn
    case eiResult of
        Left err -> fail $ show err
        Right result -> return result
    where
    CE.Session{..} = sess


    dbTransaction = do
        let (mLot, mInd) = sepAnyMerch pCart
        when (isJust mLot) $ do
            removeCartRowTrx sid $ I.AnyMerchK $ (\(I.MerchLot2 k _) -> k) $ fromJust mLot
            -- shall remove these 777. Shall save null as amount instead
            V.mapM_ (\r -> Trans.statement (sid, r, 777) saveRowLot) $ V.singleton $ fromJust mLot
        when (isJust mInd) $
            V.mapM_ (\r -> Trans.statement (sid, r, 777) saveRowInd) $ V.singleton $ fromJust mInd
        --mExt <- Trans.statement pCart getMerchsExtStmt
        --Trans.statement sid cleanPcartLot
        --Trans.statement sid cleanPcartInd
        --let (lots, inds) = sepPcart $ V.singleton pCart
--        prices <- calcRevenuesTrx $ V.map I.extrMerchK pCart
        --let    lots = V.map (\((_, lot), rvn) -> (lot, rvn)) $ V.filter (\((i, _), _) -> isNothing i) mExtWrvn
        --inds = V.map (\((Just i, _), rvn) -> (i, rvn)) $ V.filter (\((i, _), _) -> isJust i) mExtWrvn
        --let withRev = V.zip pCart prices
        --Trans.statement (sid, withRev) saveRowLot2

        --V.mapM_ (\r -> Trans.statement (sid, r, 777) saveRowLot) lots 
        --V.mapM_ (\r -> Trans.statement (sid, r, 888) saveRowInd) inds
        return ()

    sepPcart :: V.Vector I.AnyMerch -> (V.Vector (I.Merch2 'I.Lot3), V.Vector (I.Merch2 'I.Ind3))
    sepPcart ams = (\(lots, inds) -> (V.mapMaybe id lots, V.mapMaybe id inds)) $ V.unzip $ V.map sepAnyMerch ams

--    saveRowLot2 :: Statement (BS.ByteString, V.Vector(I.AnyMerch, E.Amount)) ()
--    saveRowLot2 = Statement query encoder D.unit True
--        where
--        query = [r|
--            insert into pcart_lot (sid, item, token, qty, amount)
--            select $1, unnest($2), unnest($3), unnest($4), unnest($5)
--            where $1 is null
--            |]
--            --inner join unnest($2, $3, $4, $5) d(itemK, tokenK, qty, revenue)
--    saveRowInd2 :: Statement (BS.ByteString, (I.Merch2 'I.Ind3, E.Amount)) ()
--    saveRowInd2 = Statement query encoder D.unit True
--        where
--        query = [r|
--            insert into pcart_ind (sid, ind, amount)
--            values ($1, $2, $3)
--            |]

    saveRowLot :: Statement (BS.ByteString, I.Merch2 'I.Lot3, E.Amount) ()
    saveRowLot = Statement query encoder D.unit True
        where
        query = [r|
            insert into pcart_lot (sid, item, token, qty, amount)
            values ($1, $2, $3, $4, $5)
            |]
    saveRowInd :: Statement (BS.ByteString, I.Merch2 'I.Ind3, E.Amount) ()
    saveRowInd = Statement query encoder D.unit True
        where
        query = [r|
            insert into pcart_ind (sid, ind, amount)
            values ($1, $2, $3)
            |]


--instance Decodable I.OrderRow where
--    decoder :: D.Row I.OrderRow
--    decoder = I.OrderRow
--        <$> decoder
--        <*> decoder
--        <*> decoder
instance Decodable I.OrderRow2 where
    decoder :: D.Row I.OrderRow2
    decoder = I.OrderRow2
        <$> decoder
        <*> decoder
        <*> decoder
        <*> decoder


pcartStmt :: Statement BS.ByteString (V.Vector (I.OrderRow2, I.Item, T.Text, E.Amount))
pcartStmt = Statement query encoder (D.rowVector decoder) True
        where
        query = [r|
            select p.indk, p.item, p.token, p.qty, p.amount, p.instr
                , i.name, i.ordnum, i.specie, i.cutting, i.uom, i.minsztp, i.minszval, i.markup
                , u.name, i.price
            from pcart p
            inner join item i
            on i.id=p.item
            inner join uom u
            on u.id = i.uom
            where p.sid=$1 and p.so is null
            |]

ordRowsStmt :: Statement (E.Key2 OE.OrderHdr) (V.Vector (I.OrderRow2, I.Item, T.Text))
ordRowsStmt = Statement query encoder (D.rowVector decoder) True
        where
        query = [r|
            select p.indk, p.item, p.token, p.qty, p.amount, p.instr
                , i.name, i.ordnum, i.specie, i.cutting, i.uom, i.minsztp, i.minszval, i.markup
                , u.name
            from pcart p
            inner join item i
            on i.id=p.item
            inner join uom u
            on u.id = i.uom
            where p.so=$1
            |]


dispatchOrderTrx :: (E.Key OE.OrderHdr) -> Trans.Transaction ()
dispatchOrderTrx hdrK = do
    return ()

cancelOrderTrx :: (E.Key OE.OrderHdr) -> Trans.Transaction ()
cancelOrderTrx hdrK = do
    return ()

-------------------------------------------------------------------------
instance Encodable (I.WriteOff 'I.Lot3) where
    encoder :: E.Params (I.WriteOff 'I.Lot3)
    encoder = contramap (\(I.WriteOffLot s q d n) -> (s, q, d, n)) encoder
instance Encodable (I.WriteOff 'I.Ind3) where
    encoder :: E.Params (I.WriteOff 'I.Ind3)
    encoder = contramap (\(I.WriteOffInd l d n) -> (l, d, n)) encoder

lotWrOff :: Connection -> I.WriteOff 'I.Lot3 -> IO (E.Key2 (I.WriteOff 'I.Lot3))
lotWrOff conn wroffDoc = do
    eiResult <- run (Trans.transaction Trans.ReadCommitted Trans.Write dbTransaction) conn
    case eiResult of
        Left err -> fail $ show err
        Right result -> return result
    where
    --CE.Session{..} = sess
    dbTransaction = do
        let (I.WriteOffLot stockK qtyD _ _) = wroffDoc 
        stock <- Trans.statement stockK getStockLot
        let (I.StockLot _ qtyB amB) = stock
            amD = (fromIntegral amB) * (fromIntegral qtyD) `div` (fromIntegral qtyB)
        Trans.statement (stockK, (-1)*qtyD, (-1)*amD) updBalanceStmt
        let (I.StockLotK merchK _) = stockK
        Trans.statement (merchK, (-1)*qtyD, (-1)*amD, 0) updMerchLotStmt

        docK <-Trans.statement (wroffDoc, amD) saveDoc
        trxK <- Trans.statement "wo" crtTrxTagStmt
        Trans.statement (trxK, docK, qtyD, amD) saveDocTrx
        return docK
        

    saveDoc:: Statement (I.WriteOff 'I.Lot3, E.Amount) (E.Key2 (I.WriteOff 'I.Lot3))
    saveDoc= Statement query encoder (D.singleRow decoder) True
            where
            query = [r|
                insert into wroff_lot (item, token, place, qty, day, note, amount)
                values ($1, $2, $3, $4, $5, $6, $7)
                returning id
                |]
    saveDocTrx :: Statement (Int64, E.Key2 (I.WriteOff 'I.Lot3), E.Qty, E.Amount) ()
    saveDocTrx = Statement query encoder D.unit True
            where
            query = [r|
                insert into wroff_lot_trx (trxkey, wroff, qty, amount)
                values ($1, $2, $3, $4)
                |]

labelWrOff :: Connection -> E.Key2 I.Token3 -> IO ()
labelWrOff conn lblK = do
    currTime <- liftIO getCurrentTime --temporary. for saving in label creation doc
    eiResult <- run (Trans.transaction Trans.ReadCommitted Trans.Write $ dbTransaction currTime) conn
    case eiResult of
        Left err -> fail $ show err
        Right result -> return result
    where
    --CE.Session{..} = sess
    dbTransaction currTime = do
        labeledDb <- Trans.statement lblK getLabeledByK
        let (labeled, _, (lastDoc, rsrK)) = labeledDb
        if rsrK /= 0 then
            Trans.condemn
        else do
            trxK <- Trans.statement "wo" crtTrxTagStmt
            let wroffDoc = I.WriteOffInd lblK (utctDay currTime) "" 
            wroffK <- Trans.statement (wroffDoc, lastDoc, trxK) saveMvmt 
            Trans.statement wroffK updLabeled


    saveMvmt :: Statement (I.WriteOff 'I.Ind3, Int64, Int64) (E.Key2 (I.WriteOff 'I.Ind3))
    saveMvmt = Statement query encoder (D.singleRow decoder) True
            where
            query = [r|
                insert into labeled_mvmt (lblid, day, note, prevdoc, trxid, tp, place)
                values ($1, $2, $3, $4, $5, 'wf', null)
                returning id
                |]

    -- This statement can easily be generalized to any movement
    updLabeled :: Statement (E.Key2 (I.WriteOff 'I.Ind3)) ()
    updLabeled = Statement query encoder D.unit True
            where
            query = [r|
                update labeled l set place=null, lastdoc=$1
                from labeled_mvmt m
                where l.id=m.lblid and m.id=$1
                |]

-- this statement throws an error when the place is null (Labeled inv. is written off
getLabeledByK :: Statement (E.Key I.Token3) (E.FromDb I.Token3)
getLabeledByK = Statement query encoder (D.singleRow decoder) True
        where
        query = [r|
            select l.label, l.item, l.token, l.place, l.qty, l.cost
                        , l.id
                        , l.lastdoc, l.rsrid
            from labeled l
            where l.id=$1
            |]

labeledSaleTrx :: Int64 -> E.Key2 I.Token3 -> Trans.Transaction ()
labeledSaleTrx trxK labeledK = do
    labeledDb <- Trans.statement labeledK getLabeledByK
    let (labeled, _, (lastDoc, rsrK)) = labeledDb
    if rsrK == 0 then
        Trans.condemn
    else do
        let day = ModifiedJulianDay 30000
        docK <- Trans.statement (labeledK, day, lastDoc, trxK) saveMvmt 
        Trans.statement docK updLabeled

    where
    saveMvmt :: Statement (E.Key2 I.Token3, Day, Int64, Int64) (Int64)
    saveMvmt = Statement query encoder (D.singleRow decoder) True
            where
            query = [r|
                insert into labeled_mvmt (lblid, day, prevdoc, trxid, tp, place)
                values ($1, $2, $3, $4, 'sl', null)
                returning id
                |]
    -- Can and shall be generalized (look previous function)
    updLabeled :: Statement Int64 ()
    updLabeled = Statement query encoder D.unit True
            where
            query = [r|
                update labeled l set place=null, lastdoc=$1
                from labeled_mvmt m
                where l.id=m.lblid and m.id=$1
                |]


labeledBackTrx :: Int64 -> E.Key2 I.Token3 -> Trans.Transaction ()
labeledBackTrx trxK labeledK = do
    -- the following line throw errow when place is null. Temporary replaced by getlastDoc
    --labeledDb <- Trans.statement labeledK getLabeledByK
    --let (_, _, (lastDoc, _)) = labeledDb
    lastDoc <- Trans.statement labeledK getLastDoc
    mPrevDoc <- Trans.statement lastDoc getPrevDoc
    if isNothing mPrevDoc then
        Trans.condemn
    else do
        let prevDoc = fromJust mPrevDoc
        Trans.statement (lastDoc, trxK) saveBack
        Trans.statement prevDoc updLabeled

    where
    getLastDoc :: Statement (E.Key2 I.Token3) Int64
    getLastDoc = Statement query encoder (D.singleRow decoder) True
            where
            query = [r|
                select l.lastdoc from labeled l where l.id=$1
                |]
    getPrevDoc :: Statement Int64 (Maybe Int64)
    getPrevDoc = Statement query encoder (D.singleRow decoder) True
            where
            query = [r|
                select m.prevdoc
                from labeled_mvmt m
                where m.id=$1
                |]

    saveBack :: Statement (Int64, Int64) ()
    saveBack = Statement query encoder D.unit True
            where
            query = [r|
                insert into labeled_mvmt_back (docid, trxid)
                values ($1, $2)
                |]

    -- Can and shall be generalized (look at previous function)
    updLabeled :: Statement Int64 ()
    updLabeled = Statement query encoder D.unit True
            where
            query = [r|
                update labeled l set place=m.place, lastdoc=m.id
                from labeled_mvmt m
                where l.id=m.lblid and m.id=$1
                |]

