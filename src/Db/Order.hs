{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE GADTs #-}

{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE KindSignatures #-}


module Db.Order where

import Text.RawString.QQ (r)
--import Data.Maybe (fromJust)
import GHC.Int(Int64)
import Data.Maybe(fromJust, fromMaybe)
--import Data.Time.Calendar (Day)
import Data.Time (Day, LocalTime)
import Data.Time.Clock (UTCTime(utctDay), getCurrentTime)
import qualified Data.Text as T
import qualified Data.ByteString as BS
import qualified Data.Vector as V
import Data.Functor.Contravariant (contramap)
import Control.Monad (when)
import Control.Monad.IO.Class (liftIO)

import Hasql.Connection (Connection)
import Hasql.Statement (Statement(..))
import qualified Hasql.Decoders as D
import qualified Hasql.Encoders as E
import Hasql.Session (run, statement, Session)
import qualified Hasql.Transaction.Sessions as Trans
import qualified Hasql.Transaction as Trans


import qualified Authentication as A
import Db.Authentication()
import Db(Encodable(..), Decodable(..), execStmt, crtTrxTagStmt)
import Entities.Common (Key, FromDb, Key2(Key2), DbEntity, Amount, prettyPrice, Qty)
import qualified Entities.Inventory as I
import Db.Inventory (sepAnyMerch, labeledSaleTrx, labeledBackTrx)
import qualified Entities.Order as O

import qualified Entities.Client as CE

----------------------------------------------------------------
instance Encodable O.Courrier where
    encoder :: E.Params O.Courrier
    encoder = contramap (\(O.Courrier n o t) -> (n, o, t)) encoder
instance Encodable (V.Vector O.Courrier) where
    encoder :: E.Params (V.Vector O.Courrier)
    encoder = contramap (fmap (\(O.Courrier n o t) -> (n, o, t))) encoder
instance Decodable O.Courrier where
    decoder :: D.Row O.Courrier
    decoder = O.Courrier
        <$> decoder
        <*> decoder
        <*> decoder

saveCourriers :: Statement (V.Vector O.Courrier) (V.Vector (Key O.Courrier))
saveCourriers = Statement query encoder (D.rowVector decoder) True
    where
    query = [r|
        insert into courrier (name, ordnum, phone)
        select unnest($1), unnest($2), unnest($3)
        returning id
        |]

saveCourrier :: Statement O.Courrier (Key O.Courrier)
saveCourrier = Statement query encoder (D.singleRow decoder) True
    where
    query = [r|
        insert into courrier (name, ordnum, phone)
        values ($1, $2, $3)
        returning id
        |]

getCourrierById :: Statement (Key O.Courrier) (FromDb O.Courrier)
getCourrierById = Statement query encoder (D.singleRow decoder) True
    where
    query = [r|
        select name, ordnum, phone from courrier where id=$1
        |]

getCourriers :: Statement (Bool) (V.Vector (FromDb O.Courrier))
getCourriers = Statement query encoder (D.rowVector decoder) True
    where
    query = [r|
        select name, ordnum, phone, id, savedat, isclosed
        from courrier
        where ($1 or (not isclosed))
        order by ordnum asc
        |]
----------------------------------------------------------------------------
instance Encodable O.Status where
    encoder :: E.Params O.Status
    encoder = contramap stt2txt encoder
        where
        stt2txt :: O.Status -> T.Text
        stt2txt O.Created     = "cr"
        stt2txt O.Paid        = "pd"
        stt2txt O.Dispatched  = "dp"
        stt2txt O.Delivered   = "dl"
        stt2txt O.Cancelled   = "cl"

instance Decodable O.Status where
    decoder :: D.Row O.Status
    decoder = txt2stt <$> decoder
        where
        txt2stt :: T.Text -> O.Status
        txt2stt "cr" = O.Created
        txt2stt "pd" = O.Paid
        txt2stt "dp" = O.Dispatched
        txt2stt "dl" = O.Delivered
        txt2stt "cl" = O.Cancelled
    

----------------------------------------------------------------------------
instance Encodable (O.Order 'O.Paid) where
    encoder :: E.Params (O.Order 'O.Paid)
    encoder = contramap (\(O.OrPayment p) -> p) encoder

instance Decodable (O.Order 'O.Paid) where
    decoder :: D.Row (O.Order 'O.Paid)
    decoder = O.OrPayment
        <$> decoder

----------------------------------------------------------------------------
instance Encodable O.OKey where
    encoder :: E.Params O.OKey
    encoder = contramap (\(O.OKey (Key2 k)) -> k) encoder

instance Decodable O.OKey where
    decoder :: D.Row O.OKey
    decoder = O.OKey
        <$> decoder

----------------------------------------------------------------------------
instance Encodable O.DspHdr where
    encoder :: E.Params O.DspHdr
    encoder = contramap (\(O.DspHdr c n) -> (c, n)) encoder
instance Decodable O.DspHdr where
    decoder :: D.Row O.DspHdr
    decoder = O.DspHdr
        <$> decoder
        <*> decoder

saveDspHdr :: Statement O.DspHdr (Key2 O.DspHdr)
saveDspHdr = Statement query encoder (D.singleRow decoder) True
    where
    query = [r|
        insert into dsp_header (courrier, note)
        values ($1, $2)
        returning id
        |]

dspHdrsByDates :: Statement (Day, Day) (V.Vector (DbEntity O.DspHdr))
dspHdrsByDates = Statement query encoder (D.rowVector decoder) True
    where
    query = [r|
        select d.courrier, d.note, id, d.amount, d.ordsqty
        from dsp_header d
        |]

----------------------------------------------------------------------------
instance Encodable O.DspRow where
    encoder :: E.Params O.DspRow
    encoder = contramap (\(O.DspRow k h) -> (k, h)) encoder
instance Decodable O.DspRow where
    decoder :: D.Row O.DspRow
    decoder = O.DspRow
        <$> decoder
        <*> decoder

saveDspRow :: Statement (Key2 O.DspHdr, Key2 (O.Order 'O.Dispatched)) ()
saveDspRow = Statement query encoder D.unit True
    where
    query = [r|
        insert into dsp_row (hdr, so)
        values ($1, $2)
        |]

dspRowsByHdr :: Statement (Key2 O.DspHdr) (V.Vector O.DspRow)
dspRowsByHdr = Statement query encoder (D.rowVector decoder) True
    where
    query = [r|
        select d.courrier, d.note, id, d.amount, d.ordsqty
        from dsp_row r
        where hdr = $1
        |]

----------------------------------------------------------------------------
instance Encodable O.OrderHdr where
    encoder :: E.Params O.OrderHdr
    encoder = contramap (\(O.OrderHdr p d n a dlv t) -> (p, d, n, a, dlv, t)) encoder
instance Decodable O.OrderHdr where
    decoder :: D.Row O.OrderHdr
    decoder = O.OrderHdr
        <$> decoder
        <*> decoder
        <*> decoder
        <*> decoder
        <*> decoder
        <*> decoder

unpaidOrders :: Statement () (V.Vector (Key2 (O.Order 'O.Created), UTCTime))
unpaidOrders = Statement query encoder (D.rowVector decoder) True
    where
    query = [r|
        select s.id, t.savedat
        from so_header s
        inner join so_hist h
        on s.id = h.sohdr and s.curid = h.id
        inner join trxtag t
        on h.trxtag = t.id
        where h.st = 'cr' 
        |]


saveOrderHdr :: Statement (O.OrderHdr, Int64) (Key2 (O.Order 'O.Created))
saveOrderHdr = Statement query encoder (D.singleRow decoder) True
    where
    query = [r|
        with h as (
            insert into so_hist (st, trxtag)
            values ('cr', $7)
            returning id
        ) 
        insert into so_header (phone, dlvday, name, address, dlvcost, amount, id, curid)
        select $1, $2, $3, $4, $5, $6, h.id, h.id
        from h
        returning id
        |]

setOrderStatus :: Statement (Key2 (O.Order a)) ()
setOrderStatus = Statement query encoder D.unit True
    where
    query = [r|
        update so_header s set curid=h.id, curst=h.st
        from so_hist h
        where h.id=$1 and h.sohdr=s.id
        |]


orderHdrByK :: Statement (Key2 O.OrderHdr) (FromDb O.OrderHdr)
orderHdrByK = Statement query encoder (D.singleRow decoder) True
    where
    query = [r|
        select phone, dlvday, name, address, dlvcost, amount, id, curst, curid
        from so_header h
        where h.id=$1
        |]

--ordersByStatus :: Statement O.Status (V.Vector (O.OrderHdr, Key O.Order)
ordersByStatus :: Statement O.Status (V.Vector (FromDb O.OrderHdr))
ordersByStatus = Statement query encoder (D.rowVector decoder) True
    where
    query = [r|
        select phone, dlvday, name, address, dlvcost, amount, id, curst, curid
        from so_header h
        where h.curst=$1
        |]

ordersByPhone :: Statement (T.Text) (V.Vector (FromDb O.OrderHdr))
ordersByPhone = Statement query encoder (D.rowVector decoder) True
    where
    query = [r|
        select s.phone, s.dlvday, s.name, s.address, s.dlvcost, s.amount ,s.id, s.curst, s.curid
        from so_header s
        inner join so_hist h
        on s.id=h.id
        inner join trxtag t
        on t.id=h.trxtag
        where h.st = 'cr' and s.phone like ('%' || $1 || '%')
        order by t.savedat desc
        |]

ords2Pay :: Statement () (V.Vector (FromDb O.OrderHdr))
ords2Pay = Statement query encoder (D.rowVector decoder) True
    where
    query = [r|
        select s.phone, s.dlvday, s.name, s.address, s.dlvcost, s.amount, s.id, s.curst, s.curid
        from so_header s
        where s.curst = 'cr'
        |]

ords2Dispatch :: Statement () (V.Vector (FromDb O.OrderHdr))
ords2Dispatch = Statement query encoder (D.rowVector decoder) True
    where
    queryOld = [r|
        select s.phone, s.dlvday, s.name, s.address, s.dlvcost, s.amount, s.id, s.curst, s.curid
        from so_header s
        where s.curst = 'pd'
        |]
    query = [r|
        select s.phone, s.dlvday, s.name, s.address, s.dlvcost, s.amount, s.id, s.curst, s.curid
        from so_header s
        left join (
            select so 
            from pcart_lot
            where qty-prpqty <> 0
            group by so
        ) r
        on s.id=r.so
        where s.curst = 'pd' and r.so is null
        |]

ords2Deliver :: Statement () (V.Vector (FromDb O.OrderHdr, T.Text))
ords2Deliver = Statement query encoder (D.rowVector decoder) True
    where
    query = [r|
        select s.phone, s.dlvday, s.name, s.address, s.dlvcost, s.amount, s.id, s.curst, s.curid, c.name
        from so_header s
        inner join dsp_row r
        on s.curid = r.so
        inner join dsp_header h
        on h.id = r.hdr
        inner join courrier c
        on c.id = h.courrier
        where s.curst = 'dp'
        |]

ordHistory :: Statement (Key2 O.OrderHdr) (V.Vector (O.OKey, UTCTime, O.Status))
ordHistory = Statement query encoder (D.rowVector decoder) True
    where
    query = [r|
        with recursive r(id, st) as (
                select s.curid, s.curst
                from so_header s
                where s.id=$1
            union all
                select h.fromid, h.fromst
                from so_hist h
                inner join r
                on r.id=h.id and r.st=h.st
        )
        select r.id, t.savedat, r.st
        from r
        inner join so_hist h
        on r.id = h.id and r.st=h.st
        inner join trxtag t
        on h.trxtag=t.id
    |]


---------------------------------------------------------------------------------
savePayment :: Connection -> O.Order 'O.Paid -> IO ()
savePayment conn payment = do
    eiResult <- run (Trans.transaction Trans.ReadCommitted Trans.Write dbTransaction) conn
    case eiResult of
        Left err -> fail $ show err
        Right result -> return result
    where
    dbTransaction = do
        trxK <- Trans.statement "pm" crtTrxTagStmt
        -- to add check that I'm updating the last status
        pmtK <- Trans.statement (payment, trxK) savePaymentStmt
        Trans.statement pmtK setOrderStatus



    savePaymentStmt :: Statement (O.Order 'O.Paid, Int64) (Key2 (O.Order 'O.Paid))
    savePaymentStmt = Statement query encoder (D.singleRow decoder) True
        where
        query = [r|
            insert into so_hist (st, sohdr, fromid, fromst, trxtag)
            select 'pd', h.sohdr, h.id, h.st, $2
            from so_hist h
            where h.st='cr' and h.id=$1
            returning id
            |]

---------------------------------------------------------------------------------
saveCancel :: Connection -> Key2 (O.Order 'O.Created) -> IO ()
saveCancel conn ordK = do
    eiResult <- run (Trans.transaction Trans.ReadCommitted Trans.Write dbTransaction) conn
    case eiResult of
        Left err -> fail $ show err
        Right result -> return result
    where
    dbTransaction = do
        trxK <- Trans.statement "pm" crtTrxTagStmt
        cnlK <- Trans.statement (ordK, trxK) saveCancelStmt
        Trans.statement cnlK setOrderStatus
        Trans.statement ordK unReserveLot
        Trans.statement ordK unReserveInd

    saveCancelStmt :: Statement (Key2 (O.Order 'O.Created), Int64) (Key2 (O.Order 'O.Cancelled))
    saveCancelStmt = Statement query encoder (D.singleRow decoder) True
        where
        query = [r|
            insert into so_hist (st, sohdr, fromid, fromst, trxtag)
            select 'cl', h.sohdr, h.id, h.st, $2
            from so_hist h
            where h.st='cr' and h.id=$1
            returning id
            |]

    unReserveLot :: Statement (Key2 (O.Order 'O.Created)) ()
    unReserveLot = Statement query encoder D.unit True
            where
            query = [r|
                update merchandise_lot m set rsqty=m.rsqty-p.qty
                from pcart_lot p
                where p.so=$1 and p.item=m.item and p.token=m.token 
                |]
    unReserveInd :: Statement (Key2 (O.Order 'O.Created)) ()
    unReserveInd = Statement query encoder D.unit True
            where
            query = [r|
                update labeled l set rsrid = 0 
                from pcart_ind p
                where p.so=$1 and p.ind=l.id and l.rsrid > 0
                |]

---------------------------------------------------------------------------------
-- to change input to O.Order 'O.Delivered
saveDelivery :: Connection -> Key2 (O.Order 'O.Dispatched) -> IO ()
saveDelivery conn dspK = do
    eiResult <- run (Trans.transaction Trans.ReadCommitted Trans.Write dbTransaction) conn
    case eiResult of
        Left err -> fail $ show err
        Right result -> return result
    where
    dbTransaction = do
        trxK <- Trans.statement "pm" crtTrxTagStmt
        dlvK <- Trans.statement (dspK, trxK) saveDeliveryStmt
        Trans.statement dlvK setOrderStatus


    saveDeliveryStmt :: Statement (Key2 (O.Order 'O.Dispatched), Int64) (Key2 (O.Order 'O.Paid))
    saveDeliveryStmt = Statement query encoder (D.singleRow decoder) True
        where
        query = [r|
            insert into so_hist (st, sohdr, fromid, fromst, trxtag)
            select 'dl', h.sohdr, h.id, h.st, $2
            from so_hist h
            where h.st='dp' and h.id=$1
            returning id
            |]


---------------------------------------------------------------------------------
paymentsStmt :: Statement () (V.Vector (Key2 (O.Order 'O.Paid), FromDb O.OrderHdr, Maybe Int64))
paymentsStmt = Statement query encoder (D.rowVector decoder) True
    where
    query = [r|
        select h.id, s.phone, s.dlvday, s.name, s.address, s.dlvcost, s.amount, s.id, s.curst, s.curid, b.trxtag
        from so_header s
        inner join so_hist h
        on h.sohdr = s.id
        left join so_back b
        on b.fromid=h.id and b.fromst=h.st and b.sohdr=h.sohdr
        where h.st = 'pd'
        |]


----------------------------------------------------------------------------------------
--saveBackDoc :: Statement (Key2 (O.Order a), Int64) (Key2 (O.Order a))
saveBackDoc :: Statement (O.OKey, Int64) (Key2 (O.Order a))
saveBackDoc = Statement query encoder (D.singleRow decoder) True
    where
    query = [r|
        insert into so_back (sohdr, fromid, fromst, toid, tost, trxtag)
        select h.sohdr, h.id, h.st, h.fromid, h.fromst, $2
        from so_hist h
        where h.id=$1
        returning toid
        |]

----------------------------------------------------------------------------------------
statusBack :: Connection -> Key2 O.OrderHdr -> IO ()
statusBack conn ordK = do
    eiResult <- run (Trans.transaction Trans.ReadCommitted Trans.Write dbTransaction) conn
    case eiResult of
        Left err -> fail $ show err
        Right result -> return result
    where
    dbTransaction = do
        -- to set "pm" to something reasonable
        trxK <- Trans.statement "pm" crtTrxTagStmt
        hdrDb <- Trans.statement ordK orderHdrByK
        let (_, _, (status, docK)) = hdrDb
        backAction trxK status ordK

        prevDoc <- Trans.statement (docK, trxK) saveBackDoc
        Trans.statement prevDoc setOrderStatus
        return ()

    backAction :: Int64 -> O.Status -> Key2 O.OrderHdr -> Trans.Transaction () 
    backAction _ O.Created ordK = do
        Trans.condemn
        fail "Cannot rollback \"Created\" status"

    backAction _ O.Paid key = do
        prpQty <- Trans.statement ordK checkIfEmpty 
        if (fromMaybe 0 prpQty) /= 0 then do
            Trans.condemn
            fail "The order is not empty"
        else
            -- reservation is not cleared here, because it will be made
            -- automatically with order cancelation (by cron)
            return ()
        where
        checkIfEmpty :: Statement (Key2 O.OrderHdr) (Maybe Qty)
        checkIfEmpty = Statement query encoder (D.singleRow decoder) True
            where
            query = [r|
                select sum(p.prpqty)
                from pcart_lot p
                where p.so=$1
                |]

    backAction trxK O.Dispatched ordK = do
        Trans.statement (ordK, "prp") setStatusLotRows
        -- The following may throw an error, if the label is already occupied
        indRows <- Trans.statement ordK getOrderInds
        V.mapM_ (labeledBackTrx trxK) indRows
        
    backAction _ O.Delivered key = return ()
    backAction _ O.Cancelled key = return ()


getOrderInds :: Statement (Key2 O.OrderHdr) (V.Vector (Key2 I.Token3))
getOrderInds = Statement query encoder (D.rowVector decoder) True
    where
    query = [r|
        select p.ind from pcart_ind p where p.so=$1
        |]


----------------------------------------------------------------------------------------
saveDspRowTrx :: Key2 (O.Order 'O.Paid) -> Key2 O.DspHdr -> Int64 -> Trans.Transaction (Key2 (O.Order 'O.Dispatched))
saveDspRowTrx rowK hdrK trxK = do
    soHdrK <- Trans.statement rowK getSoHdrK
    diff <- Trans.statement soHdrK checkLotsPrepared
    if (fromMaybe 0 diff) == 0 then do
        Trans.statement (soHdrK, "dsp") setStatusLotRows

        indRows <- Trans.statement soHdrK getOrderInds
        V.mapM_ (labeledSaleTrx trxK) indRows

        dspK <- Trans.statement (rowK, trxK) saveDspHist
        Trans.statement (hdrK, dspK) saveDspRow
        Trans.statement dspK setOrderStatus
        return dspK
    else do
        Trans.condemn
        fail $ "Not all order rows are prepared"


    where
    saveDspHist :: Statement (Key2 (O.Order 'O.Paid), Int64) (Key2 (O.Order 'O.Dispatched))
    saveDspHist = Statement query encoder (D.singleRow decoder) True
        where
        query = [r|
            insert into so_hist (st, sohdr, fromid, fromst, trxtag)
            select 'dp', h.sohdr, h.id, h.st, $2
            from so_hist h
            where h.st='pd' and h.id=$1
            returning id
            |]
    getSoHdrK :: Statement (Key2 (O.Order 'O.Paid)) (Key2 O.OrderHdr)
    getSoHdrK = Statement query encoder (D.singleRow decoder) True
        where
        query = [r|
            select h.sohdr from so_hist h where h.id=$1
            |]

    checkLotsPrepared :: Statement (Key2 O.OrderHdr) (Maybe Qty) 
    checkLotsPrepared = Statement query encoder (D.singleRow decoder) True
        where
        query = [r|
            select sum(abs(p.qty - p.prpqty))
            from pcart_lot p
            where p.so=$1
            |]

setStatusLotRows :: Statement (Key2 O.OrderHdr, T.Text) ()
setStatusLotRows = Statement query encoder D.unit True
    where
    query = [r|
        update pcart_lot p set status=$2 where p.so=$1
        |]

-- shall be deleted
setIsUsedInds :: Statement (Key2 O.OrderHdr, Bool) ()
setIsUsedInds = Statement query encoder D.unit True
    where
    query = [r|
        update markitem m set isused=$2
        from pcart_ind p
        where p.ind = m.id and p.so=$1
        |]

----------------------------------------------------------------------------------------
instance Decodable (O.OrderRow 'I.Lot3) where
    decoder :: D.Row (O.OrderRow 'I.Lot3)
    decoder = O.OrderRow
        <$> decoder
        <*> decoder
        <*> decoder
        <*> decoder

getOrderRowByK :: Statement (Key2 (O.OrderRow 'I.Lot3)) (O.OrderRow 'I.Lot3, T.Text)
getOrderRowByK = Statement query encoder (D.singleRow decoder) True
    where
    query = [r|
        select p.item, p.token, p.qty, p.amount as ramount, p.instr, p.id, p.status
        from pcart_lot p
        where p.id=$1
        |]

ords2PrpStmt :: Statement () (V.Vector (FromDb O.OrderHdr, O.OrderRow 'I.Lot3, Qty))
ords2PrpStmt = Statement query encoder (D.rowVector decoder) True
    where
    query = [r|
        select h.phone, h.dlvday, h.name, h.address, h.dlvcost, h.amount as tamount, h.id, h.curst, h.curid
            , p.item, p.token, p.qty, p.amount as ramount, p.instr, p.id
            , p.prpqty
        from pcart_lot p
        inner join so_header h
        on h.id=p.so
        where p.status <> 'dsp' and h.curst = 'pd'
        |]





----------------------------------------------------------------------------------------
saveDispatch :: Connection -> (O.DspHdr, V.Vector (Key2 (O.Order 'O.Paid))) -> IO ()
saveDispatch conn (hdr, ordsK) = do
    eiResult <- run (Trans.transaction Trans.ReadCommitted Trans.Write dbTransaction) conn
    case eiResult of
        Left err -> fail $ show err
        Right result -> return result
    where
    dbTransaction = do
        hdrK <- Trans.statement hdr saveDspHdr
        trxK <- Trans.statement "ds" crtTrxTagStmt
        V.mapM_ (\k -> saveDspRowTrx k hdrK trxK) ordsK
        return ()
        -- incomplete. One have to select the warehouses to take inventory from


--    savePayment :: Statement (Key O.OrderHdr, Int64, Int64) ()
--    savePayment = Statement query encoder D.unit True
--        where
--        query = [r|
--            insert into so_payment (so, dir, savedby)
--            values ($1, $2, $3)
--            returning id
--            |]


--getPreOrderRows :: Statement (T.Text) (V.Vector O.PreOrder)
--getPreOrderRows = Statement query encoder (D.rowVector decoder) True
--    where
--    query = [r|
--        select sid, item, qty
--        from preorder_row
--        where sid=$1
--        |]
--saveOrderHdr :: Statement (T.Text, V.Vector (E.Key I.Item, Int64)) ()
--saveOrderHdr = Statement query encoder D.unit True
--    where
--    query = [r|
--        insert into preorder_row (sid, item, qty)
--        select $1, unnest($2), unnest($3)
--        |]
--getPreOrderRows :: Statement (T.Text) (V.Vector O.PreOrder)
--getPreOrderRows = Statement query encoder (D.rowVector decoder) True
--    where
--    query = [r|
--        select sid, item, qty
--        from preorder_row
--        where sid=$1
--        |]

----------------------------------------------------------------------------------------
setSpeciePrice :: Connection -> Key2 I.Specie -> Amount -> IO () 
setSpeciePrice conn specieK price = do
    eiResult <- run (Trans.transaction Trans.ReadCommitted Trans.Write dbTransaction) conn
    case eiResult of
        Left err -> fail $ show err
        Right result -> return result
    where
    dbTransaction = do
        Trans.statement (specieK, price) patchSpecie
        markups <- Trans.statement specieK getItemMkps
        let sprices = V.map (\(iK, mkp) -> (iK, prettyPrice $ (fromIntegral price) * (fromIntegral mkp) `div` 100)) markups
        Trans.statement sprices saveItemPrices


    patchSpecie :: Statement (Key2 I.Specie, Amount) ()
    patchSpecie = Statement query encoder D.unit True
        where
        query = [r|
            update specie set price=$2 where id=$1
            |]

    getItemMkps :: Statement (Key2 I.Specie) (V.Vector (Key2 I.Item, Amount))
    getItemMkps = Statement query encoder (D.rowVector decoder) True
        where
        query = [r|
            select id, markup from item where specie=$1
            |]

    saveItemPrices :: Statement (V.Vector (Key2 I.Item, Amount)) ()
    saveItemPrices = Statement query encoder D.unit True
        where
        query = [r|
            update item i set price=d.price
            from unnest($1, $2) as d(itemk, price)
            where i.id=d.itemk
            |]

saveOrder :: Connection -> CE.Session -> (O.OrderHdr, V.Vector I.OrderRow) -> IO ()
saveOrder conn sess (ohdr, rows) = do
    when (visitor == CE.Anonimous) $ fail "The client is not authenticated"
    let CE.Client{..} = visitor
    when (authst /= CE.Authenticated) $ fail "The phone number is not confirmed"
    --curTime <- liftIO getCurrentTime
    let phoneSess = CE.phone (visitor :: CE.Visitor)
        phoneOhdr = O.phone (ohdr :: O.OrderHdr)
    when (phoneSess /= phoneOhdr) $ fail "Trying to save order for no authenticated phone"
    -- here the order shall checked (prices, total amount etc. Proceed in transaction if success
    -- here I trust the amount recevied from client. I shall check if 
    -- the prices are correct and shall not trust the client
    --dlvPrice <- Trans.statement () getDlvPrice
    --amount = V.sum $ V.map (\(_, am, _) -> am) rows
    -- ....

    eiResult <- run (Trans.transaction Trans.ReadCommitted Trans.Write $ dbTransaction) conn
    case eiResult of
        Left err -> fail $ show err
        Right result -> return result
    where
    CE.Session{..} = sess
    

    dbTransaction = do
        trxK <- Trans.statement "pd" Db.crtTrxTagStmt
        hdrK <- Trans.statement (ohdr, trxK) saveOrderHdr

        let (rowsLot, rowsInd) = (\(lots, inds) -> (V.mapMaybe id lots, V.mapMaybe id inds)) $ V.unzip $ V.map (\(anyMerch, _, _) -> sepAnyMerch anyMerch) rows
        let rowsSep = V.map (\(anyMerch, am, instr) -> (sepAnyMerch anyMerch, am, instr)) rows
            rowsIndExt = V.mapMaybe (\((_, mi), am, instr) -> fmap (\i -> (i, am, instr)) mi) rowsSep
            rowsLotExt = V.mapMaybe (\((ml, _), am, instr) -> fmap (\i -> (i, am, instr)) ml) rowsSep
        Trans.statement (sid, hdrK, rowsIndExt) updPcartInd
        Trans.statement (sid, hdrK, rowsLotExt) updPcartLot

        Trans.statement rowsLot reserveLot 
        inds <- Trans.statement rowsInd reserveInd
        when ((V.length inds) /= (V.length rowsInd)) Trans.condemn
        
        return ()

    updPcartLot :: Statement (BS.ByteString, Key2 (O.Order 'O.Created)
                        , V.Vector (I.Merch2 'I.Lot3, Amount, T.Text)) ()
    updPcartLot = Statement query encoder D.unit True
            where
            query = [r|
                update pcart_lot p set so=$2, amount=d.am, instr=d.instr
                from unnest($3, $4, $6, $7) as d(item, token, am, instr)
                where sid=$1 and so is null and d.item=p.item and d.token=p.token
                |]

    updPcartInd :: Statement (BS.ByteString, Key2 (O.Order 'O.Created)
                        , V.Vector (I.Merch2 'I.Ind3, Amount, T.Text)) ()
    updPcartInd = Statement query encoder D.unit True
            where
            query = [r|
                update pcart_ind p set so=$2, amount=d.am, instr=d.instr
                from unnest($3, $4, $5) as d(indk, am, instr)
                where sid=$1 and so is null and p.ind=d.indk
                |]


    reserveLot :: Statement (V.Vector (I.Merch2 'I.Lot3)) ()
    reserveLot = Statement query encoder D.unit True
            where
            query = [r|
                update merchandise_lot m set rsqty=m.rsqty+r.qty
                from unnest($1, $2, $3) as r(itemk, lotk, qty)
                where m.item=r.itemk and m.token=r.lotk
                |]
    reserveInd :: Statement (V.Vector (I.Merch2 'I.Ind3)) (V.Vector (Key2 I.Token3))
    reserveInd = Statement query encoder (D.rowVector decoder) True
            where
            query = [r|
                update labeled l set rsrid = 1 
                from unnest($1) as r(indk)
                where l.id = r.indk and l.rsrid = 0
                returning l.id
                |]

getPickList :: Statement (Key2 (O.OrderRow 'I.Lot3)) (V.Vector (Key2 I.Place, Qty))
getPickList = Statement query encoder (D.rowVector decoder) True
        where
        query = [r|
            select p.place, sum(p.qty)
            from so_picklist p
            where p.sorow=$1
            group by p.place
            having sum(p.qty) <> 0
            |]


---------------------------------------------------------------------------------
prepareOrdLot :: Connection -> Key2 (O.OrderRow 'I.Lot3) -> V.Vector (Key2 I.Place, Qty) -> IO ()
prepareOrdLot conn rowK picklist = do
    eiResult <- run (Trans.transaction Trans.ReadCommitted Trans.Write dbTransaction) conn
    case eiResult of
        Left err -> fail $ show err
        Right result -> return result
    where
    dbTransaction = do
        ordRowSt <- Trans.statement rowK getOrderRowByK
        let (ordRow, status) = ordRowSt
        if status == "prp" then do
            trxK <- Trans.statement "pm" crtTrxTagStmt
            let plTotal = V.sum $ V.map (\(_, q) -> q) picklist
            Trans.statement (rowK, plTotal) updateSoRow
            Trans.statement (rowK, picklist, trxK) savePickList
            let O.OrderRow merch _ _ _ = ordRow 
                I.MerchLot2 merchK _ = merch
            Trans.statement (merchK, plTotal, 0) updMerchStmt
            let stocks = V.map (\(place, qty) -> I.StockLot (I.StockLotK merchK place) qty 0) picklist
            Trans.statement stocks updStockStmt
        else do
            Trans.condemn
            fail "Trying to prepare dispatched row"


    savePickList :: Statement (Key2 (O.OrderRow 'I.Lot3), V.Vector (Key2 I.Place, Qty), Int64) ()
    savePickList = Statement query encoder D.unit True
        where
        query = [r|
            insert into so_picklist (sorow, place, qty, trx)
            select $1, unnest($2), unnest($3), $4
            |]

    updateSoRow :: Statement (Key2 (O.OrderRow 'I.Lot3), Qty) ()
    updateSoRow = Statement query encoder D.unit True
        where
        query = [r|
            update pcart_lot set prpqty=prpqty+$2 where id=$1
            |]

    updMerchStmt :: Statement (I.MerchK 'I.Lot3, Qty, Amount) ()
    updMerchStmt = Statement query encoder D.unit True
        where
        query = [r|
            update merchandise_lot m set qty=m.qty-$3, rsqty=m.rsqty-$3
            where m.item=$1 and m.token=$2
            |]

    updStockStmt :: Statement (V.Vector (I.Stock3 'I.Lot3)) ()
    updStockStmt = Statement query encoder D.unit True
        where
        query = [r|
            update inventory_balance s set qty=s.qty-d.qty, amount=s.amount-d.amount
            from unnest($1, $2, $3, $4, $5) as d(item, token, place, qty, amount)
            where s.item=d.item and s.token=d.token and s.place=d.place
            |]

saveDlvPrice :: Statement Amount ()
saveDlvPrice = Statement query encoder D.unit True
    where
    query = [r|
        insert into dlvprice (price)
        values ($1)
        |]
getDlvPrice :: Statement () Amount
getDlvPrice = Statement query encoder (D.singleRow decoder) True
    where
    query = [r|
        select price from dlvprice
        order by savedat desc
        limit 1
        |]
histDlvPrice :: Statement () (V.Vector (Amount, UTCTime))
histDlvPrice = Statement query encoder (D.rowVector decoder) True
    where
    query = [r|
        select price, savedat from dlvprice
        order by savedat desc
        |]

getDlvDay :: Statement () Day
getDlvDay = Statement query encoder (D.singleRow decoder) True
    where
    query = [r|
        select day from dlvday
        |]

setDlvDay :: Statement Day ()
setDlvDay = Statement query encoder D.unit True
    where
    query = [r|
        update dlvday set day=$1
        |]

