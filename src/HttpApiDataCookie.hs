module HttpApiDataCookie where

import Web.Cookie (SetCookie, renderSetCookie)
import Web.HttpApiData (ToHttpApiData(..))

import qualified Data.ByteString as BS
import Data.ByteString.Builder (toLazyByteString)
import qualified Data.ByteString.Lazy as LBS

import Data.Text.Encoding (decodeUtf8With)
import Data.Text.Encoding.Error (lenientDecode)

--instance ToHttpApiData SetCookie where
--  toUrlPiece = decodeUtf8With lenientDecode . toHeader
--  toHeader = LBS.toStrict . toLazyByteString . renderSetCookie


