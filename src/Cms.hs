{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE InstanceSigs #-}

module Cms where

import Text.RawString.QQ (r)
import GHC.Generics
import GHC.Int (Int16, Int32)
import Data.Time (UTCTime)
import Hasql.Connection (Connection)
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSL
import qualified Data.Text as T
import qualified Data.Vector as V

import Control.Monad.IO.Class (liftIO)

import Data.Functor.Contravariant (contramap)
import qualified Hasql.Encoders as E
import qualified Hasql.Decoders as D
import Db(Encodable(..), Decodable(..), execStmt)
import Hasql.Statement (Statement(..))
import Entities.Common (tshow)

import Servant.API (ToHttpApiData(toUrlPiece), FromHttpApiData(parseUrlPiece))

data Resource = PrivacyPolicy | TermsOfUse | AboutUs | StepByStep | PaymentData | ReturnPolicy
    deriving (Show, Enum, Bounded)

instance Encodable Resource where
    encoder :: E.Params Resource
    encoder =  contramap tshow (E.param E.text)

instance ToHttpApiData Resource where
    toUrlPiece = tshow

instance FromHttpApiData Resource where
    parseUrlPiece rsrName = maybe
        (Left "Resource name is wrong") Right
        $ lookup rsrName $ map (\a -> (tshow a , a)) $ enumFrom minBound


saveResource :: Statement (Resource, BS.ByteString) ()
saveResource = Statement query encoder D.unit True
    where
    query = [r|
        insert into cmsdata (name, content) values ($1, $2)
        |]

getResource :: Statement Resource BS.ByteString
getResource = Statement query encoder (D.singleRow decoder) True
    where
    query = [r|
        select content
        from cmsdata
        where name=$1
        order by savedat desc
        limit 1 
        |]

