{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE KindSignatures #-}
-- {-# LANGUAGE TypeInType #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE DeriveGeneric #-}

{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ExistentialQuantification #-}

module Authentication where

import GHC.Int(Int64)
import qualified Data.Text as T
import Data.Time.LocalTime(LocalTime)
import Data.Time.Clock (UTCTime(..), secondsToDiffTime, getCurrentTime, addUTCTime, nominalDay)

import GHC.Generics
import Data.Aeson (FromJSON(parseJSON), withObject, (.:), ToJSON)

import Entities.Common (Persistable, Key, Tag)


newtype Phone = Phone T.Text deriving (Generic, Eq)
instance Show Phone where
    show (Phone n) = T.unpack n
instance ToJSON Phone
instance FromJSON Phone



data StaffRole = Admin | Operator deriving (Generic)
instance ToJSON StaffRole
instance FromJSON StaffRole
instance Show StaffRole where
    show Admin = "Administrador"
    show Operator = "Operador"

data ClientAuthStatus = Authenticated | CodeSent UTCTime deriving (Generic)


data Client = Client Phone deriving (Generic)
instance ToJSON Client
data Staff = Staff StaffRole T.Text T.Text T.Text

instance Persistable Staff where
    type Key Staff = Int64
    type Tag Staff = (LocalTime, Bool)

data User
    = UserClient Client
    | UserStaff Staff
instance Show User where
    show (UserClient (Client (Phone phone))) = "Teléfono " ++ (T.unpack phone)
    show (UserStaff (Staff role nick _ _)) = "Usuario " ++ (T.unpack nick)

--data UserTp = Client | Staff
--data User (k :: UserTp) where
--    MkClient :: Phone -> User2 'Client
--    MkStaff  :: StaffRole -> T.Text -> T.Text -> T.Text -> User2 'Staff
--data UserBox where
--    UserBox :: forall (k :: UserTp). User k -> UserBox 

data SessData where
    ClientSess :: Client -> UTCTime -> ClientAuthStatus -> T.Text -> SessData
    StaffSess :: Staff -> Key Staff -> UTCTime -> SessData
getAuthUser :: SessData -> Maybe User
getAuthUser (StaffSess staff _ _) = Just $ UserStaff staff
getAuthUser (ClientSess client _ Authenticated _) = Just $ UserClient client
getAuthUser _ = Nothing

data Credentials
    = ClientCred Phone
    | StaffCred T.Text T.Text
    deriving (Generic)
instance FromJSON Credentials
instance ToJSON Credentials


