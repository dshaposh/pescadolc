{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}

module Db where

import Text.RawString.QQ (r)
import Prelude hiding (unzip, unzip2, unzip3, unzip4, unzip5, unzip6, unzip7, unzip8)
import Hasql.Statement (Statement(..))
import Hasql.Connection (Connection)
import qualified Hasql.Decoders as D
import qualified Hasql.Encoders as E
import Hasql.Session (run, statement, Session)

import Data.Functor.Contravariant (contramap)
import Contravariant.Extras.Contrazip

import GHC.Int (Int64, Int32, Int16)
import GHC.Word (Word32)
import Data.Time.Calendar(Day)
import Data.Time.LocalTime(LocalTime)
import Data.Time.Clock(UTCTime)
import Data.Text(Text)
import Data.ByteString(ByteString)

import Data.Vector
import Entities.Common (TagSC(..), Amount(Amount), unwrapAmount, Qty(Qty), unwrapQty, Key2(Key2), unwrapKey)

execStmt :: Connection -> Statement a b -> a -> IO(b)
execStmt conn stmt a = do
    eiResult <- run (statement a stmt) conn
    case eiResult of
        Left err -> fail $ show err
        Right result -> return result

class Encodable a where
    encoder     :: E.Params a


instance (Encodable a1, Encodable a2) => Encodable (a1, a2) where
    encoder = contramap fst encoder <> contramap snd encoder

instance (Encodable (Vector a1), Encodable (Vector a2)) => Encodable (Vector (a1, a2)) where
    encoder = contramap unzip $ contrazip2 encoder encoder

instance (Encodable a1, Encodable a2, Encodable a3) => Encodable (a1, a2, a3) where
    encoder = contrazip3 encoder encoder encoder

instance (Encodable (Vector a1), Encodable (Vector a2), Encodable (Vector a3)) => Encodable (Vector (a1, a2, a3)) where
    encoder = contramap unzip3 $ contrazip3 encoder encoder encoder

instance (Encodable a1, Encodable a2, Encodable a3, Encodable a4)
            => Encodable (a1, a2, a3, a4) where
    encoder = contrazip4 encoder encoder encoder encoder

instance (Encodable (Vector a1), Encodable (Vector a2), Encodable (Vector a3), Encodable (Vector a4))
            => Encodable (Vector (a1, a2, a3, a4)) where
    encoder = contramap unzip4 $ contrazip4 encoder encoder encoder encoder

instance (Encodable a1, Encodable a2, Encodable a3, Encodable a4, Encodable a5)
            => Encodable (a1, a2, a3, a4, a5) where
    encoder = contrazip5 encoder encoder encoder encoder encoder

instance (Encodable (Vector a1), Encodable (Vector a2), Encodable (Vector a3), Encodable (Vector a4),
            Encodable (Vector a5))
            => Encodable (Vector (a1, a2, a3, a4, a5)) where
    encoder = contramap unzip5 $ contrazip5 encoder encoder encoder encoder encoder

instance (Encodable a1, Encodable a2, Encodable a3, Encodable a4, Encodable a5,
            Encodable a6)
            => Encodable (a1, a2, a3, a4, a5, a6) where
    encoder = contrazip6 encoder encoder encoder encoder encoder encoder

instance (Encodable (Vector a1), Encodable (Vector a2), Encodable (Vector a3), Encodable (Vector a4),
            Encodable (Vector a5), Encodable (Vector a6))
            => Encodable (Vector (a1, a2, a3, a4, a5, a6)) where
    encoder = contramap unzip6 $ contrazip6 encoder encoder encoder encoder encoder encoder

instance (Encodable a1, Encodable a2, Encodable a3, Encodable a4, Encodable a5,
            Encodable a6, Encodable a7)
            => Encodable (a1, a2, a3, a4, a5, a6, a7) where
    encoder = contrazip7 encoder encoder encoder encoder encoder encoder encoder

instance (Encodable a1, Encodable a2, Encodable a3, Encodable a4, Encodable a5,
            Encodable a6, Encodable a7, Encodable a8)
            => Encodable (a1, a2, a3, a4, a5, a6, a7, a8) where
    encoder = contrazip8 encoder encoder encoder encoder
                    encoder encoder encoder encoder

instance (Encodable a1, Encodable a2, Encodable a3, Encodable a4, Encodable a5,
            Encodable a6, Encodable a7, Encodable a8, Encodable a9)
            => Encodable (a1, a2, a3, a4, a5, a6, a7, a8, a9) where
    encoder = contrazip9 encoder encoder encoder encoder encoder encoder encoder encoder encoder

instance (Encodable (Vector a1), Encodable (Vector a2), Encodable (Vector a3), Encodable (Vector a4),
            Encodable (Vector a5), Encodable (Vector a6), Encodable (Vector a7))
            => Encodable (Vector (a1, a2, a3, a4, a5, a6, a7)) where
    encoder = contramap unzip7 $ contrazip7 encoder encoder encoder encoder encoder encoder encoder


instance (Encodable (Vector a1), Encodable (Vector a2), Encodable (Vector a3), Encodable (Vector a4),
            Encodable (Vector a5), Encodable (Vector a6), Encodable (Vector a7), Encodable (Vector a8))
            => Encodable (Vector (a1, a2, a3, a4, a5, a6, a7, a8)) where
    encoder = contramap unzip8 $ contrazip8 encoder encoder encoder encoder encoder encoder encoder encoder

instance (Encodable (Vector a1), Encodable (Vector a2), Encodable (Vector a3), Encodable (Vector a4),
            Encodable (Vector a5), Encodable (Vector a6), Encodable (Vector a7), Encodable (Vector a8), Encodable (Vector a9))
            => Encodable (Vector (a1, a2, a3, a4, a5, a6, a7, a8, a9)) where
    encoder = contramap unzip9 $ contrazip9 encoder encoder encoder encoder encoder encoder encoder encoder encoder

instance Encodable () where
    encoder = E.unit

--------------------------------------------------------------------------------
instance Encodable Int64 where
    encoder = E.param E.int8 
instance Encodable (Maybe Int64) where
    encoder = E.nullableParam E.int8 
instance Encodable (Vector Int64) where
    encoder = E.param $ E.array $ E.dimension foldl' $ E.element E.int8
instance Encodable (Vector (Maybe Int64)) where
    encoder = E.param $ E.array $ E.dimension foldl' $ E.nullableElement E.int8

instance Decodable Int64 where
    decoder = D.column D.int8
instance Decodable (Maybe Int64)  where
    decoder = D.nullableColumn D.int8

--------------------------------------------------------------------------------
instance Encodable Int32 where
    encoder = E.param E.int4 
instance Encodable (Maybe Int32) where
    encoder = E.nullableParam E.int4 
instance Encodable (Vector Int32) where
    encoder = E.param $ E.array $ E.dimension foldl' $ E.element E.int4
instance Encodable (Vector (Maybe Int32)) where
    encoder = E.param $ E.array $ E.dimension foldl' $ E.nullableElement E.int4

instance Decodable Int32 where
    decoder = D.column D.int4
instance Decodable (Maybe Int32)  where
    decoder = D.nullableColumn D.int4

--------------------------------------------------------------------------------
instance Encodable Word32 where
    encoder = contramap fromIntegral $ E.param E.int4

--------------------------------------------------------------------------------
instance Encodable Int16 where
    encoder = E.param E.int2 
instance Encodable (Maybe Int16) where
    encoder = E.nullableParam E.int2 
instance Encodable (Vector Int16) where
    encoder = E.param $ E.array $ E.dimension foldl' $ E.element E.int2
instance Encodable (Vector (Maybe Int16)) where
    encoder = E.param $ E.array $ E.dimension foldl' $ E.nullableElement E.int2

instance Decodable Int16 where
    decoder = D.column D.int2
instance Decodable (Maybe Int16)  where
    decoder = D.nullableColumn D.int2

--------------------------------------------------------------------------------
instance Encodable Day where
    encoder = E.param E.date
instance Encodable (Maybe Day) where
    encoder = E.nullableParam E.date
instance Encodable (Vector Day) where
    encoder = E.param $ E.array $ E.dimension foldl' $ E.element E.date
instance Encodable (Vector (Maybe Day)) where
    encoder = E.param $ E.array $ E.dimension foldl' $ E.nullableElement E.date

instance Decodable Day where
    decoder = D.column D.date
instance Decodable (Maybe Day)  where
    decoder = D.nullableColumn D.date

--------------------------------------------------------------------------------
instance Encodable Text where
    encoder = E.param E.text
instance Encodable (Maybe Text) where
    encoder = E.nullableParam E.text
instance Encodable (Vector Text) where
    encoder = E.param $ E.array $ E.dimension foldl' $ E.element E.text
instance Encodable (Vector (Maybe Text)) where
    encoder = E.param $ E.array $ E.dimension foldl' $ E.nullableElement E.text

instance Decodable Text where
    decoder = D.column D.text
instance Decodable (Maybe Text) where
    decoder = D.nullableColumn D.text

--------------------------------------------------------------------------------
instance Encodable ByteString where
    encoder = E.param E.bytea
instance Encodable (Maybe ByteString) where
    encoder = E.nullableParam E.bytea
instance Encodable (Vector ByteString) where
    encoder = E.param $ E.array $ E.dimension foldl' $ E.element E.bytea
instance Encodable (Vector (Maybe ByteString)) where
    encoder = E.param $ E.array $ E.dimension foldl' $ E.nullableElement E.bytea

instance Decodable ByteString where
    decoder = D.column D.bytea
instance Decodable (Maybe ByteString) where
    decoder = D.nullableColumn D.bytea
--------------------------------------------------------------------------------
instance Encodable Bool where
    encoder = E.param E.bool
instance Encodable (Maybe Bool) where
    encoder = E.nullableParam E.bool
instance Encodable (Vector Bool) where
    encoder = E.param $ E.array $ E.dimension foldl' $ E.element E.bool
instance Encodable (Vector (Maybe Bool)) where
    encoder = E.param $ E.array $ E.dimension foldl' $ E.nullableElement E.bool

instance Decodable Bool where
    decoder = D.column D.bool
instance Decodable (Maybe Bool)  where
    decoder = D.nullableColumn D.bool

--------------------------------------------------------------------------------
instance Encodable Float where
    encoder = E.param E.float4
instance Encodable (Maybe Float) where
    encoder = E.nullableParam E.float4
instance Encodable (Vector Float) where
    encoder = E.param $ E.array $ E.dimension foldl' $ E.element E.float4
instance Encodable (Vector (Maybe Float)) where
    encoder = E.param $ E.array $ E.dimension foldl' $ E.nullableElement E.float4

instance Decodable Float where
    decoder = D.column D.float4
instance Decodable (Maybe Float) where
    decoder = D.nullableColumn D.float4

--------------------------------------------------------------------------------
instance Decodable LocalTime where
    decoder = D.column D.timestamp
instance Decodable (Maybe LocalTime)  where
    decoder = D.nullableColumn D.timestamp

--------------------------------------------------------------------------------
instance Decodable UTCTime where
    decoder = D.column D.timestamptz
instance Decodable (Maybe UTCTime)  where
    decoder = D.nullableColumn D.timestamptz

instance Encodable UTCTime where
    encoder = E.param E.timestamptz

--------------------------------------------------------------------------------
instance Decodable Char where
    decoder = D.column D.char
instance Decodable (Maybe Char) where
    decoder = D.nullableColumn D.char

--------------------------------------------------------------------------------
instance Encodable (Key2 a) where
    encoder = contramap (\(Key2 i) -> i) $ E.param E.int8
instance Encodable (Vector (Key2 a)) where
    encoder = contramap (fmap (\(Key2 i) -> i)) encoder
instance Encodable (Maybe (Key2 a)) where
    encoder = contramap (\case
        Just (Key2 i) -> Just i
        Nothing -> Nothing
            ) $ E.nullableParam E.int8
instance Encodable (Vector (Maybe (Key2 a))) where
    encoder = contramap (fmap (\mk -> fmap unwrapKey mk)) encoder

instance Decodable (Key2 a) where
    decoder = Key2 <$> (D.column D.int8)
instance Decodable (Maybe (Key2 a)) where
    decoder = (fmap Key2) <$> (D.nullableColumn D.int8)
--------------------------------------------------------------------------------

class Decodable a where
    decoder :: D.Row a


instance (Decodable a1, Decodable a2) => Decodable (a1, a2) where
    decoder = (,) <$> decoder <*> decoder

instance (Decodable a1, Decodable a2, Decodable a3) => Decodable (a1, a2, a3) where
    decoder = (,,) <$> decoder <*> decoder <*> decoder

instance (Decodable a1, Decodable a2, Decodable a3, Decodable a4)
            => Decodable (a1, a2, a3, a4) where
    decoder = (,,,) <$> decoder <*> decoder <*> decoder <*> decoder

instance (Decodable a1, Decodable a2, Decodable a3, Decodable a4, Decodable a5)
            => Decodable (a1, a2, a3, a4, a5) where
    decoder = (,,,,) <$> decoder <*> decoder <*> decoder <*> decoder <*> decoder

instance (Decodable a1, Decodable a2, Decodable a3, Decodable a4, Decodable a5, Decodable a6)
            => Decodable (a1, a2, a3, a4, a5, a6) where
    decoder = (,,,,,) <$> decoder <*> decoder <*> decoder <*> decoder <*> decoder <*> decoder

instance (Decodable a1, Decodable a2, Decodable a3, Decodable a4, Decodable a5, Decodable a6, Decodable a7)
            => Decodable (a1, a2, a3, a4, a5, a6, a7) where
    decoder = (,,,,,,) <$> decoder <*> decoder <*> decoder <*> decoder <*> decoder <*> decoder <*> decoder

--instance Encodable (Key a) where
--    encoder = contramap unwrapKey (E.param E.int8)
--instance Encodable (Maybe (Key a)) where
--    encoder = contramap (fmap unwrapKey) (E.nullableParam E.int8)
--instance Encodable (Vector (Key a)) where
--    encoder = E.param $ E.array $ E.dimension foldl' $ E.element $ contramap unwrapKey E.int8
--
--instance Encodable Amount where
--    encoder = contramap unwrapAmount $ E.param $ E.int8
--
--
--instance Decodable (Key a) where
--    decoder = Key <$> decoder
--instance Decodable (Maybe (Key a)) where
--    decoder = (fmap . fmap) Key (D.nullableColumn D.int8)
--
--instance Decodable Amount where
--    decoder = Amount <$> decoder
--instance Decodable (Maybe Amount) where
--    decoder = (fmap Amount) <$> decoder
--
--instance Encodable (Vector Amount) where
--    encoder = E.param $ E.array $ E.dimension foldl' $ E.element $ contramap unwrapAmount E.int8


instance Decodable TagSC where
    decoder = TagSC
        <$> decoder
        <*> decoder

----------------------------------------------------------------------
instance Encodable Amount where
    encoder = contramap unwrapAmount $ E.param $ E.int8

instance Encodable (Vector Amount) where
    encoder = E.param $ E.array $ E.dimension foldl' $ E.element $ contramap unwrapAmount E.int8

instance Decodable Amount where
    decoder = Amount <$> decoder

instance Decodable (Maybe Amount) where
    decoder = (fmap Amount) <$> decoder

----------------------------------------------------------------------
instance Encodable Qty where
    encoder = contramap unwrapQty $ E.param $ E.int4

instance Encodable (Maybe Qty) where
    encoder = contramap (fmap unwrapQty) $ E.nullableParam $ E.int4

instance Encodable (Vector Qty) where
    encoder = E.param $ E.array $ E.dimension foldl' $ E.element $ contramap unwrapQty E.int4

instance Encodable (Vector (Maybe Qty)) where
    encoder = E.param $ E.array $ E.dimension foldl' $ E.nullableElement $ contramap unwrapQty E.int4

instance Decodable Qty where
    decoder = Qty <$> decoder

instance Decodable (Maybe Qty) where
    decoder = (fmap Qty) <$> decoder

----------------------------------------------------------------------
unzip7 :: Vector (a1, a2, a3, a4, a5, a6, a7) -> 
                (Vector a1, Vector a2, Vector a3, Vector a4, Vector a5,
                    Vector a6, Vector a7)
unzip7 v =
    ( fmap (\(a, _, _, _, _, _, _) -> a) v
    , fmap (\(_, a, _, _, _, _, _) -> a) v
    , fmap (\(_, _, a, _, _, _, _) -> a) v
    , fmap (\(_, _, _, a, _, _, _) -> a) v
    , fmap (\(_, _, _, _, a, _, _) -> a) v
    , fmap (\(_, _, _, _, _, a, _) -> a) v
    , fmap (\(_, _, _, _, _, _, a) -> a) v
    )


unzip8 :: Vector (a1, a2, a3, a4, a5, a6, a7, a8) -> 
                (Vector a1, Vector a2, Vector a3, Vector a4, Vector a5,
                    Vector a6, Vector a7, Vector a8)
unzip8 v =
    ( fmap (\(a, _, _, _, _, _, _, _) -> a) v
    , fmap (\(_, a, _, _, _, _, _, _) -> a) v
    , fmap (\(_, _, a, _, _, _, _, _) -> a) v
    , fmap (\(_, _, _, a, _, _, _, _) -> a) v
    , fmap (\(_, _, _, _, a, _, _, _) -> a) v
    , fmap (\(_, _, _, _, _, a, _, _) -> a) v
    , fmap (\(_, _, _, _, _, _, a, _) -> a) v
    , fmap (\(_, _, _, _, _, _, _, a) -> a) v
    )

unzip9 :: Vector (a1, a2, a3, a4, a5, a6, a7, a8, a9) -> 
                (Vector a1, Vector a2, Vector a3, Vector a4, Vector a5,
                    Vector a6, Vector a7, Vector a8, Vector a9)
unzip9 v =
    ( fmap (\(a, _, _, _, _, _, _, _, _) -> a) v
    , fmap (\(_, a, _, _, _, _, _, _, _) -> a) v
    , fmap (\(_, _, a, _, _, _, _, _, _) -> a) v
    , fmap (\(_, _, _, a, _, _, _, _, _) -> a) v
    , fmap (\(_, _, _, _, a, _, _, _, _) -> a) v
    , fmap (\(_, _, _, _, _, a, _, _, _) -> a) v
    , fmap (\(_, _, _, _, _, _, a, _, _) -> a) v
    , fmap (\(_, _, _, _, _, _, _, a, _) -> a) v
    , fmap (\(_, _, _, _, _, _, _, _, a) -> a) v
    )

crtTrxTagStmt :: Statement Text Int64
crtTrxTagStmt = Statement query encoder (D.singleRow decoder) True
    where
    query = [r|
        insert into trxtag (dt) values ($1) returning id
        |]
