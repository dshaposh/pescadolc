{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

module ApiType where

import GHC.Int(Int64, Int16)
--import qualified Data.Vector as V
--import Data.Time.Calendar (Day)
import qualified Data.Text as T
import Web.Cookie (SetCookie)
import Data.Time (UTCTime, Day)
--import Data.Time.LocalTime (LocalTime)
import Data.Vector (Vector)
import Servant.API
import qualified Servant.Multipart as SMP

import Authentication(Phone)

import Network.HTTP.Media ((//), (/:))

import qualified Data.ByteString.Lazy as BSL
import qualified Data.ByteString as BS
import Data.Time.Clock (NominalDiffTime)

import Entities.Common (Key, Key2, Amount, Qty)
import qualified Entities.Client as CE
import qualified Entities.User as UE
import qualified Entities.Order as OE
import qualified Entities.Inventory as IE
import qualified Sms
import qualified Cms
--import qualified Authentication as EA
import qualified Data.Vault.Lazy as Vlt

data Html
instance Accept Html where
    contentType _ = "text" // "html" /: ("charset", "utf-8")

data Image
instance Accept Image where
    contentType _ = "image" // "jpeg"


type FullAPI =
         "static" :> Raw
    :<|> "protected" :> ProtectedAPI
    :<|> ClientAPI
    :<|> CaptureAll "" T.Text :> Get '[Html] BSL.ByteString


type ProtectedAPI = 
             Vlt.Vault :> Get '[Html] BSL.ByteString
        :<|> "html" :> "login" :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> "login" :> ReqBody '[JSON] UE.Credentials :> Post '[JSON] (Headers '[Header "set-cookie" SetCookie] ()) 
        :<|> Vlt.Vault :> "logout" :> Post '[Html] (Headers '[Header "set-cookie" SetCookie] BSL.ByteString) 

        :<|> Vlt.Vault :> "html" :> "users" :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> "html" :> "cuttings" :> Get '[Html] BSL.ByteString

        :<|> Vlt.Vault :> "html" :> "species" :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> "html" :> "species" :> "new" :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> "html" :> "species" :> Capture "id" (Key2 IE.Specie) :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> "species" :> ReqBody '[JSON] IE.Specie :> Post '[JSON] (Key2 IE.Specie)
        :<|> Vlt.Vault :> "species" :> Capture "id" (Key2 IE.Specie) :> Delete '[JSON] ()
        :<|> Vlt.Vault :> "species" :> Capture "id" (Key2 IE.Specie) :> ReqBody '[JSON] Amount :> Patch '[JSON] ()
        :<|> Vlt.Vault :> "species" :> Capture "id" (Key2 IE.Specie) :> "image" :> SMP.MultipartForm SMP.Mem (SMP.MultipartData SMP.Mem) :> Post '[JSON] ()

        :<|> Vlt.Vault :> "html" :> "vendors" :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> "html" :> "vendors" :> "new" :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> "vendors" :> ReqBody '[JSON] (IE.Vendor) :> Post '[JSON] (Key IE.Vendor)

        :<|> Vlt.Vault :> "html" :> "courriers" :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> "html" :> "courriers" :> "new" :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> "courriers" :> ReqBody '[JSON] (OE.Courrier) :> Post '[JSON] (Key OE.Courrier)

        :<|> Vlt.Vault :> "html" :> "labels" :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> "html" :> "labels" :> "new" :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> "labels" :> ReqBody '[JSON] T.Text :> Post '[JSON] ()
        :<|> Vlt.Vault :> "labels" :> ReqBody '[JSON] T.Text :> Delete '[JSON] ()

        :<|> Vlt.Vault :> "html" :> "items" :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> "html" :> "items" :> Capture "id" (Key2 IE.Item) :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> "items" :> Capture "id" (Key2 IE.Item) :> Delete '[JSON] ()
        :<|> Vlt.Vault :> "items" :> Capture "id" (Key2 IE.Item) :> ReqBody '[JSON] Int16 :> Patch '[JSON] ()
        :<|> Vlt.Vault :> "html" :> "items" :> "new" :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> "items" :> ReqBody '[JSON] (IE.Item) :> Post '[JSON] (Key2 IE.Item)

        :<|> Vlt.Vault :> "html" :> "purchases" :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> "html" :> "purchase" :> "new" :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> "purchase" :> ReqBody '[JSON] IE.PrchDoc :> Post '[JSON] (Key IE.PrchHdr)

        :<|> Vlt.Vault :> "html" :> "stock" :> "summary" :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> "html" :> "stock" :> "item" :> Capture "id" (Key2 IE.Item) :> Get '[Html] BSL.ByteString

        :<|> Vlt.Vault :> "html" :> "stock" :> "markitem" :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> "html" :> "stock" :> "labeled" :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> "html" :> "stock" :> "labeled" :> Capture "id" (Key2 IE.Token3) :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> "stock" :> "labeled" :> Capture "id" (Key2 IE.Token3) :> "writeoff" :> Post '[JSON] ()
---------------------------
        :<|> Vlt.Vault :> "html" :> "stock" :> QueryParam "ind" (Key2 IE.Token3) :> QueryParam "item" (Key2 IE.Item) :> QueryParam "lot" (Key2 IE.Token) :> QueryParam "place" (Key2 IE.Place) :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> "html" :> "stock" :> "writeoff" :> QueryParam "ind" (Key2 IE.Token3) :> QueryParam "item" (Key2 IE.Item) :> QueryParam "lot" (Key2 IE.Token) :> QueryParam "place" (Key2 IE.Place) :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> "stock" :> "writeoff" :> ReqBody '[JSON] (IE.WriteOff 'IE.Lot3) :> Post '[JSON] (Key2 (IE.WriteOff 'IE.Lot3))
---------------------------
        :<|> Vlt.Vault :> "html" :> "stock" :> "lots" :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> "html" :> "stock" :> "tolabel" :> QueryParam "item" (Key2 IE.Item) :> QueryParam "place" (Key IE.Place) :> QueryParam "lot" (Key2 IE.Token) :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> "stock" :> "tolabel" :> ReqBody '[JSON] (IE.MarkItemDoc) :> Post '[JSON] (Key IE.MarkItemDoc)

        :<|> Vlt.Vault :> "html" :> "orders" :> QueryParam "phone" T.Text :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> "html" :> "orders" :> "2pay" :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> "html" :> "orders" :> "rows" :> "2prepare" :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> "html" :> "orders" :> "2dispatch" :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> "html" :> "orders" :> "2deliver" :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> "html" :> "orders" :> "rows" :> Capture "id" (Key2 (OE.OrderRow 'IE.Lot3)) :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> "orders" :> "rows" :> Capture "id" (Key2 (OE.OrderRow 'IE.Lot3)) :> "prepare" :> ReqBody '[JSON] (Vector (Key2 IE.Place, Qty)) :> Post '[JSON] ()
        :<|> Vlt.Vault :> "orders" :> "payments" :> ReqBody '[JSON] (OE.Order 'OE.Paid) :> Post '[JSON] ()
        :<|> Vlt.Vault :> "orders" :> "deliveries" :> ReqBody '[JSON] (Key2 (OE.Order 'OE.Dispatched)) :> Post '[JSON] ()
        :<|> Vlt.Vault :> "html" :> "orders" :> "payments" :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> "orders" :> "dispatches" :> ReqBody '[JSON] (OE.DspHdr, Vector (Key2 (OE.Order 'OE.Paid))) :> Post '[JSON] ()
        :<|> Vlt.Vault :> "html" :> "orders" :> "dispatches" :> QueryParam "dayFrom" Day :> QueryParam "dayTo" Day :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> "html" :> "orders" :> Capture "id" (Key2 OE.OrderHdr) :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> "orders" :> Capture "id" (Key2 OE.OrderHdr) :> "laststatus" :> Delete '[JSON] ()
        :<|> Vlt.Vault :> "html" :> "delivery" :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> "delivery" :> "price" :> ReqBody '[JSON] Amount :> Post '[JSON] ()
        :<|> Vlt.Vault :> "delivery" :> "day" :> ReqBody '[JSON] Day :> Post '[JSON] ()
        :<|> Vlt.Vault :> "html" :> "smsservice" :> RemoteHost :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> "smsservice" :> RemoteHost :> ReqBody '[JSON] T.Text :> Post '[JSON] [Sms.SendStatus]
        :<|> Vlt.Vault :> "html" :> "visitorlog" :> Get '[Html] BSL.ByteString

        :<|> Vlt.Vault :> "html" :> "cms" :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> "cms" :> SMP.MultipartForm SMP.Mem (SMP.MultipartData SMP.Mem) :> Post '[JSON] ()


--------------------------------------------------------------------------
type ClientAPI =
             Vlt.Vault :> RemoteHost :> "html" :> "login" :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> "pcart" :> ReqBody '[JSON] IE.AnyMerch :> Post '[JSON] ()
        :<|> Vlt.Vault :> "pcart" :> ReqBody '[JSON] IE.AnyMerchK :> Delete '[JSON] ()
        :<|> Vlt.Vault :> "order" :> ReqBody '[JSON] (OE.OrderHdr, Vector IE.OrderRow) :> Post '[JSON] ()
        :<|> Vlt.Vault :> RemoteHost :> "html" :> "pcart" :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> "html" :> "orders" :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> "html" :> "orders" :> Capture "id" (Key2 OE.OrderHdr) :> Get '[Html] BSL.ByteString
        :<|> Vlt.Vault :> RemoteHost :> Header "user-agent" T.Text :> Get '[Html] (Headers '[Header "set-cookie" SetCookie] BSL.ByteString)
        :<|> Vlt.Vault :> RemoteHost :> "login" :> "credentials" :> ReqBody '[JSON] CE.Credentials :> Post '[JSON] NominalDiffTime 
        :<|> Vlt.Vault :> RemoteHost :> "login" :> "newcode" :> Post '[JSON] NominalDiffTime 
        :<|> Vlt.Vault :> "login" :> "code" :> ReqBody '[JSON] T.Text :> Post '[JSON] ()
        :<|> Vlt.Vault :> RemoteHost :> "logout" :> Post '[JSON] NominalDiffTime 
        :<|> Vlt.Vault :> "html" :> "ourfish" :> Get '[Html] BSL.ByteString 
        :<|> "species" :> Capture "id" (Key2 IE.Specie) :> "image" :> Header "If-Modified-Since" UTCTime :> Get '[Image] (Headers '[Header "Cache-Control" T.Text, Header "Last-Modified" UTCTime] BS.ByteString)
        :<|> Vlt.Vault :> "html" :> Capture "rsrName" Cms.Resource :> Get '[Html] BSL.ByteString

