{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE UndecidableInstances #-}
-- 
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE ExplicitForAll #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE RankNTypes #-}
-- {-# LANGUAGE TypeInType #-}
-- {-# LANGUAGE FlexibleContexts #-}

module Entities.Inventory where

import GHC.Generics
import GHC.Int(Int16,Int64(..))
import Data.Maybe (isNothing, fromJust)
import Data.Vector (Vector)
import Data.Time.Calendar(Day)
import Data.Time (UTCTime)
import Data.Time.LocalTime(LocalTime)
import qualified Data.Text as T
import Data.Aeson (FromJSON, ToJSON, parseJSON, withObject, (.:), (.:?), toJSON, object, (.=))
import Entities.Common (Key2, TagSC, TagDoc, Persistable, FromDb, Key, Tag, TShow, tshow, Amount, Qty)


data Uom = Uom
    { name      :: T.Text
    , ordnum    :: Int16
    } deriving (Generic)
instance FromJSON Uom
instance ToJSON Uom
instance Persistable Uom where
    type Key Uom = Int16
    type Tag Uom = TagSC

data Specie = Specie
    { name      :: T.Text
    , ordnum    :: Int16
    , uom       :: Key2 Uom
    , price     :: Amount
    } deriving (Generic, Eq)
instance FromJSON Specie
instance ToJSON Specie
instance Persistable Specie where
    type Key Specie = Key2 Specie
    type Tag Specie = (T.Text, UTCTime, Bool) 

data Cutting = Cutting
    { name      :: T.Text
    , ordnum    :: Int16
    } deriving (Generic)
instance FromJSON Cutting
instance Persistable Cutting where
    type Key Cutting = Int16
    type Tag Cutting = TagSC


data Item = Item
    { name      :: T.Text
    , ordnum    :: Int16
    , specie    :: Key Specie
    , cutting   :: Key Cutting
    , uom       :: Key Uom
    , size      :: Size
    , markup    :: Int16
    } deriving (Generic)
instance FromJSON Item
instance ToJSON Item
instance Show Item where
    show (Item name _ _ _ _ size _) = (T.unpack name) ++ ", " ++ (show size)
instance Persistable Item where
    type Key Item = Int64
    type Tag Item = TagSC
type ItemDescr = (T.Text, T.Text, T.Text, Amount)

data Size
    = NoSize
    | Fixed Qty
    | Variable
    deriving(Generic, Eq)
instance FromJSON Size
instance ToJSON Size
instance Show Size where
    show NoSize = "NA"
    show (Fixed sz) = show sz
    show Variable = "Variable"

--------------------------------------------------------------
data Place = Place
    { name      :: T.Text
    , ordnum    :: Int16
    } deriving (Generic)
instance FromJSON Place
instance Persistable Place where
    type Key Place  = Int16
    type Tag Place = TagSC

newtype Label = Label T.Text deriving (Generic)
instance FromJSON Label

data Vendor = Vendor
    { name      :: T.Text
    , ordnum    :: Int16
    , taxid     :: T.Text
    , phone     :: T.Text
    } deriving (Generic)
instance FromJSON Vendor
instance Persistable Vendor where
    type Key Vendor  = Int16
    type Tag Vendor = TagSC



data Token = Token
    { code      :: T.Text
    , day       :: Day
    , note      :: T.Text
    } deriving (Generic)
instance FromJSON Token
instance ToJSON Token
instance Persistable Token where
    type Key Token = Int64
    type Tag Token = ()

data Balance = Balance
    { wh        :: Key Place
    , item      :: Key Item
    , token     :: Key Token
    , qty       :: Qty
    , amount    :: Amount
    } deriving (Generic) 
instance ToJSON Balance where
instance Persistable Balance where
    type Key Balance = (Key Place, Key Item, Key Token)
    type Tag Balance = ()


--------------------------------------------------------------
data LblStatus
    = AtStock (Key Place)
    | Gone

data Token3 = Token3
    { label     :: T.Text
    , stock     :: Stock3 'Lot3
    } deriving (Generic)
instance FromJSON Token3

instance Persistable Token3 where
    type Key Token3 = Key2 Token3
    type Tag Token3 = (Int64, Int64) 

data SType = Lot3 | Ind3

data MerchK (k :: SType) where
    MerchLotK :: Key2 Item -> Key2 Token -> MerchK 'Lot3
    MerchIndK :: Key2 Token3 -> MerchK 'Ind3
instance FromJSON (MerchK 'Lot3) where
    parseJSON = withObject "Merchandise Lot Key" $ \v -> MerchLotK
        <$> v .: "itemK"
        <*> v .: "tokenK"
instance FromJSON (MerchK 'Ind3) where
    parseJSON = withObject "Merchandise Ind Key" $ \v -> MerchIndK
        <$> v .: "indK"

instance forall (k :: SType). ToJSON (MerchK k) where
    toJSON (MerchLotK iK tK) = object
        [ "itemK" .= iK
        , "tokenK" .= tK
        ]
    toJSON (MerchIndK iK) = object
        [ "indK" .= iK
        ]


--type AnyMerchKExt = (Maybe (MerchK 'Ind3), MerchK 'Lot3)
-- I believe that this type can be removed
data AnyMerchK where
    AnyMerchK :: forall (k :: SType). MerchK k -> AnyMerchK
instance FromJSON AnyMerchK where
    parseJSON = withObject "Any Merchandise Key" $ \v -> do
        mInd <- v .:? "indK"
        if isNothing mInd then do
            lotK <- v .: "lotK"
            return $ AnyMerchK (lotK :: MerchK 'Lot3)
        else return $ AnyMerchK $ MerchIndK $ fromJust mInd

data MerchBal where
    MerchBal ::
        { merchK    :: forall (k :: SType). MerchK k
        , qty       :: Qty
        , amount    :: Amount
        , rsqty     :: Qty
--        , item      :: Item
--        , uom       :: Uom
        } -> MerchBal

data MerchPart (k :: SType) where
    MerchLotP :: MerchK 'Lot3 -> Qty -> MerchPart 'Lot3
    MerchIndP :: MerchK 'Ind3 -> MerchPart 'Lot3

-- Shall be renamed to MerchPart
data Merch2 (k :: SType) where
    MerchLot2 :: MerchK 'Lot3 -> Qty -> Merch2 'Lot3
    MerchInd2 :: Key2 Token3 -> Merch2 'Ind3
instance forall (k :: SType). ToJSON (Merch2 k) where
    toJSON (MerchLot2 lotK qty) = object
        [ "lotK" .= lotK
        , "qty" .= qty
        ]
    toJSON (MerchInd2 indK) = object
        [ "indK" .= indK
        ]

extrMerchK :: AnyMerch -> AnyMerchK
extrMerchK (AnyMerch (MerchLot2 k _)) = AnyMerchK k
extrMerchK (AnyMerch (MerchInd2 k)) = AnyMerchK $ MerchIndK k

type SalesOffer = (Maybe (MerchK 'Ind3), Merch2 'Lot3, Amount) -- Amount is price
data AnyMerch where
    AnyMerch :: forall (k :: SType). Merch2 k -> AnyMerch
instance FromJSON AnyMerch where
    parseJSON = withObject "Any Merchandise" $ \v -> do
        mInd <- v .:? "indK"
        if isNothing mInd then do
            lotK <- v .: "lotK"
            qty <- v .: "qty"
            return $ AnyMerch $ MerchLot2 lotK qty
        else do
            return $ AnyMerch $ MerchInd2 (fromJust mInd)
instance ToJSON AnyMerch where
    toJSON (AnyMerch (MerchLot2 lotK qty)) = object
        [ "lotK" .= lotK
        , "qty" .= qty
        ]
    toJSON (AnyMerch (MerchInd2 indK)) = object
        [ "indK" .= indK
        ]

type OrderRow = (AnyMerch, Amount, T.Text)
--data OrderRow where
--    OrderRow ::
--        { merch     :: AnyMerch
--        , amount    :: Amount
--        , instr     :: T.Text
--        } -> OrderRow

data OrderRow2 where
    OrderRow2 ::
        { indK      :: Maybe (Key2 Token3)
        , merch     :: Merch2 'Lot3
        , amount    :: Amount
        , instr     :: T.Text
        } -> OrderRow2

--instance FromDb OrderRow3 where
--    key OrderRow3
--data Merch (k :: SType) where
--    MerchLot :: MerchK 'Lot3 -> Qty -> Amount -> Qty -> Merch 'Lot3
--    MerchInd :: Token3 -> Maybe (Key2 (OrderRow3 'Ind3)) -> Merch 'Ind3
--instance FromJSON (Merch 'Lot3) where
--    parseJSON = withObject "Merchandise Lot" $ \v -> MerchLot
--        <$> v .: "merchK"
--        <*> v .: "qty"
--        <*> v .: "amount"
--        <*> v .: "rsqty"
--instance FromJSON (Merch 'Ind3) where
--   parseJSON = withObject "Merchandise Ind" $ \v -> MerchInd
--        <$> v .: "ind"
--        <*> v .: "order"




-------------------------------------------------------------------
data StockK (k :: SType) where
    StockLotK :: MerchK 'Lot3 -> Key2 Place -> StockK 'Lot3
    StockIndK :: Key2 Token3 -> StockK 'Ind3
instance forall (k :: SType). ToJSON (StockK k) where
    toJSON (StockLotK merchK placeK) = object
        [ "merchK" .= merchK
        , "placeK" .= placeK
        ]
    toJSON (StockIndK indK) = object
        [ "indK" .= indK
        ]
instance FromJSON (StockK 'Lot3) where
    parseJSON = withObject "Stock Lot Key" $ \v -> StockLotK
        <$> v .: "merchK"
        <*> v .: "placeK"
instance FromJSON (StockK 'Ind3) where
    parseJSON = withObject "Stock Ind Key" $ \v -> StockIndK
        <$> v .: "indK"

data WriteOff (k :: SType) where
    WriteOffLot :: StockK 'Lot3 -> Qty -> Day -> T.Text -> WriteOff 'Lot3
    WriteOffInd :: Key2 Token3 -> Day -> T.Text -> WriteOff 'Ind3
instance FromJSON (WriteOff 'Lot3) where
    parseJSON = withObject "WriteOff Lot" $ \v -> WriteOffLot
        <$> v .: "stockK"
        <*> v .: "qty"
        <*> v .: "day"
        <*> v .: "note"
instance FromJSON (WriteOff 'Ind3) where
    parseJSON = withObject "WriteOff Ind" $ \v -> WriteOffInd
        <$> v .: "indK"
        <*> v .: "day"
        <*> v .: "note"


data Stock3 (k :: SType) where
    StockLot :: StockK 'Lot3 -> Qty -> Amount -> Stock3 'Lot3
-- I believe that StockInd (next line can be deleted. Not used
    StockInd :: Key2 Token3 -> Stock3 'Ind3
instance FromJSON (Stock3 'Lot3) where
    parseJSON = withObject "Stock Lot" $ \v -> StockLot
        <$> v .: "stockK"
        <*> v .: "qty"
        <*> v .: "amount"
instance FromJSON (Stock3 'Ind3) where
    parseJSON = withObject "Stock Ind" $ \v -> StockInd
        <$> v .: "ind"
instance forall (k :: SType). ToJSON (Stock3 k) where
    toJSON (StockLot lotK qty amount) = object
        [ "stockK" .= lotK
        , "qty" .= qty
        , "amount" .= amount
        ]
    toJSON (StockInd indK) = object
        [ "indK" .= indK
        ]

data AnyStock where
    AnyStock :: forall (k :: SType). Stock3 k -> AnyStock


data MvmtDoc where
    MvmtDoc     :: forall (k :: SType). Stock3 k -> Key2 Place -> MvmtDoc



data StockKey = StockKey
    { item      :: Key2 Item
    , token     :: Key Token
    , place     :: Key Place
    } deriving(Generic)
instance FromJSON StockKey where

data Lot where
    Lot :: StockKey -> Qty -> Amount -> Lot
    deriving (Generic)


data Stock where
    FromLot :: Lot -> Stock
    FromInd :: Key Token22 -> Lot -> Stock
    deriving (Generic)

data Token22 = Token22
    { code      :: T.Text
    } deriving(Generic)
instance FromJSON Token22 where
instance Persistable Token22 where
    type Key Token22 = Int64
    type Tag Token22 = TagSC

data MarkItemDoc2 = MarkItemDoc2
    { lot       :: Lot
    , token2    :: Token22
    , note      :: T.Text
    } deriving (Generic)
--instance FromJSON MarkItemDoc2 where
instance Persistable MarkItemDoc2 where
    type Key MarkItemDoc2 = Int64
    type Tag MarkItemDoc2 = TagSC
--------------------------------------------------------------
-- Token2 and MarkItemDoc shall be deleted and replaced by 
-- Token22 and MarkItemDoc2
data Token2 = Token2
    { code      :: T.Text
    , token     :: Key Token
    , item      :: Key Item
    , qty       :: Qty
    , amount    :: Amount
    , wh        :: Where 
    } deriving (Generic)
instance FromJSON Token2
instance Persistable Token2 where
    type Key Token2 = Int64
    type Tag Token2 = TagSC


data MarkItemDoc = MarkItemDoc
    { from      :: Key Balance
    , token2    :: Token2
    , note      :: T.Text
    } deriving (Generic)
instance FromJSON MarkItemDoc where
instance Persistable MarkItemDoc where
    type Key MarkItemDoc = Int64
    type Tag MarkItemDoc = TagSC

data Where = AtPlace Int64 | AtWo Int64 | AtSo Int64 deriving (Generic) 
instance FromJSON Where

--data Origin = FromPrch (Key PrchRow) | FromWo (Key WoOut) deriving (Generic) 
--instance FromJSON Origin
-- End of OldBlock
-------------------------------------------------------------
data PrchHdr = PrchHdr
    { vendor    :: Key Vendor
    , docday    :: Day
    , docnum    :: T.Text
    , note      :: T.Text
    } deriving (Generic) 
instance FromJSON PrchHdr
instance Persistable PrchHdr where
    type Key PrchHdr  = Int64
    type Tag PrchHdr = Amount
instance Show PrchHdr where
    show (PrchHdr vendor docday docnum note) = "Purchase (vendor: "
            ++ (show vendor) ++ ", doc: " ++ (T.unpack docnum) ++ ", date: "
            ++ (show docday) ++ ")"

data PrchRow = PrchRow
    { item      :: Key Item
    , place     :: Key Place
    , qty       :: Qty
    , amount    :: Amount
    } deriving (Generic) 
instance FromJSON PrchRow
instance Persistable PrchRow where
    type Key PrchRow = Int64
    type Tag PrchRow = (Key PrchHdr, Key Token)

type PrchDoc = (PrchHdr, Vector PrchRow)






{--
data WoHdr = WoHdr
    { origin    :: Origin
    , docday    :: Day
    , docnum    :: T.Text
    , note      :: T.Text
    } deriving (Generic) 
instance FromJSON WoHdr
instance Persistable WoHdr where
    type Key WoHdr = Int64
    type Tag WoHdr = TagSC

data WoIn = WoIn
    { origin    :: Origin
    , qtyLog    :: Int16
    , qtyPrc    :: Int16
    } deriving (Generic) 
instance FromJSON WoIn
instance Persistable WoIn where
    type Key WoIn = Int64
    type Tag WoIn = Key WoHdr

data WoCls = WoCls
    { wo        :: Key WoHdr
    , docday    :: Day
    , note      :: T.Text
    } deriving (Generic) 

data WoOut = WoOut
    { item      :: Key Item
    , place     :: Key Place
    , qty       :: Qty
    } deriving (Generic) 
instance Persistable WoOut where
    type Key WoOut = Int64
    type Tag WoOut = Key WoHdr
--}      
