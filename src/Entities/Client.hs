{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE InstanceSigs #-}

module Entities.Client where

import Text.RawString.QQ (r)
import GHC.Int(Int64, Int16, Int8)
import qualified Data.Vector as V
import qualified Data.Text as T
import qualified Data.ByteString as BS
import Data.Time.Clock (UTCTime)

import GHC.Generics
import Data.Aeson (FromJSON(parseJSON), withObject, (.:), ToJSON)

import Network.Socket (SockAddr(SockAddrInet), HostAddress)

import Data.Functor.Contravariant (contramap)
import qualified Hasql.Encoders as E
import qualified Hasql.Decoders as D
import Db(Encodable(..), Decodable(..))
import Hasql.Statement (Statement(..))

import Entities.Common (Persistable, Key, Tag)


newtype Phone = Phone T.Text deriving (Generic, Eq)
instance Show Phone where
    show (Phone n) = T.unpack n
instance ToJSON Phone
instance FromJSON Phone


data AuthStatus = Authenticated | CodeSent T.Text Int8 deriving (Generic, Eq)
instance ToJSON AuthStatus


data Visitor
    = Anonimous
    | Client
        { phone     :: Phone
        , authst    :: AuthStatus
        , tzshift   :: Int16
        }
    deriving (Generic, Eq)
instance ToJSON Visitor

isAuthenticated :: Maybe Session -> Bool
isAuthenticated (Just (Session (Client _ Authenticated _) _ _)) = True
isAuthenticated _ = False

instance Show Visitor where
    show (Anonimous) = "Anonimous"
    show (Client (Phone phone) _ _) = "Teléfono " ++ (T.unpack phone)


data Session = Session
    { visitor   :: Visitor
    , expires   :: UTCTime
    , sid       :: BS.ByteString
    } deriving (Generic)

getPhone :: Session -> Maybe Phone
getPhone (Session (Client phone Authenticated _) _ _) = Just phone
getPhone _ = Nothing

data Credentials = Credentials
    { phone     :: Phone
    , tzshift   :: Int16
    } deriving (Generic)
instance FromJSON Credentials
instance ToJSON Credentials


data VisitorLog = VisitorLog
    { sid       :: BS.ByteString
    , ip        :: HostAddress
    , userAgent :: T.Text
    } deriving (Generic)

instance Encodable VisitorLog where
    encoder :: E.Params VisitorLog
    encoder
        =  contramap (sid :: VisitorLog -> BS.ByteString) (E.param E.bytea)
        <> contramap (fromIntegral . ip) (E.param E.int4)
        <> contramap userAgent (E.param E.text)

instance Decodable VisitorLog where
    decoder :: D.Row VisitorLog
    decoder = VisitorLog
        <$> decoder
        <*> D.column (fmap fromIntegral D.int4)
        <*> decoder

saveLogEntry :: Statement VisitorLog ()
saveLogEntry = Statement query encoder D.unit True
    where
    query = [r|
        insert into visitorlog (sid, ip, useragent) values ($1, $2, $3)
        |]
getLog :: Statement () (V.Vector (VisitorLog, UTCTime, Int64, Int64))
getLog = Statement query encoder (D.rowVector decoder) True
    where
    query = [r|
        select l.sid, l.ip, l.useragent, l.savedat, coalesce(p.cnt, 0), coalesce(p.amt, 0)
        from visitorlog l
        left join (
            select sid, count(sid) as cnt, sum(amount) as amt
            from pcart
            where so is not null
            group by sid
        ) p
        on l.sid = p.sid
        order by savedat desc
        limit 40
        |]
