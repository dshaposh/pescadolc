{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE ExplicitForAll #-}
module Entities.Common where

import Prelude hiding (String(..))
import GHC.Generics
import Data.Time.LocalTime(LocalTime)
import Data.Text (Text, pack)
import GHC.Int (Int32, Int64)
import Data.Aeson (FromJSON, ToJSON, parseJSON, withObject, (.:), Value(String), toJSON)

import qualified Web.HttpApiData as WHAD

---------------------------------------------------------------------------------
newtype Amount = Amount Int64 deriving (Generic, Eq, Ord, Num, Enum, Real, Integral)
unwrapAmount :: Amount -> Int64
unwrapAmount (Amount am) = am

instance ToJSON Amount
instance FromJSON Amount

instance Show Amount where
    show = reverse . f . reverse .show . unwrapAmount
        where
        f (a:b:c:d:e) = [a,b,c,'.'] ++ (f $ d:e)
        f x = x

prettyPrice :: Amount -> Amount
prettyPrice a = priceStep * ((fromIntegral a) `div` priceStep)
    where
    priceStep = 10000


---------------------------------------------------------------------------------
newtype Qty = Qty Int32 deriving (Generic, Eq, Ord, Num, Enum, Real, Integral)
unwrapQty :: Qty -> Int32
unwrapQty (Qty qty) = qty

instance ToJSON Qty
instance FromJSON Qty

instance Show Qty where
    show (Qty qty) = show $ (fromIntegral qty) / 100

multQty :: Int -> Qty -> Qty
multQty n q = Qty $ (fromIntegral n) * (unwrapQty q)

---------------------------------------------------------------------------------

class TShow a where
    tshow :: a -> Text


instance (Show a) => TShow a where
    tshow = pack . show

class Persistable a where
    type Key a :: *
    type Tag a :: *

data TagSC = TagSC
    { savedAt   :: LocalTime
    , isClosed  :: Bool
    }

data TagDoc = TagDoc
    { savedAt   :: LocalTime
    , savedBy   :: Int64
    }

type FromDb a = (a, Key a, Tag a)

newtype Key2 a = Key2 Int64 deriving (Eq)
unwrapKey :: forall a. Key2 a -> Int64
unwrapKey (Key2 k) = k

instance FromJSON (Key2 a) where
    parseJSON a = Key2 <$> (parseJSON a)
instance ToJSON (Key2 a) where
    toJSON a = toJSON k
        where
        Key2 k = a 
instance Show (Key2 a) where
    show (Key2 a) = show a
instance WHAD.FromHttpApiData (Key2 a) where
    parseUrlPiece p = fmap Key2 $ WHAD.parseUrlPiece p
    parseHeader h = Key2 <$> WHAD.parseHeader h
    parseQueryParam q = Key2 <$> WHAD.parseQueryParam q

class Persistable2 a where
    type Tag2 a :: *

type DbEntity a = (a, Key2 a, Tag2 a)
    
