{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeFamilies #-}


module Entities.User where

import GHC.Int(Int64, Int16)
import qualified Data.Text as T
import qualified Data.ByteString as BS
import Data.Time.Clock (UTCTime)

import GHC.Generics
import Data.Aeson (FromJSON(parseJSON), withObject, (.:), ToJSON)

import Entities.Common (Persistable2, Key2, Tag2)


data UserRole = Admin | Operator deriving (Generic)
instance ToJSON UserRole
instance FromJSON UserRole
instance Show UserRole where
    show Admin = "Administrador"
    show Operator = "Operador"

data User = User
    { usrRole  :: UserRole
    , usrNick  :: T.Text
    , usrName  :: T.Text
    , usrEmail :: T.Text
    } deriving (Generic)
instance FromJSON User
instance ToJSON User

instance Persistable2 User where
    type Tag2 User = (UTCTime, Bool)

instance Show User where
    show (User role nick _ _) = "Usuario " ++ (T.unpack nick)


data Session = Session
    { key       :: Key2 User
    , login     :: T.Text
    , role      :: UserRole
    , expires   :: UTCTime
    , tzshift   :: Int16
    , sid       :: BS.ByteString
    }

data Credentials = Credentials T.Text T.Text Int16
    deriving (Generic)
instance FromJSON Credentials
instance ToJSON Credentials

