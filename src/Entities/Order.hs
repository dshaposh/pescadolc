{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleInstances #-}


{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeInType #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE UndecidableInstances #-}

module Entities.Order where

import GHC.Generics
import GHC.Int(Int16,Int64)
import Data.Time.Calendar(Day)
import Data.Time.LocalTime(LocalTime)
import qualified Data.Text as T
import Data.Aeson (FromJSON, parseJSON, withObject, (.:))
import Entities.Common (TagSC, Persistable, FromDb, Key, Amount, Qty, Key2)
import qualified Entities.Common as C
import qualified Entities.Client as CE
import qualified Entities.Inventory as IE


data OrderHdr = OrderHdr
    { phone     :: CE.Phone
    , dlvDay    :: Day
    , name      :: T.Text
    , address   :: T.Text
    , dlvCost   :: Amount
    , total     :: Amount
    } deriving (Generic)
instance FromJSON OrderHdr
instance Persistable OrderHdr where
    type Key OrderHdr = Int64
    type Tag OrderHdr = (Status, OKey)

data OKey where
    OKey :: forall (k :: Status). C.Key2 (Order k) -> OKey
instance Eq OKey where
    (==) (OKey (C.Key2 k1)) (OKey (C.Key2 k2)) = k1 == k2
instance Show OKey where
    show (OKey k) = show k



data OrderRow (s :: IE.SType) where
   OrderRow :: IE.Merch2 a -> Amount -> T.Text -> Key2 (OrderRow 'IE.Lot3) -> OrderRow a

data PrpOrderRow (s :: IE.SType) where
    PrpOrderInd :: (Key2 (OrderRow 'IE.Ind3)) -> PrpOrderRow 'IE.Ind3
    PrpOrderLot :: (Key2 (OrderRow 'IE.Lot3)) -> [(IE.Place, Qty)] -> PrpOrderRow 'IE.Lot3

data WorkOrder = WorkOrder
    { solot     :: Key2 (OrderRow 'IE.Lot3)
    , wh        :: Key2 IE.Place
    } deriving (Generic)
instance FromJSON (WorkOrder)

--data OrderItem
--    -- = OrderLot (Key IE.Token) (Key IE.Item) Qty
--    = OrderLot (IE.MerchK 'IE.Lot3) Qty
--    | OrderInd (Key IE.Token22)
--    deriving (Generic)
--instance FromJSON OrderItem
--
--data OrderRow = OrderRow
--    { oitem     :: OrderItem
--    , amount    :: Amount
--    , instr     :: T.Text
--    } deriving (Generic)
--instance FromJSON OrderRow 

data Courrier = Courrier
    { name      :: T.Text
    , ordnum    :: Int16
    , phone     :: T.Text
    } deriving (Generic)
instance FromJSON Courrier
instance Persistable Courrier where
    type Key Courrier = Int16
    type Tag Courrier = TagSC


data Status = Created | Paid | Dispatched | Delivered | Cancelled

instance Show Status where
    show Created    = "Creado"
    show Paid       = "Pagado"
    show Dispatched = "Enviado"
    show Delivered  = "Entregado"
    show Cancelled  = "Cancelado"


data Order (k :: Status) where
    OrCreation      :: OrderHdr -> LocalTime -> Order 'Created
    OrPayment       :: C.Key2 (Order 'Created) -> Order 'Paid
    OrDispatching   :: C.Key2 (Order 'Paid) -> Key DspHdr -> Order 'Dispatched
    OrDelivery      :: C.Key2 (Order 'Dispatched) -> Order 'Delivered
    OrCancel        :: C.Key2 (Order 'Created) -> Order 'Cancelled
instance C.Persistable2 (Order a) where
    type Tag2 (Order a) = TagSC

instance FromJSON (Order 'Paid) where
    parseJSON = withObject "Payment" $ \v -> OrPayment
        <$> v .: "orderK"


data OrBack (k :: Status) where
    BackPmt :: C.Key2 (Order 'Paid) -> C.Key2 (Order 'Created) -> OrBack ('Paid)



--data Payment = Payment
--    { order     :: Key OrderHdr
--    }
--instance Persistable Payment where
--    type Key Payment = Int64
--    type Tag Payment = TagSC

data DspHdr = DspHdr
    { courrier      :: Key Courrier
    , note          :: T.Text
    } deriving (Generic)
instance FromJSON DspHdr

instance C.Persistable2 DspHdr where
    type Tag2 DspHdr = (Amount, Int16)

data DspRow = DspRow
    { hdrK      :: C.Key2 DspHdr
    , rowK      :: C.Key2 (Order 'Dispatched)
    }
