{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE InstanceSigs #-}

module Sms where

import Text.RawString.QQ (r)
import GHC.Generics
import GHC.Int (Int16, Int32)
import Data.Time (UTCTime, getCurrentTime, NominalDiffTime, addUTCTime, diffUTCTime)
import Hasql.Connection (Connection)
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSL
import qualified Data.Text as T
import qualified Data.Vector as V

import Control.Monad.IO.Class (liftIO)

import Data.Default.Class (def)
import qualified Network.HTTP.Req as R
import Data.Aeson (withObject, (.=), (.:), ToJSON, FromJSON(..), parseJSON, object, encode, eitherDecode)

import Data.Functor.Contravariant (contramap)
import qualified Hasql.Encoders as E
import qualified Hasql.Decoders as D
import Db(Encodable(..), Decodable(..), execStmt)
import Hasql.Statement (Statement(..))

import Network.Socket (SockAddr(SockAddrInet), HostAddress)

import Entities.Common (tshow)
import qualified Entities.Client as CE

url :: T.Text
url = "api.bulksms.com"

tokenName :: T.Text
tokenName = "pescadolc"

data Conf = Conf BS.ByteString BS.ByteString

data SendStatus = SendStatus
    { id        :: T.Text
    , phone     :: T.Text
    , msg       :: T.Text
    , encoding  :: T.Text
    , cost      :: Float
    , submid    :: T.Text
    , date      :: UTCTime
    , status    :: T.Text
    } deriving (Generic)
instance ToJSON SendStatus
instance FromJSON SendStatus where
    parseJSON = withObject "sendStatus" $ \v -> do
        objStatus <- v .: "status"
        objSubm <- v .: "submission"
        id <- v .: "id"
        phone <- v .: "to"
        msg <- v .: "body"
        encoding <- v .: "encoding"
        cost <- v .: "creditCost"
        submid <- objSubm .: "id"
        date <- objSubm .: "date"
        status <- objStatus .: "id"
        return SendStatus{..}

testPayload phone = object
    [ "to" .= ("58" <> phone)
    , "body" .= ("pescadolacruz.com\nTest message" :: T.Text)
    ]

confCodePayload phone confCode = object
    [ "to" .= ("58" <> phone)
    , "body" .= ("pescadolacruz.com\nSu código de confirmación es: " <> confCode)
    ]
    

data SmsLog = SmsLog
    { ip        :: HostAddress
    , phone     :: T.Text
    , servJson  :: BS.ByteString
    }
instance Encodable SmsLog where
    encoder :: E.Params SmsLog
    encoder
        =  contramap (fromIntegral . ip) (E.param E.int4)
        <> contramap (phone :: SmsLog -> T.Text) (E.param E.text)
        <> contramap servJson (E.param E.jsonBytes)
instance Decodable SmsLog where
    decoder :: D.Row SmsLog
    decoder = SmsLog
        <$> D.column (fmap fromIntegral D.int4)
        <*> D.column D.text
        <*> D.column D.bytea
        -- <*> D.column (D.jsonBytes (\s -> leftToText $ eitherDecode $ BSL.fromStrict s))
        where
        leftToText (Left s) = Left $ T.pack s
        leftToText (Right a) = Right a

saveLog :: Statement SmsLog ()
saveLog = Statement query encoder D.unit True
    where
    query = [r|
        insert into smslog (ip, phone, srvjson) values ($1, $2, $3)
        |]
getLogs :: Statement () (V.Vector (SmsLog, UTCTime))
getLogs = Statement query encoder (D.rowVector decoder) True
    where
    query = [r|
        select ip, phone, srvjson, savedat
        from smslog
        order by savedat desc
        limit 40
        |]

getLastTime :: Statement HostAddress (Maybe UTCTime)
getLastTime = Statement query encoder (D.singleRow decoder) True
    where
    query = [r|
        select max(savedat)
        from smslog
        where ip=$1
        |]
getSentQty :: Statement (HostAddress, UTCTime, Int16) (Int16)
getSentQty = Statement query encoder (D.singleRow decoder) True
    where
    query = [r|
        select count(ip)
        from smslog
        where ip=$1 and savedat >= $2 - (interval '1 HOUR' * $3)
        |]


sendTest :: (Connection, Conf) -> SockAddr -> T.Text -> IO [SendStatus]
sendTest (conn, conf) socket phone = do
    sendStatus <- R.runReq R.defaultHttpConfig $ do
        r <- R.req R.POST
            (R.https url R./: "v1" R./: "messages")
            (R.ReqBodyJson $ testPayload phone)
            R.jsonResponse
            (R.basicAuth tokenId pwd)
        return $ R.responseBody r
    let SockAddrInet _ ip = socket
    liftIO $ execStmt conn saveLog $ SmsLog ip phone $ BSL.toStrict $ encode $ head sendStatus
    return sendStatus
    where
    Conf tokenId pwd = conf

sendConfCode :: (Connection, Conf) -> SockAddr -> CE.Phone -> T.Text -> IO [SendStatus]
sendConfCode (conn, conf) socket (CE.Phone phoneTxt) confCode = do
    sendStatus <- R.runReq R.defaultHttpConfig $ do
        r <- R.req R.POST
            (R.https url R./: "v1" R./: "messages")
            (R.ReqBodyJson $ confCodePayload phoneTxt confCode)
            R.jsonResponse
            ((R.basicAuth tokenId pwd) <> ("auto-unicode" R.=: ("true" :: T.Text)))
        return $ R.responseBody r
    --currTime <- getCurrentTime
    --let sendStatus = [SendStatus "fake" phoneTxt "fake" "fake" 1.1 "fake" currTime "fake"]
    let SockAddrInet _ ip = socket
    liftIO $ execStmt conn saveLog $ SmsLog ip phoneTxt $ BSL.toStrict $ encode $ head sendStatus
    return sendStatus
    where
    Conf tokenId pwd = conf

--------------------------------------------------------------
data ServiceStatus = ServiceStatus
    { balance   :: Float
    , limit     :: Int32
    }
instance FromJSON ServiceStatus where
    parseJSON = withObject "serviceStatus" $ \v -> do
        obj <- v .: "credits"
        balance <- obj .: "balance"
        limit <- obj .: "limit"
        return ServiceStatus{..}


getServiceStatus :: (Connection, Conf) -> IO (ServiceStatus)
getServiceStatus (conn, conf) = do
    R.runReq R.defaultHttpConfig $ do
        r <- R.req R.GET
            (R.https url R./: "v1" R./: "profile")
            R.NoReqBody
            R.jsonResponse
            (R.basicAuth tokenId pwd)
        return $ R.responseBody r
    where
    Conf tokenId pwd = conf


getSmsDelay :: Connection -> SockAddr -> IO (NominalDiffTime)
getSmsDelay conn socket = do
    let SockAddrInet _ ip = socket
    currTime <- getCurrentTime
    mLastTime <- execStmt conn Sms.getLastTime ip
    case mLastTime of
        Nothing -> return 0
        Just lastTime -> do
            qty <- execStmt conn Sms.getSentQty (ip, lastTime, 24)
            return $ max 0 $ diffUTCTime (addUTCTime (60 * (delaySchedule qty)) lastTime) currTime
    where
    delaySchedule :: Int16 -> NominalDiffTime
    delaySchedule 0 = 0
    delaySchedule 1 = 1
    delaySchedule 2 = 5
    delaySchedule 3 = 30
    delaySchedule 4 = 60
    delaySchedule _ = 60*24



