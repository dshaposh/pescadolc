drop schema if exists pescadolc cascade;
create schema pescadolc;


create table staff
    ( roles char(3) not null check (roles in ('adm', 'opr'))
    , nick varchar(12) not null
    , name varchar(50) not null
    , email varchar(100) not null
    , id serial primary key
    , savedat timestamp not null default (now() at time zone 'utc')
    , isblocked boolean not null default false
    , pwd varchar(20)
    , unique (nick)
    , unique (email)
    );
insert into staff (email, nick, name, roles, pwd)
values
    ('gerente@pescadolacruz.com', 'admin', 'Alberto Thomas', 'adm', '1'),
    ('chucho@yahoo.com', 'chucho', 'Jesus Carlos Rincon', 'opr', 'chucho'),
    ('pandas77@gmail.com', 'pand88as', 'Paula Andrea Astorio', 'opr', 'panda');

create table uom 
    ( id serial primary key
    , name varchar(50) not null
    , ordnum int not null default 1000
    , isclosed boolean not null default false
    , savedat timestamp not null default (now() at time zone 'utc')
    );
insert into uom (name)
values ('kg'), ('pz');

create table specie
    ( name varchar(50) not null
    , ordnum int not null default 1000
    , uom smallint not null references uom(id)
    , price bigint not null default 0
    , image bytea
    , imgsavedat timestamp
    , description text not null default ''
    , id serial primary key
    , savedat timestamp not null default (now() at time zone 'utc')
    , isclosed boolean not null default false
    );
insert into specie (name, uom, price)
values
('Mero', 1, 0), ('Cason', 1, 0), ('Piqua', 1, 0),
('Raya', 1, 0), ('Pargo', 1, 0), ('Camaron', 1, 0),
('Langostino', 2, 0), ('Sierra', 1, 0), ('Cabaña blanca', 1, 0),
('Cojinua', 1, 0), ('Carite', 1,  0), ('Atun medregal', 1, 0),
('Lebranche (Lisa)', 1, 0), ('Dorado', 1, 0), ('Corocoro', 1, 0),
('Mejillon', 1, 0);

create table cutting 
    ( id serial primary key
    , name varchar(50) not null
    , ordnum int not null default 1000
    , isclosed boolean not null default false
    , savedat timestamp not null default (now() at time zone 'utc')
    );
insert into cutting (name)
values ('Entero'), ('Ruedas'), ('Churrasco'), ('Pelado'), ('Remnantes');


create table item
    ( name varchar(50) not null
    , ordnum int not null default 1000
    , specie int not null references specie(id)
    , cutting int not null references cutting(id)
    , uom int not null references uom(id)
    , minsztp char(2) not null check ( minsztp in ('vs', 'fs', 'nl'))
    , minszval int
    , markup smallint not null
    , id bigserial primary key
    , price bigint not null default 0
    , savedat timestamp not null default (now() at time zone 'utc')
    , isclosed boolean not null default false
    );
\copy item(name, specie, cutting, uom, minsztp, minszval, markup)  from 'items.csv' with (format 'csv', header true, delimiter ',');

create view item_v as
    select i.name, i.ordnum, i.specie, i.cutting, i.uom, i.minsztp, i.minszval, i.markup, i.id, i.savedat, i.isclosed
                , s.name as sname, c.name as cname, u.name as uname, i.price
    from item i
    inner join specie s
    on i.specie=s.id
    inner join cutting c
    on i.cutting=c.id
    inner join uom u
    on i.uom=u.id
    order by i.ordnum asc;


create table preorder_row
    ( sid   char(20)    not null
    , item  bigint      not null references item(id)
    , qty   bigint      not null check (qty >= 0)
    , unique (sid, item)
    );


create table courrier
    ( name varchar(50) not null
    , ordnum int not null default 1000
    , phone char(11) not null
    , id bigserial primary key
    , savedat timestamp not null default (now() at time zone 'utc')
    , isclosed boolean not null default false
    );
insert into courrier (name, phone)
values ('Jesus Rodriquez', '04124568332'),
       ('Jose Vasquez', '04169843232'),
       ('Luis Alvarez', '04123459293');

create table place
    ( name varchar(50) not null
    , ordnum int not null default 1000
    , id serial primary key
    , isclosed boolean not null default false
    , savedat timestamp not null default (now() at time zone 'utc')
    );
insert into place (name)
values ('Congelador A'), ('Nivera 32'), ('Mesa de trabajo');

create table label
    ( name varchar(5) primary key check (name <> '')
    );
/* create unique index label_notclosed on label (txt) where not isclosed;*/
insert into label (name)
select a.n from generate_series(1,30) as a(n);

create table vendor
    ( name varchar(50) not null
    , ordnum int not null default 1000
    , taxid varchar(20) not null
    , phone varchar(7) not null default ''
    , id serial primary key
    , isclosed boolean not null default false
    , savedat timestamp not null default (now() at time zone 'utc')
    );
insert into vendor (name, taxid, phone)
values ('Mariscos Maravillosos, C.A.', '2242452-2', '242524'), ('El Pescador, S.A.', '7476456-2', '35353');

create table token
    ( code varchar(10) not null
    , day date not null
    , note varchar(100) not null
    , id bigserial primary key
    , savedat timestamp not null default (now() at time zone 'utc')
    );
insert into token (code, day, note)
values ('token1', '2020-03-17', 'Test token');

/* maybe this table will not be required */
create table token2
    ( code varchar(10) not null
    , item bigint not null
    , token bigint not null references item(id)
    , placetp char(2) not null default 'wh'
    , placeid bigint not null
    , qty int not null default 0 check (qty >= 0)
    , amount bigint not null default 0 check (amount >= 0)
    , id bigserial primary key
);

create table merchandise_lot
    ( item bigint not null references item(id)
    , token bigint not null references token(id)
    , qty bigint not null default 0 check (qty >= 0)
    , amount bigint not null default 0 check (amount >= 0)
    , sprice bigint not null default 0 check (sprice >= 0)
    , rsqty bigint not null default 0 check (rsqty >= 0)
    , unique (item, token)
    , check (rsqty <= qty)
    );
insert into merchandise_lot (item, token, qty, amount)
values (1, 1, 8000, 950000),
       (2, 1, 3500, 200000),
       (3, 1, 4500, 350000),
       (10, 1, 5600, 750000);

create table trxtag
    ( id bigserial primary key
    , dt char(2) not null check (dt in ('pr', 'pm', 'ds', 'pd', 'wo', 'lb'))
    , savedat timestamp not null default (now() at time zone 'utc')
    , unique (dt, id)
    );
insert into trxtag (dt) values ('pr'), ('pr'), ('pr'), ('pr');

create table inventory_balance
    ( item bigint not null references item(id)
    , token bigint not null references token(id)
    , place bigint not null references place(id)
    , qty bigint not null default 0 check (qty >= 0)
    , amount bigint not null default 0 check (amount >= 0)
    , unique (item, token, place)
    );
insert into inventory_balance (item, place, token, qty, amount)
values (1, 1, 1, 2500, 800000),
       (2, 1, 1, 3500, 200000),
       (3, 1, 1, 4500, 350000),
       (1, 2, 1, 5500, 150000),
       (10, 2, 1, 5600, 750000);

create table labeled_mvmt
( id bigserial primary key
, tp char(2) not null
, trxid bigint not null references trxtag(id)
, lblid bigint not null default currval('labeled_mvmt_id_seq')
, prevdoc bigint  
, place bigint references place(id)
, day date not null
, note varchar(100) not null default ''
, unique (id, tp)
, unique (id, lblid)
, foreign key (prevdoc, lblid) references labeled_mvmt(id, lblid)
);

create table labeled
( label varchar(10) not null
, item bigint not null references item(id)
, token bigint not null references token(id)
, place bigint references place(id)
, qty int not null default 0
, cost bigint not null default 0
, id bigint primary key
, tp char(2) default 'cr'
, lastdoc bigint not null references labeled_mvmt(id)
, rsrtp char(2) default 'so' 
, rsrid bigint default 0
, foreign key (id, tp) references labeled_mvmt(id, tp)
);
create unique index labeled_label on labeled (label) where place is not null;

create table labeled_mvmt_back
( docid bigint not null references labeled_mvmt(id)
, trxid bigint not null references trxtag(id)
);
    
/* to rename tkcode to label */
create table markitem
    ( place bigint not null references place(id)
    , item bigint not null references item(id)
    , token bigint not null references token(id)
    , tkcode varchar(10) not null
    , tkitem bigint not null references item(id)
    , qty int not null default 0 check (qty >= 0)
    , amount bigint not null default 0 check (amount >= 0)
    , sprice bigint not null default 0 check (sprice >= 0)
    , docwhtp char(2) not null
    , docwhid bigint not null
    , tkwhtp char(2) not null
    , tkwhid bigint not null
    , note varchar(100) not null default ''
    , id bigserial primary key
    , isused bool not null default false
    , reserved2 bigint default 0
);

create view merchandise as (
    select m.tp, m.indk, m.item, m.token, m.qty, m.amount, m.sprice, m.rsqty
        , t.code as tcode, t.day as tday, t.note as tnote
        , i.name as iname, i.ordnum as irdnum, i.specie as ispecie, i.cutting as icutting, i.uom as iuom, i.minsztp as iminsztp, i.minszval as iminszval, i.markup as imarkup
        , u.name as uname
    from (
        select 'lot' as tp, null as indk, item, token, qty, amount, sprice, rsqty
        from merchandise_lot
        where qty > 0
        union all
        select 'ind', id, item, token, qty, cost, 0, case coalesce(rsrid, 0) when 0 then 0 else qty end
        from labeled
        where place is not null
    ) m
    inner join item i
    on i.id = m.item
    inner join token t
    on t.id = m.token
    inner join uom u
    on u.id = i.uom
);
/*
        union all
        select 'ind', id, item, token, qty, amount, sprice, case coalesce(reserved2, 0) when 0 then 0 else qty end
        from markitem
        where not isused
*/
create view inventory_balance_v as
    select b.tag, b.token2, b.item, b.token, b.place, b.qty, b.amount
        , t.code as tcode, t.day as tday, t.note as tnote
        , i.name as iname, i.ordnum as irdnum, i.specie as ispecie, i.cutting as icutting, i.uom as iuom, i.minsztp as iminsztp, i.minszval as iminszval, i.markup as imarkup
        , p.name as pname, u.name as uname

    from (
        select 'lt' as tag, null as token2, b.item, b.token, b.place, b.qty, b.amount
        from inventory_balance b
        where b.qty > 0
        union all
        select 'id', label, item, token, place, qty, cost 
        from labeled
        where place is not null 
    ) b
    inner join item i
    on i.id = b.item
    inner join token t
    on t.id = b.token
    inner join place p
    on p.id = b.place
    inner join uom u
    on u.id = i.uom;

/*
        union all
        select 'id', m.tkcode, m.tkitem, m.token, m.tkwhid, m.qty, m.amount
        from markitem m
        where not isused and m.tkwhtp ='pl'
*/

create table purchase_header
    ( vendor int not null references vendor(id)
    , docday date not null
    , docnum varchar(30) not null
    , note varchar(100) not null default ''
    , id bigserial primary key
    , token bigint not null references token(id)
    , amount bigint not null default 0 check (amount >=0)
    );
insert into purchase_header (vendor, docday, docnum, token, amount)
values (1, '2020-03-17', 'test purchase1', 1, 1000000),
       (2, '2020-03-17', 'test purchase2', 1, 500000);

create table purchase_row
    ( item bigint not null references item(id)
    , place int not null references place(id)
    , qty bigint not null default 0 check (qty >= 0)
    , amount bigint not null default 0 check (amount >= 0)
    , tp char(2) not null default ('pr')
    , id bigserial not null primary key
    , header bigint not null references purchase_header(id)
    , unique (item, place, header)
    );
insert into purchase_row (item, place, qty, amount, header)
values (1, 1, 1500, 800000, 1),
       (2, 1, 2500, 200000, 1),
       (3, 1, 1700, 350000, 2),
       (1, 2, 3400, 150000, 2);

create table purchase_trx
    ( docrow bigint not null references purchase_row(id)
    , header bigint not null references purchase_header(id)
    , trxtag bigint not null
    , item bigint not null
    , token bigint not null references token(id)
    , place bigint not null
    , qty bigint not null
    , amount bigint not null
    , dt char(2) not null default 'pr'
    , foreign key (dt, trxtag) references trxtag(dt,id)
    , foreign key (item, token, place) references inventory_balance(item, token, place)
    , unique (trxtag, docrow)
    );


create table so_hist
    ( id  bigserial primary key
    , st char(2) not null check (st in('cr', 'pd', 'dp', 'dl', 'cl'))
    , sohdr bigint not null default currval('so_hist_id_seq')
    , fromid bigint
    , fromst char(2)
    , trxtag bigint not null references trxtag(id)
    , check ((fromid is null and fromst is null and sohdr=id) or (fromid is not null and fromst is not null and sohdr <> id))
    , unique (sohdr, id, st)
    , unique (id, st)
    , foreign key (sohdr, fromid, fromst) references so_hist(sohdr, id, st)
    );
insert into so_hist(sohdr, st, trxtag)
values (1, 'cr', 1), (2, 'cr', 2), (3, 'cr', 3);

create table so_back
    ( sohdr bigint not null
    , fromid bigint not null
    , fromst char(2) not null
    , toid bigint not null
    , tost char(2) not null
    , trxtag bigint not null references trxtag(id)
    , foreign key (sohdr, fromid, fromst) references so_hist(sohdr, id, st)
    , foreign key (sohdr, toid, tost) references so_hist(sohdr, id, st)
    );

create table so_header
    ( phone varchar(11) not null
    , dlvday date not null
    , name varchar (30) not null
    , address varchar(100) not null
    , dlvcost bigint not null default 0
    , amount bigint not null default 0
    , rowsq smallint not null default 0
    , curid bigint not null
    , curst char(2) not null default 'cr'
    , id bigint not null primary key
    , foreign key (curid, curst) references so_hist(id, st)
    );
insert into so_header (phone, dlvday, name, address, amount, rowsq, curid, id)
values
 ('04245993465', '2020-04-05', 'Jose Pardo', 'Lecheria, Av.Principal 45 567', 874922, 4, 1, 1)
,('04127379285', '2020-04-04', 'Johanna Alvarez', 'Lecheria, El Morro, 4 57', 1274983, 3, 2, 2)
,('04145492817', '2020-04-05', 'Alfredo Gardo', 'Lecheria, Pueblo Viejo 32 6', 2494922, 2, 3, 3);

create table pcart_lot
    ( sid bytea not null
    , item bigint not null references item(id)
    , token bigint not null references token(id)
    , qty int not null check (qty > 0)
    , amount bigint not null check (amount > 0)
    , instr varchar(100) not null default ''
    , so bigint references so_header(id)
    , id bigserial primary key
    , status char(3) not null default 'prp' check (status in ('prp', 'dsp'))
    , prpqty int not null default 0
    );
create table so_picklist
( sorow bigint not null references pcart_lot(id)
, place bigint not null references place(id)
, qty int not null
, trx bigint not null references trxtag(id)
);


create table pcart_ind
    ( sid bytea not null
    , ind bigint not null references labeled(id)
    , amount bigint not null check (amount > 0)
    , instr varchar(100) not null default ''
    , so bigint references so_header(id)
    );
create view pcart as (
    select sid, 'lot' as tp, item, token, qty, null as indk, amount, instr, so
    from pcart_lot
    union all
    select p.sid, 'ind', m.item, m.token, m.qty, p.ind, p.amount, p.instr, so
    from pcart_ind p
    inner join labeled m
    on p.ind = m.id
    

);


create table dsp_header
    ( courrier bigint not null references courrier(id)
    , note varchar(100) not null default ''
    , id bigserial primary key
    , amount bigint not null default 0
    , ordsqty smallint not null default 0
    ); 

create table dsp_row
    ( hdr bigint not null references dsp_header(id)
    , so bigint not null
    , st char(2) not null default 'dp'
    , foreign key (so, st) references so_hist(id, st)
    ); 
create view dsp_row_final as
    select r.hdr, r.so, r.st
    from dsp_row r
    inner join so_hist as h
    on r.so=h.id and r.st=h.st
    left join so_back as b
    on h.sohdr=b.sohdr and h.id=b.fromid and h.st=b.fromst
    where b.fromid is null;

create table wroff_lot
( item bigint not null references item(id)
, token bigint not null references token(id)
, place int not null references place(id)
, day date not null 
, note varchar(100) not null default ''
, qty int not null default 0 check (qty >= 0)
, amount bigint not null default 0 check (amount >= 0)
, id bigserial primary key
);

create table wroff_lot_trx
( wroff bigint not null references wroff_lot(id)
, qty int not null default 0
, amount bigint not null default 0
, trxkey bigint not null references trxtag(id)
);

create table smslog
( ip bigint not null
, phone varchar(10) not null
, srvjson json not null
, savedat timestamp not null default (now() at time zone 'utc')
);

create table visitorlog
( sid bytea not null
, ip bigint not null
, useragent varchar(150) not null
, savedat timestamp not null default (now() at time zone 'utc')
);

create table dlvprice
( price bigint not null
, savedat timestamp not null default (now() at time zone 'utc')
);
insert into dlvprice values(350000);

create table dlvday
( day date not null
, savedat timestamp not null default (now() at time zone 'utc')
);
insert into dlvday values('2020-05-11');


create table cmsdata
( name varchar(20) not null
, content bytea not null
, savedat timestamp not null default (now() at time zone 'utc')
);

