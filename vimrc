"set makeprg=cabal\ new-build
set makeprg=nix-shell\ --pure\ shell.nix\ --run\ \"cabal\ new-build\"
set errorformat=
    \%-G%.%#:\ build,
    \%-G%.%#preprocessing\ library\ %.%#,
    \%-G[%.%#]%.%#,
    \%E%f:%l:%c:\ %m,
    \%-G--%.%#

map <F5> :make<CR>
set foldnestmax=1
set foldmethod=indent
